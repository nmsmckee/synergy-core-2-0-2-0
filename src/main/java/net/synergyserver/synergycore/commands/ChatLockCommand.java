package net.synergyserver.synergycore.commands;

import net.synergyserver.synergycore.BroadcastType;
import net.synergyserver.synergycore.Chat;
import net.synergyserver.synergycore.SynergyCore;
import net.synergyserver.synergycore.configs.Message;
import net.synergyserver.synergycore.events.ChatBroadcastEvent;
import net.synergyserver.synergycore.events.ChatLockEvent;
import net.synergyserver.synergycore.utils.PlayerUtil;
import org.bukkit.Bukkit;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.UUID;

@CommandDeclaration(
        commandName = "lock",
        aliases = "l",
        permission = "syn.chat.lock",
        usage = "/chat lock",
        description = "Toggles the locked state of chat. " +
                "If chat is locked then anyone who doesn't have the bypass permission is prevented from chatting.",
        maxArgs = 0,
        validSenders = {SenderType.PLAYER, SenderType.CONSOLE},
        parentCommandName = "chat"
)
public class ChatLockCommand extends SubCommand {

    @Override
    public boolean execute(CommandSender sender, String[] args, CommandFlags flags) {
        Chat chat = Chat.getInstance();
        UUID locker;
        boolean newLockState = !chat.isLocked();

        // Get the UUID of the sender
        if (SenderType.getSenderType(sender).equals(SenderType.PLAYER)) {
            locker = ((Player) sender).getUniqueId();
        } else {
            locker = SynergyCore.SERVER_ID;
        }

        // Emit an event
        ChatLockEvent event = new ChatLockEvent(locker, newLockState);
        Bukkit.getPluginManager().callEvent(event);

        // Check if the event was cancelled before continuing
        if (event.isCancelled()) {
            return true;
        }

        // Lock chat
        chat.setLocked(newLockState);

        // Broadcast a message
        String message;
        if (newLockState) {
            message = Message.format("commands.chat.lock.info.announcement_lock", PlayerUtil.getName(locker));
        } else {
            message = Message.format("commands.chat.lock.info.announcement_unlock", PlayerUtil.getName(locker));
        }
        ChatBroadcastEvent broadcastEvent = new ChatBroadcastEvent(BroadcastType.ANNOUNCEMENT, message);
        Bukkit.getPluginManager().callEvent(broadcastEvent);

        // Check if the event was cancelled before broadcasting
        if (!broadcastEvent.isCancelled()) {
            Bukkit.broadcastMessage(message);
        }

        // Give the sender feedback
        if (newLockState) {
            sender.sendMessage(Message.get("commands.chat.lock.info.lock"));
        } else {
            sender.sendMessage(Message.get("commands.chat.lock.info.unlock"));
        }
        return true;
    }

}
