package net.synergyserver.synergycore.commands;

import net.synergyserver.synergycore.ServiceToken;
import net.synergyserver.synergycore.ServiceType;
import net.synergyserver.synergycore.SynergyCore;
import net.synergyserver.synergycore.configs.Message;
import net.synergyserver.synergycore.database.DataManager;
import net.synergyserver.synergycore.profiles.MinecraftProfile;
import net.synergyserver.synergycore.profiles.SynUser;
import net.synergyserver.synergycore.utils.PlayerUtil;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

@CommandDeclaration(
        commandName = "add",
        aliases = {"a", "connect"},
        permission = "syn.connection.add",
        usage = "/connection add <discord|dubtrack>",
        description = "Allows the sender to connect to a supported service.",
        minArgs = 1,
        maxArgs = 1,
        validSenders = SenderType.PLAYER,
        parentCommandName = "connection"
)
public class ConnectionAddCommand extends SubCommand {

    @Override
    public boolean execute(CommandSender sender, String[] args, CommandFlags flags) {
        Player player = (Player) sender;
        MinecraftProfile mcp = PlayerUtil.getProfile(player);

        if (args.length == 0) {
            player.sendMessage(Message.format("commands.connection.error.undefined"));
            return false;
        }

        ServiceType serviceType = ServiceType.getServiceType(args[0]);

        if (serviceType == null) {
            player.sendMessage(Message.format("commands.connection.error.invalid", args[0]));
            return false;
        }

        SynUser synUser = DataManager.getInstance().getDataEntity(SynUser.class, mcp.getSynID());
        ServiceToken serviceToken = new ServiceToken(300000);
        String timeMessage = Message.getTimeMessage(
                serviceToken.getLife(), 1, 0,
                Message.get("info_colored_lists.item_color_1"),
                Message.get("info_colored_lists.grammar_color")
        );

        if (serviceType.equals(ServiceType.DISCORD)) {
            // If the bot is online then there's no point in giving them a token
            if (!SynergyCore.isDiscordBotOnline()) {
                sender.sendMessage(Message.format("commands.error.service_offline", "Discord"));
                return false;
            }

            if (synUser.getDiscordID() != null) {
                // If they already have a connection then stop the process and send them a message
                player.sendMessage(Message.format("commands.connection.connect.error.already_connected", "Discord"));
                return false;
            } else {
                // Otherwise, begin the connection process
                synUser.setDiscordToken(serviceToken);
                player.sendMessage(Message.format("commands.connection.connect.info.token", "the #welcome channel on Discord",
                        serviceToken.getToken(), timeMessage, "Discord", "https://discord.gg/xwvx22P"));
                return true;
            }
        } else if (serviceType.equals(ServiceType.DUBTRACK) && synUser.getDubtrackID() == null) {
            // If the bot is online then there's no point in giving them a token
            if (!SynergyCore.isDubtrackBotOnline()) {
                sender.sendMessage(Message.format("commands.error.service_offline", "Dubtrack"));
                return false;
            }

            if (synUser.getDubtrackID() != null) {
                // If they already have a connection then stop the process and send them a message
                player.sendMessage(Message.format("commands.connection.connect.error.already_connected", "Dubtrack"));
                return false;
            } else {
                // Otherwise, begin the connection process
                synUser.setDubtrackToken(serviceToken);
                player.sendMessage(Message.format("commands.connection.connect.info.token", "chat on Dubtrack",
                        serviceToken.getToken(), timeMessage, "Dubtrack", "https://www.dubtrack.fm/join/synergyserver"));
                return true;
            }
        }

        return false;
    }
}
