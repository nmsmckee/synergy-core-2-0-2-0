package net.synergyserver.synergycore.commands;

import net.synergyserver.synergycore.SynergyCore;
import net.synergyserver.synergycore.SynergyPlugin;
import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.FormattedCommandAlias;
import org.bukkit.command.PluginIdentifiableCommand;
import org.bukkit.command.SimpleCommandMap;
import org.bukkit.command.defaults.BukkitCommand;
import org.bukkit.craftbukkit.v1_19_R1.CraftServer;
import org.bukkit.craftbukkit.v1_19_R1.command.VanillaCommandWrapper;
import org.spigotmc.SpigotCommand;

import java.lang.reflect.Field;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * Represents a <code>CommandManager</code>, which registers
 * and keeps track of synCommands by its <code>MainCommand</code>.
 */
public class CommandManager {

    private static CommandManager instance = null;
    private SimpleCommandMap commandMap;
    private HashMap<String, SynCommand> synCommands = new HashMap<>();

    /**
     * Creates a new <code>CommandManager</code> object and hooks into CraftBukkit's central command system.
     */
    private CommandManager() {
        commandMap = ((CraftServer) Bukkit.getServer()).getCommandMap();
    }

    /**
     * Returns the object representing this <code>CommandManager</code>.
     *
     * @return The object of this class.
     */
    public static CommandManager getInstance() {
        if (instance == null) {
            instance = new CommandManager();
        }
        return instance;
    }

    /**
     * Gets the <code>CommandMap</code> used by Bukkit for registering commands.
     *
     * @return Bukkit's <code>CommandMap</code>.
     */
    public SimpleCommandMap getCommandMap() {
        return commandMap;
    }

    /**
     * Gets the map that represents all registered commands in Bukkit.
     *
     * @return The map of registered commands.
     */
    public Map<String, Command> getRegisteredCommands() {
        try {
            Field f = SimpleCommandMap.class.getDeclaredField("knownCommands");
            if (!f.isAccessible()) {
                f.setAccessible(true);
            }
            return (Map<String, Command>) f.get(commandMap);
        } catch (NoSuchFieldException|IllegalAccessException e) {
            e.printStackTrace();
            return new HashMap<>();
        }
    }

    /**
     * Creates and registers a <code>MainCommand</code>.
     * This will also inject it into CraftBukkit's central <code>CommandMap</code>.
     *
     * @param plugin The plugin that owns the given command.
     * @param commandClass The class of the command to register.
     */
    public <T extends MainCommand> void registerMainCommand(SynergyPlugin plugin, Class<T> commandClass) {
        try {
            // Instantiate and populate the command
            MainCommand command = commandClass.newInstance();
            command.populate(plugin);

            // Register the command
            synCommands.put(command.getCommandName(), command);
            commandMap.register(plugin.getName(), command);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Creates and registers a <code>SubCommnand</code>.
     *
     * @param plugin The plugin that owns the given command.
     * @param commandClass The class of the command to register.
     */
    public <T extends SubCommand> void registerSubCommand(SynergyPlugin plugin, Class<T> commandClass) {
        try {
            // Instantiate and populate the command
            SubCommand command = commandClass.newInstance();
            command.populate(plugin);

            // Register the command
            String parentCommandName = command.getParentCommandName();
            getSynCommand(parentCommandName).registerSubCommand(command);
            String commandKey = parentCommandName + " " + command.getCommandName();
            synCommands.put(commandKey, command);
        } catch (Exception e) {
            plugin.getLogger().severe("Possibly attempted to register a SubCommand before its parent!");
            e.printStackTrace();
        }
    }

    /**
     * Gets the <code>Command</code> with the given command label registered to Bukkit.
     *
     * @param commandLabel The label of the command to get.
     * @return The <code>Command</code>, if found, or null.
     */
    public Command getCommand(String commandLabel) {
        return commandMap.getCommand(commandLabel);
    }

    /**
     * Checks if the given command labels refer to the same command, including aliases.
     *
     * @param commandLabel1 The first command label.
     * @param commandLabel2 The second command label.
     * @return True if the commands are the same.
     */
    public boolean areCommandsEqual(String commandLabel1, String commandLabel2) {
        Command command1 = getCommand(commandLabel1);
        Command command2 = getCommand(commandLabel2);

        if (command1 == null || command2 == null) {
            return false;
        } else if (command1.equals(command2)) {
            return true;
        } else if (command1 instanceof FormattedCommandAlias) {
            // Make the components of the alias available and check the first result
            try {
                Field f = FormattedCommandAlias.class.getDeclaredField("formatStrings");
                f.setAccessible(true);
                String[] formatStrings = (String[]) f.get(command1);

                if (formatStrings.length == 0) {
                    return false;
                }

                String[] alias1 = formatStrings[0].split(" ");

                if (alias1.length == 0) {
                    return false;
                }

                if (getCommand(alias1[0]).equals(command2)) {
                    return true;
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else if (command2 instanceof FormattedCommandAlias) {
            // Make the components of the alias available and check the first result
            try {
                Field f = FormattedCommandAlias.class.getDeclaredField("formatStrings");
                f.setAccessible(true);
                String[] formatStrings = (String[]) f.get(command2);

                if (formatStrings.length == 0) {
                    return false;
                }

                String[] alias2 = formatStrings[0].split(" ");

                if (alias2.length == 0) {
                    return false;
                }

                if (getCommand(alias2[0]).equals(command1)) {
                    return true;
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return false;
    }

    /**
     * Gets the <code>SynCommand</code> from <code>synCommands</code> by its given name, if it's found.
     *
     * @param commandName The name of the requested <code>SynCommand</code>.
     * @return The instance of the requested <code>SynCommand</code>, if it's found.
     */
    public SynCommand getSynCommand(String commandName) {
        return synCommands.get(commandName);
    }

    /**
     * Gets the list of commands that snoop ignores.
     *
     * @return The list of blacklisted snoop commands.
     */
    public List<Command> getSnoopBlacklistCommands() {
        List<String> commandNames = SynergyCore.getPluginConfig().getStringList("snoop_command_blacklist");
        return commandNames.stream().map(CommandManager.getInstance()::getCommand).collect(Collectors.toList());
    }

    /**
     * Gets the name of the owner of the given command. If the command is a plugin command, this returns the name
     * of the owning plugin. If it is instead a vanilla command then "minecraft" is returned. If it is a Bukkit/Spigot
     * command then "bukkit" is returned.
     *
     * @param command The command to get the owner of.
     * @return The name of the owner of the command, if found, or an empty string.
     */
    public String getOwnerName(Command command) {
        if (command instanceof PluginIdentifiableCommand) {
            return ((PluginIdentifiableCommand)command).getPlugin().getName();
        } else if (command instanceof VanillaCommandWrapper) {
            return "Minecraft";
        } else if (command instanceof BukkitCommand) {
            return "Bukkit";
        } else if (command instanceof SpigotCommand) {
            return "Spigot";
        } else {
            return "";
        }
    }

    /**
     * Gets the full label of the given command, including its fallback prefix if it has one.
     *
     * @param command The command to get the full label of.
     * @return The full label of the command.
     */
    public String getFullLabel(Command command) {
        // If the command label is already the full label then return that
        if (command.getLabel().contains(":")) {
            return command.getLabel();
        }

        String ownerName = getOwnerName(command);

        if (getOwnerName(command).isEmpty()) {
            return command.getName();
        } else {
            return ownerName.toLowerCase() + ":" + command.getName();
        }
    }
}
