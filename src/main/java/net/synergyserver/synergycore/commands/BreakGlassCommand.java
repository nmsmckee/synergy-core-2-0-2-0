package net.synergyserver.synergycore.commands;

import net.synergyserver.synergycore.StaffDashboard;
import org.bukkit.command.CommandSender;

@CommandDeclaration(
        commandName = "breakglass",
        permission = "syn.breakglass",
        usage = "/breakglass <true|false>",
        description = "TODO: REMOVE AND REPLACE",
        minArgs = 1,
        maxArgs = 1
)
public class BreakGlassCommand extends MainCommand {

    @Override
    public boolean execute(CommandSender sender, String[] args, CommandFlags flags) {
        boolean value;

        if (args[0].equalsIgnoreCase("true")) {
            value = true;
        } else if (args[0].equalsIgnoreCase("false")) {
            value = false;
        } else {
            sender.sendMessage("Check yourself before you wreck yourself");
            return false;
        }

        StaffDashboard.getInstance().memberWhitelist = value;
        sender.sendMessage("Member whitelist has been set to " + value);
        return true;
    }
}
