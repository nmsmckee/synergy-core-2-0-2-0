package net.synergyserver.synergycore.commands;

import net.synergyserver.synergycore.configs.Message;
import net.synergyserver.synergycore.utils.PlayerUtil;
import org.bukkit.Bukkit;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.UUID;

@CommandDeclaration(
        commandName = "feed",
        aliases = "sate",
        permission = "syn.feed",
        usage = "/feed [player]",
        description = "Sates yourself or another player.",
        maxArgs = 1
)
public class FeedCommand extends MainCommand {

    @Override
    public boolean execute(CommandSender sender, String[] args, CommandFlags flags) {
        Player target;
        // Target the sender if no player was provided
        if (args.length == 0) {
            // If the sender is not a player, then there needs to be a player specified
            if (!SenderType.getSenderType(sender).equals(SenderType.PLAYER)) {
                sender.sendMessage(Message.get("commands.error.wrong_sender_type"));
                return false;
            }

            target = (Player) sender;
        } else {
            // Give an error if the sender doesn't have permission to feed another player
            if (!sender.hasPermission("syn.feed.others")) {
                sender.sendMessage(Message.get("commands.feed.error.no_permission_others"));
                return false;
            }

            UUID targetID = PlayerUtil.getUUID(args[0], false, sender.hasPermission("vanish.see"));

            // Return error if player doesn't exist
            if (targetID == null) {
                sender.sendMessage(Message.format("commands.error.player_not_found", args[0]));
                return false;
            }

            target = Bukkit.getPlayer(targetID);
        }

        // Fill requested player's hunger bar
        target.setFoodLevel(20);
        target.setSaturation(10);
        target.setExhaustion(0);
        
        // Give the sender feedback
        if (sender.equals(target)) {
            sender.sendMessage(Message.format("commands.feed.info.self"));
        } else {
            sender.sendMessage(Message.format("commands.feed.info.others", target.getName()));
        }
        return true;
    }
}
