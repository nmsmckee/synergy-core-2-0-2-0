package net.synergyserver.synergycore.commands;

import net.synergyserver.synergycore.MemberApplication;
import net.synergyserver.synergycore.StaffDashboard;
import net.synergyserver.synergycore.SynergyCore;
import net.synergyserver.synergycore.configs.Message;
import net.synergyserver.synergycore.events.MemberApplicationSubmitEvent;
import net.synergyserver.synergycore.profiles.MinecraftProfile;
import net.synergyserver.synergycore.utils.PlayerUtil;
import net.synergyserver.synergycore.utils.TimeUtil;
import org.bukkit.Bukkit;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;

import java.util.UUID;

@CommandDeclaration(
        commandName = "apply",
        permission = "syn.apply",
        usage = "/apply",
        description = "Submits an application to staff that signals that your plot is ready to be evaluated for membership.",
        maxArgs = 0,
        validSenders = SenderType.PLAYER
)
public class ApplyCommand extends MainCommand {

    @Override
    public boolean execute(CommandSender sender, String[] args, CommandFlags flags) {
        Player player = (Player) sender;
        MinecraftProfile mcp = PlayerUtil.getProfile(player);
        UUID pID = player.getUniqueId();
        YamlConfiguration config = SynergyCore.getPluginConfig();

        // If the player has too little playtime, give them a message
        long minPlaytimeHours = config.getInt("applications.command.min_playtime_hours");
        long minPlaytime = TimeUtil.toRoundedUnit(TimeUtil.TimeUnit.HOUR, TimeUtil.TimeUnit.MILLI, minPlaytimeHours);

        // Update playtime values before checking
        mcp.updateTotalPlaytime();
        mcp.updateTotalAfktime();
        if (mcp.getTotalCalculatedPlaytime() < minPlaytime) {
            player.sendMessage(Message.get("commands.application.error.not_enough_playtime"));
            return false;
        }

        StaffDashboard staffDashboard = StaffDashboard.getInstance();

        // If the player already has an application in review, give them a message
        if (staffDashboard.hasPendingMemberApplication(pID)) {
            player.sendMessage(Message.get("commands.application.error.already_pending"));
            return false;
        }

        // If the player still has a cooldown, give them a message
        long daysCooldown = config.getInt("applications.command.cooldown_days");
        long cooldown = TimeUtil.toRoundedUnit(TimeUtil.TimeUnit.DAY, TimeUtil.TimeUnit.MILLI, daysCooldown);
        if (mcp.getLastApplied() != null && mcp.getLastApplied() + cooldown > System.currentTimeMillis()) {
            long cooldownLeft = (mcp.getLastApplied() + cooldown) - System.currentTimeMillis();
            String timeMessage = Message.getTimeMessage(
                    cooldownLeft, 3, 0,
                    Message.get("info_colored_lists.item_color_1"),
                    Message.get("info_colored_lists.grammar_color")
            );
            player.sendMessage(Message.format("commands.application.error.cooldown", timeMessage));
            return false;
        }

        // Create the MemberApplication
        MemberApplication application = new MemberApplication(pID, System.currentTimeMillis());

        // Emit an event
        MemberApplicationSubmitEvent event = new MemberApplicationSubmitEvent(application);
        Bukkit.getPluginManager().callEvent(event);

        // Save the application in the database and give the player feedback
        staffDashboard.saveMemberApplication(application);
        player.sendMessage(Message.get("commands.application.info.submit_successful"));
        mcp.setLastApplied(System.currentTimeMillis());
        return true;
    }

}
