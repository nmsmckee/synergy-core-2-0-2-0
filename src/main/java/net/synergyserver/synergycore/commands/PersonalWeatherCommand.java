package net.synergyserver.synergycore.commands;

import net.synergyserver.synergycore.WeatherType;
import net.synergyserver.synergycore.configs.Message;
import net.synergyserver.synergycore.profiles.WorldGroupProfile;
import net.synergyserver.synergycore.utils.PlayerUtil;
import net.synergyserver.synergycore.utils.WorldUtil;
import org.bukkit.Bukkit;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.UUID;

@CommandDeclaration(
        commandName = "personalweather",
        aliases = "pweather",
        permission = "syn.personalweather",
        usage = "/personalweather [sun|storm|reset] [player]",
        description = "Changes the player's local weather.",
        maxArgs = 2,
        validSenders = SenderType.PLAYER
)
public class PersonalWeatherCommand extends MainCommand {

    @Override
    public boolean execute(CommandSender sender, String[] args, CommandFlags flags) {
        Player player = (Player) sender;

        // If the player didn't specify a weather to set or another player then send the player their current weather
        if (args.length == 0) {
            if (player.getPlayerWeather() == null) {
                player.sendMessage(Message.format("commands.personalweather.see.info.self_matches",
                        WorldUtil.getWeather(player.getLocation()).getDisplayText(),
                        PlayerUtil.getProfile(player).getCurrentWorldGroupName()
                ));
                return true;
            }

            // If the personal weather is downfall then check whether it's snow or rain
            String weather = WeatherType.fromBukkitWeatherType(player.getPlayerWeather(), WorldUtil.wouldSnow(player.getLocation())).getDisplayText();
            player.sendMessage(Message.format("commands.personalweather.see.info.self_fixed",
                    weather,
                    PlayerUtil.getProfile(player).getCurrentWorldGroupName()
            ));
            return true;
        }

        // Determine who the target is
        Player target = player;
        if (args.length == 2) {
            // If the player doesn't have permission to change another player's personal weather then give an error
            if (!player.hasPermission("syn.personalweather.others.set")) {
                player.sendMessage("commands.personalweather.set.error.no_permission_others");
                return false;
            }

            UUID targetID = PlayerUtil.getUUID(args[1], false, player.hasPermission("vanish.see"));
            // Check if the target player exists
            if (targetID == null) {
                player.sendMessage(Message.format("commands.error.player_not_found", args[1]));
                return false;
            }
            target = Bukkit.getPlayer(targetID);
        }

        WorldGroupProfile wgp = PlayerUtil.getProfile(target).getCurrentWorldGroupProfile();

        // If the target's weather is being reset
        if (args[0].equalsIgnoreCase("reset")) {
            target.resetPlayerWeather();
            wgp.setPersonalWeather(null);

            // Give different feedback if the player is also the target
            if (player == target) {
                player.sendMessage(Message.format("commands.personalweather.set.info.reset_self", wgp.getWorldGroupName()));
            } else {
                player.sendMessage(Message.format("commands.personalweather.set.info.reset_other", target.getName(), wgp.getWorldGroupName()));
            }
            return true;
        }

        WeatherType weather = WeatherType.parseWeatherConstant(args[0]);

        // If the weather was unable to be parsed then give an error
        if (weather == null) {
            player.sendMessage(Message.format("commands.personalweather.error.invalid_weather_type", args[0]));
            return false;
        }

        // Set the target's weather
        target.setPlayerWeather(weather.toBukkitWeatherType());
        wgp.setPersonalWeather(weather);

        // Give different feedback if the player is also the target
        if (player == target) {
            player.sendMessage(Message.format("commands.personalweather.set.info.set_self", weather.getDisplayText(), wgp.getWorldGroupName()));
        } else {
            player.sendMessage(Message.format("commands.personalweather.set.info.set_other", target.getName(), weather.getDisplayText(), wgp.getWorldGroupName()));
        }
        return true;
    }
}
