package net.synergyserver.synergycore.commands;

import net.synergyserver.synergycore.configs.Message;
import net.synergyserver.synergycore.profiles.MinecraftProfile;
import net.synergyserver.synergycore.utils.PlayerUtil;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.HashSet;
import java.util.UUID;

@CommandDeclaration(
        commandName = "add",
        aliases = "a",
        permission = "syn.snoop.add",
        usage = "/snoop add <player>",
        description = "Starts snooping on the given player.",
        minArgs = 1,
        maxArgs = 1,
        validSenders = SenderType.PLAYER,
        parentCommandName = "snoop"
)
public class SnoopAddCommand extends SubCommand {

    @Override
    public boolean execute(CommandSender sender, String[] args, CommandFlags flags) {
        Player player = (Player) sender;
        MinecraftProfile mcp = PlayerUtil.getProfile(player);

        UUID pID = PlayerUtil.getUUID(args[0], true, sender.hasPermission("vanish.see"));

        // Give an error if the player can't be found
        if (pID == null) {
            player.sendMessage(Message.format("commands.error.player_not_found", args[0]));
            return false;
        }

        HashSet<UUID> snoopedPlayers = mcp.getSnoopedPlayers();

        // Give an error if the player already is in the list
        if (snoopedPlayers.contains(pID)) {
            player.sendMessage(Message.format("commands.snoop.add.error.already_snooped", PlayerUtil.getName(pID)));
            return false;
        }

        // Add the player to the snoop list
        snoopedPlayers.add(pID);
        mcp.setSnoopedPlayers(snoopedPlayers);
        player.sendMessage(Message.format("commands.snoop.add.info.added_player", PlayerUtil.getName(pID)));
        return true;
    }
}
