package net.synergyserver.synergycore.commands;

import net.synergyserver.synergycore.configs.Message;
import net.synergyserver.synergycore.utils.PlayerUtil;
import net.synergyserver.synergycore.utils.StringUtil;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.OfflinePlayer;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.SkullMeta;

import java.util.UUID;

@CommandDeclaration(
        commandName = "skull",
        permission = "syn.skull",
        usage = "/skull [player]",
        description = "Gets the skull of a player.",
        maxArgs = 1,
        validSenders = SenderType.PLAYER
)
public class SkullCommand extends MainCommand {

    @Override
    public boolean execute(CommandSender sender, String[] args, CommandFlags flags) {
        Player player = (Player) sender;
        UUID target = null;
        String targetName = null;

        if (args.length == 0) {
            target = player.getUniqueId();
        } else {
            // If the player doesn't have permission to get another player's skull then give an error
            if (!player.hasPermission("syn.skull.others")) {
                player.sendMessage(Message.format("commands.skull.error.no_permission_others"));
                return false;
            }

            if (!StringUtil.isWord(args[0])) {
                player.sendMessage(Message.format("commands.error.player_not_found", args[0]));
                return false;
            }

            // If the name matches an offline player, get their UUID, Otherwise fall back to legacy method
            OfflinePlayer matchedOfflinePlayer = Bukkit.getOfflinePlayer(args[0]);
            if (matchedOfflinePlayer != null && matchedOfflinePlayer.hasPlayedBefore()) {
                target = matchedOfflinePlayer.getUniqueId();
            } else {
                player.sendMessage(Message.format("commands.skull.warning.using_legacy", args[0]));
                targetName = args[0];
            }
        }

        // Create the skull item
        ItemStack targetSkull = new ItemStack(Material.PLAYER_HEAD, 1);
        SkullMeta meta = (SkullMeta) targetSkull.getItemMeta();

        if (target == null) {
            meta.setOwner(targetName);
        } else {
            meta.setOwningPlayer(Bukkit.getOfflinePlayer(target));
            targetName = PlayerUtil.getName(target);
        }
        meta.setDisplayName(Message.format("commands.skull.item_name", targetName));
        targetSkull.setItemMeta(meta);

        // Add the item to the player's inventory in the most convenient location
        ItemStack mainItem = player.getInventory().getItemInMainHand();
        if (mainItem == null || mainItem.getType().equals(Material.AIR)) {
            player.getInventory().setItemInMainHand(targetSkull);
        } else {
            player.getInventory().addItem(targetSkull);
        }

        // Give different feedback if the player is also the target
        if (player.getName().equalsIgnoreCase(targetName)) {
            player.sendMessage(Message.format("commands.skull.info.skull_given_self"));
        } else {
            player.sendMessage(Message.format("commands.skull.info.skull_given_other", targetName));
        }
        return true;
    }
}
