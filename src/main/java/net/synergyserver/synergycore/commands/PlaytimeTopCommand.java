package net.synergyserver.synergycore.commands;

import dev.morphia.Datastore;
import dev.morphia.aggregation.experimental.expressions.Expressions;
import dev.morphia.aggregation.experimental.expressions.MathExpressions;
import dev.morphia.aggregation.experimental.stages.Sort;
import dev.morphia.query.MorphiaCursor;
import net.synergyserver.synergycore.SynergyCore;
import net.synergyserver.synergycore.configs.Message;
import net.synergyserver.synergycore.database.MongoDB;
import net.synergyserver.synergycore.profiles.MinecraftProfile;
import net.synergyserver.synergycore.profiles.WorldGroupProfile;
import net.synergyserver.synergycore.utils.PlayerUtil;
import org.bukkit.Bukkit;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.LinkedHashMap;

import static dev.morphia.aggregation.experimental.stages.Projection.project;
import static dev.morphia.query.experimental.filters.Filters.eq;

@CommandDeclaration(
        commandName = "top",
        aliases = "leaderboard",
        permission = "syn.playtime.top",
        usage = "/playtime top [-raw] [-worldgroup <worldgroup>]",
        description = "Displays the top 10 people in playtime.",
        maxArgs = 0,
        parseCommandFlags = true,
        parentCommandName = "playtime"
)
public class PlaytimeTopCommand extends SubCommand {

    @Override
    public boolean execute(CommandSender sender, String[] args, CommandFlags flags) {
        // First check flags to see if afktime should not be subtracted, or if it should be limited to one world group
        boolean isRaw = false;
        boolean singleWorldGroup = false;
        String worldGroupName = null;
        if (flags.hasFlag("-r", "-raw")) {
            isRaw = true;
        }
        if (flags.hasFlag("-w", "-world", "-wg", "-worldgroup")) {
            singleWorldGroup = true;
            worldGroupName = flags.getFlagValue("-w", "-world", "-wg", "-worldgroup");
        }

        // Check if the world group name provided is valid
        if (singleWorldGroup && worldGroupName.isEmpty()) {
            sender.sendMessage(Message.format("commands.error.invalid_world_group_name", ""));
            return false;
        }

        String correctedWorldGroupName = null;
        if (singleWorldGroup) {
            correctedWorldGroupName = SynergyCore.getExactWorldGroupName(worldGroupName);
        }
        if (singleWorldGroup && !SynergyCore.hasWorldGroupName(correctedWorldGroupName)) {
            sender.sendMessage(Message.format("commands.error.invalid_world_group_name", worldGroupName));
            return false;
        }

        // Update the playtime of all online players in the database before querying it
        for (Player player : Bukkit.getOnlinePlayers()) {
            MinecraftProfile mcp = PlayerUtil.getProfile(player);
            mcp.updateTotalPlaytime();
            mcp.updateTotalAfktime();
        }

        Datastore ds = MongoDB.getInstance().getDatastore();

        LinkedHashMap<String, Long> playtimes = new LinkedHashMap<>();

        // Fetch the top 10 profiles that have the data for the required fields
        if (singleWorldGroup) {
            if (isRaw) {
                // Get the top 10 WorldGroupProfiles

                MorphiaCursor<WorldGroupProfile> wgps = ds.aggregate(WorldGroupProfile.class)
                        .match(
                                eq("n", correctedWorldGroupName)
                        )
                        .project(project()
                                .include("pid")
                                .include("pt")
                        )
                        .sort(Sort.sort()
                                .descending("pt")
                        )
                        .limit(10)
                        .execute(WorldGroupProfile.class);

                // Store the results
                while (wgps.hasNext()) {
                    WorldGroupProfile wgp = wgps.next();
                    playtimes.put(PlayerUtil.getName(wgp.getPlayerID()), wgp.getPlaytime());
                }

                wgps.close();
            } else {
                // Get the top 10 WorldGroupProfiles
                MorphiaCursor<WorldGroupProfile> wgps = ds.aggregate(WorldGroupProfile.class)
                        .match(
                                eq("n", correctedWorldGroupName)
                        )
                        .project(project()
                                .include("pid")
                                .include("pt", MathExpressions.subtract(
                                            Expressions.field("pt"),
                                            Expressions.field("at")
                                        )
                                )

                        )
                        .sort(Sort.sort()
                                .descending("pt")
                        )
                        .limit(10)
                        .execute(WorldGroupProfile.class);

                // Store the results
                while (wgps.hasNext()) {
                    WorldGroupProfile wgp = wgps.next();
                    playtimes.put(PlayerUtil.getName(wgp.getPlayerID()), wgp.getPlaytime());
                }

                wgps.close();
            }
        } else {
            if (isRaw) {
                // Get the top 10 MinecraftProfiles
                MorphiaCursor<MinecraftProfile> mcps = ds.aggregate(MinecraftProfile.class)
                        .project(project()
                                .include("tpt")
                        )
                        .sort(Sort.sort()
                                .descending("tpt")
                        )
                        .limit(10)
                        .execute(MinecraftProfile.class);

                // Store the results
                while (mcps.hasNext()) {
                    MinecraftProfile mcp = mcps.next();
                    playtimes.put(PlayerUtil.getName(mcp.getID()), mcp.getTotalPlaytime());
                }

                mcps.close();
            } else {
                // Get the top 10 MinecraftProfiles
                MorphiaCursor<MinecraftProfile> mcps = ds.aggregate(MinecraftProfile.class)
                        .project(
                                project().include("tpt", MathExpressions.subtract(
                                        Expressions.field("tpt"),
                                        Expressions.field("tat"))
                                )
                        )
                        .sort(Sort.sort()
                                .descending("tpt")
                        )
                        .limit(10)
                        .execute(MinecraftProfile.class);

                // Store the results
                while (mcps.hasNext()) {
                    MinecraftProfile mcp = mcps.next();
                    playtimes.put(PlayerUtil.getName(mcp.getID()), mcp.getTotalPlaytime());
                }

                mcps.close();
            }
        }

        // Send the header
        String modifier = "";
        if (singleWorldGroup) {
            modifier += Message.format("commands.playtime.single_world_group_modifier", correctedWorldGroupName);
        }
        if (isRaw) {
            modifier += Message.format("commands.playtime.raw_modifier");
        }
        sender.sendMessage(Message.format("commands.playtime.top.header", modifier));

        // Send the top 10
        int i = 1;
        for (String name : playtimes.keySet()) {
            String timeMessage = Message.getTimeMessage(
                    playtimes.get(name), 3, 1,
                    Message.get("info_colored_lists.item_color_1"),
                    Message.get("info_colored_lists.grammar_color")
            );
            sender.sendMessage(Message.format("commands.playtime.top.item", i, name, timeMessage));

            // Increment the index
            i++;
        }
        return true;
    }
}
