package net.synergyserver.synergycore.commands;

import net.synergyserver.synergycore.SynergyCore;
import net.synergyserver.synergycore.SynergyPlugin;
import net.synergyserver.synergycore.configs.Message;
import org.bukkit.command.CommandSender;

@CommandDeclaration(
        commandName = "synreload",
        aliases = {"sreload", "synreboot", "sreboot"},
        permission = "syn.synreload",
        usage = "/synreload [plugin]",
        description = "Reloads the configuration of a SynergySuite plugin.",
        maxArgs = 1
)
public class SynReloadCommand extends MainCommand {

    @Override
    public boolean execute(CommandSender sender, String[] args, CommandFlags flags) {
        SynergyCore core = SynergyCore.getPlugin();

        if (args.length == 1) {
            // Attempt to parse the SynergySuite plugin from the given argument
            SynergyPlugin plugin = core.getSynergyPlugin(args[0]);

            // Give an error if a plugin couldn't be parsed
            if (plugin == null) {
                sender.sendMessage(Message.format("commands.synreload.error.no_plugin_found", args[0]));
                return false;
            }

            // Attempt to reload the plugin and give the sender feedback
            if (core.reloadSynergyPlugin(plugin)) {
                sender.sendMessage(Message.format("commands.synreload.info.success", plugin.getName()));
                return true;
            } else {
                sender.sendMessage(Message.format("commands.synreload.error.error_reloading", plugin.getName()));
                return false;
            }
        } else {
            // Reload all SynergySuite plugins
            for (SynergyPlugin plugin : core.getSynergyPlugins()) {
                // Give feedback for each plugin
                if (core.reloadSynergyPlugin(plugin)) {
                    sender.sendMessage(Message.format("commands.synreload.info.success", plugin.getName()));
                } else {
                    sender.sendMessage(Message.format("commands.synreload.error.error_reloading", plugin.getName()));
                }
            }
            return true;
        }
    }
}
