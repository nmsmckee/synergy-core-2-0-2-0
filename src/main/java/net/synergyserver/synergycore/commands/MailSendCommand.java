package net.synergyserver.synergycore.commands;

import net.synergyserver.synergycore.Mail;
import net.synergyserver.synergycore.PrivateMessageType;
import net.synergyserver.synergycore.configs.Message;
import net.synergyserver.synergycore.database.DataManager;
import net.synergyserver.synergycore.events.MailSendEvent;
import net.synergyserver.synergycore.profiles.MinecraftProfile;
import net.synergyserver.synergycore.utils.PlayerUtil;
import org.apache.commons.lang.ArrayUtils;
import org.bukkit.Bukkit;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.UUID;

@CommandDeclaration(
        commandName = "send",
        aliases = "s",
        permission = "syn.mail.send",
        usage = "/mail send <player> <message>",
        description = "Sends a player mail.",
        minArgs = 2,
        validSenders = SenderType.PLAYER,
        parentCommandName = "mail"
)
public class MailSendCommand extends SubCommand {

    @Override
    public boolean execute(CommandSender sender, String[] args, CommandFlags flags) {
        Player mailSender = (Player) sender;
        UUID receiverID = PlayerUtil.getUUID(args[0], true, sender.hasPermission("vanish.see"));
        // If no player was found then give the player an error
        if (receiverID == null) {
            mailSender.sendMessage(Message.format("commands.error.player_not_found", args[0]));
            return false;
        }

        // Use the live MCP if possible
        MinecraftProfile receiverMCP;
        if (PlayerUtil.isOnline(receiverID)) {
            receiverMCP = PlayerUtil.getProfile(receiverID);
        } else {
            receiverMCP = DataManager.getInstance().getPartialDataEntity(MinecraftProfile.class, receiverID, "ig", "ml");
        }

        // Create the mail
        String message = String.join(" ", (String[]) ArrayUtils.remove(args, 0));

        Mail mail = new Mail(mailSender.getUniqueId(), receiverID, System.currentTimeMillis(), message, PrivateMessageType.NORMAL);
        MailSendEvent event = new MailSendEvent(mail);
        Bukkit.getPluginManager().callEvent(event);

        // Check if the event was cancelled before continuing
        if (event.isCancelled()) {
            return true;
        }

        // Add the mail to the receiver's profile
        receiverMCP.addMail(mail);
        mailSender.sendMessage(Message.format("commands.mail.send.info.mail_sent", PlayerUtil.getName(mail.getReceiver()), message));

        if (receiverMCP.isOnline()) {
            receiverMCP.getPlayer().sendMessage(Message.format("commands.mail.send.info.mail_received", mail.getSenderName()));
        }
        return true;
    }
}
