package net.synergyserver.synergycore.commands;

import net.synergyserver.synergycore.Warp;
import net.synergyserver.synergycore.configs.Message;
import net.synergyserver.synergycore.database.DataManager;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.List;

@CommandDeclaration(
        commandName = "delete",
        aliases = {"unset", "remove"},
        permission = "syn.warp.delete",
        usage = "/warp delete <warp>",
        description = "Deletes a warp.",
        minArgs = 1,
        maxArgs = 1,
        validSenders = SenderType.PLAYER,
        parentCommandName = "warp"
)
public class WarpDeleteCommand extends SubCommand {

    @Override
    public boolean execute(CommandSender sender, String[] args, CommandFlags flags) {
        Player player = (Player) sender;

        // Attempt to get the targeted warp
        String name = args[0];
        List<Warp> warps = Warp.getWarps(name, player.getUniqueId());

        // Return if an error was encountered
        if (warps == null) {
            return false;
        }

        // If they don't have a warp with the given name then give them an error
        if (warps.size() == 0) {
            player.sendMessage(Message.format("commands.warp.error.warp_not_found", name));
            return false;
        }

        Warp warp = warps.get(0);

        // Delete the warp from the database and give the player feedback
        DataManager.getInstance().deleteDataEntity(warp);
        player.sendMessage(Message.format("commands.warp.delete.info.deleted_warp", warp.getName()));
        return true;
    }
}
