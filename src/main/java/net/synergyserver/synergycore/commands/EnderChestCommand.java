package net.synergyserver.synergycore.commands;

import net.synergyserver.synergycore.configs.Message;
import net.synergyserver.synergycore.utils.PlayerUtil;
import org.bukkit.Bukkit;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.UUID;

@CommandDeclaration(
        commandName = "enderchest",
        aliases = {"endersee", "ec"},
        permission = "syn.enderchest",
        usage = "/enderchest [player]",
        description = "Opens your enderchest or another player's.",
        maxArgs = 1,
        validSenders = SenderType.PLAYER
)
public class EnderChestCommand extends MainCommand {

    @Override
    public boolean execute(CommandSender sender, String[] args, CommandFlags flags) {
        Player player = (Player) sender;

        // Open the player's own enderchest if another player isn't specified
        if (args.length == 0) {
            player.openInventory(player.getEnderChest());
            return true;
        }

        // Give an error if the sender doesn't have permission to open another player's ender chest
        if (!sender.hasPermission("syn.enderchest.others")) {
            sender.sendMessage(Message.get("commands.enderchest.error.no_permission_others"));
            return false;
        }

        UUID targetID = PlayerUtil.getUUID(args[0], false, sender.hasPermission("vanish.see"));

        // If no player was found then give the sender an error message
        if (targetID == null) {
            sender.sendMessage(Message.format("commands.error.player_not_found", args[0]));
            return false;
        }

        // Open the requested player's enderchest
        player.openInventory(Bukkit.getPlayer(targetID).getEnderChest());
        return true;
    }
}
