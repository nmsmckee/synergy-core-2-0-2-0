package net.synergyserver.synergycore.commands;

import net.synergyserver.synergycore.PowerTool;
import net.synergyserver.synergycore.configs.Message;
import net.synergyserver.synergycore.profiles.MinecraftProfile;
import net.synergyserver.synergycore.utils.ItemUtil;
import net.synergyserver.synergycore.utils.PlayerUtil;
import org.bukkit.Material;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import java.util.HashMap;

@CommandDeclaration(
        commandName = "add",
        aliases = "a",
        permission = "syn.powertool.add",
        usage = "/powertool add <command name>",
        description = "Binds a command to the current held item to later be activated by left-clicking.",
        minArgs = 1,
        validSenders = SenderType.PLAYER,
        parentCommandName = "powertool"
)
public class PowerToolAddCommand extends SubCommand {

    @Override
    public boolean execute(CommandSender sender, String[] args, CommandFlags flags) {
        Player player = (Player) sender;
        MinecraftProfile mcp = PlayerUtil.getProfile(player);
        ItemStack item = player.getInventory().getItemInMainHand();

        // Give an error if the player isn't holding an item
        if (item == null || item.getType().equals(Material.AIR)) {
            player.sendMessage(Message.get("commands.powertool.error.no_item"));
            return false;
        }

        String command = String.join(" ", args);

        // Give an error if a command wasn't provided
        if (!command.startsWith("/")) {
            player.sendMessage(Message.format("commands.powertool.add.error.not_a_command", command));
            return false;
        }

        String powerToolKey = ItemUtil.itemTypeToString(item);
        HashMap<String, PowerTool> powerTools = mcp.getPowerTools();

        // If a powertool already exists for that material then modify it, otherwise create a new one
        if (powerTools.get(powerToolKey) != null) {
            powerTools.get(powerToolKey).addCommand(command);
            mcp.setPowerTools(powerTools);
            player.sendMessage(Message.format("commands.powertool.add.info.modified_powertool", command));
        } else {
            mcp.setPowerTools(powerTools);
            powerTools.put(powerToolKey, new PowerTool(item.getType(), item.getDurability(), command));
            mcp.setPowerTools(powerTools);
            player.sendMessage(Message.format("commands.powertool.add.info.created_powertool", command));
        }
        return true;
    }
}
