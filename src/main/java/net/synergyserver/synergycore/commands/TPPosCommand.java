package net.synergyserver.synergycore.commands;

import net.synergyserver.synergycore.configs.Message;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.WorldBorder;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.event.player.PlayerTeleportEvent;

@CommandDeclaration(
        commandName = "tppos",
        aliases = {"tpp", "transpacificpartnership"},
        permission = "syn.tppos",
        usage = "/tppos <x> <y> <z> [yaw] [pitch]",
        description = "Teleports you to a specific location",
        minArgs = 3,
        maxArgs = 5,
        validSenders = SenderType.PLAYER
)

public class TPPosCommand extends MainCommand{

    @Override
    public boolean execute(CommandSender sender, String[] args, CommandFlags flags) {
        Player player = (Player) sender;
        World world = player.getWorld();

        double x = Double.parseDouble(args[0]);
        double y = Double.parseDouble(args[1]);
        double z = Double.parseDouble(args[2]);

        WorldBorder border = world.getWorldBorder();
        double borderRadius = border.getSize()/2;

        if (    x > border.getCenter().getX() + borderRadius ||
                x < border.getCenter().getX() - borderRadius ||
                z > border.getCenter().getZ() + borderRadius ||
                z < border.getCenter().getZ() - borderRadius   ) {
            player.sendMessage(Message.format("commands.teleportation.error.out_of_bounds",x,y,z));
            return false;
        }

        Location destination = new Location(world, x, y, z);

        if(args.length > 3){
            float yaw = Float.parseFloat(args[3]);
            if(args.length == 5) {
                float pitch = Float.parseFloat(args[4]);
                destination.setPitch(pitch);
            }
            destination.setYaw(yaw);
        }

        player.teleport(destination, PlayerTeleportEvent.TeleportCause.COMMAND);
        player.sendMessage(Message.format("commands.teleportation.info.teleporting_location", x, y, z));
        return true;
    }
}
