package net.synergyserver.synergycore.commands;

import net.synergyserver.synergycore.configs.Message;
import net.synergyserver.synergycore.utils.PlayerUtil;
import org.bukkit.Bukkit;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.Arrays;
import java.util.UUID;

@CommandDeclaration(
        commandName = "command",
        aliases = "c",
        permission = "syn.sudo.command",
        usage = "/sudo command <player> <command name>",
        description = "Forces a player to execute a command.",
        minArgs = 2,
        parentCommandName = "sudo"
)
public class SudoCommandCommand extends SubCommand {

    @Override
    public boolean execute(CommandSender sender, String[] args, CommandFlags flags) {
        // Verify that the player specified is valid
        UUID pID = PlayerUtil.getUUID(args[0], false, sender.hasPermission("vanish.see"));
        if (pID == null) {
            sender.sendMessage(Message.format("commands.error.player_not_found", args[0]));
            return false;
        }

        // Get the player
        Player sudoedPlayer = Bukkit.getPlayer(pID);

        // Check if the targeted player is unable to be sudoed
        if (sudoedPlayer.hasPermission("syn.sudo.exempt")) {
            sender.sendMessage(Message.format("commands.sudo.error.exempt", sudoedPlayer.getName()));
            return false;
        }

        // Assemble the command
        String command = String.join(" ", Arrays.asList(args).subList(1, args.length));

        // Execute the command and give feedback
        Bukkit.dispatchCommand(sudoedPlayer, command);
        sender.sendMessage(Message.format("commands.sudo.command.info.success", sudoedPlayer.getName(), command));
        return true;
    }
}
