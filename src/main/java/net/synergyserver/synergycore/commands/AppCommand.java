package net.synergyserver.synergycore.commands;

import org.bukkit.command.CommandSender;

@CommandDeclaration(
        commandName = "app",
        aliases = "apps",
        permission = "syn.app",
        usage = "/app <list|accept|deny>",
        description = "This is the main command to manage Member applications."
)
public class AppCommand extends MainCommand {

    @Override
    public boolean execute(CommandSender sender, String[] args, CommandFlags flags) {
        return false;
    }
}
