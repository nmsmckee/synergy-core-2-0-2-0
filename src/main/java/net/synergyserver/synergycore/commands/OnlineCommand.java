package net.synergyserver.synergycore.commands;

import org.bukkit.command.CommandSender;

@CommandDeclaration(
        commandName = "online",
        aliases = {"list"},
        permission = "syn.online",
        usage = "/online <discord|dubtrack>",
        description = "Lists the users online on a specific service.",
        validSenders = {SenderType.PLAYER, SenderType.CONSOLE}
)
public class OnlineCommand extends MainCommand {

    @Override
    public boolean execute(CommandSender sender, String[] args, CommandFlags flags) {
        return false;
    }
}
