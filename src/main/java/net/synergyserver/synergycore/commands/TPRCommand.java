package net.synergyserver.synergycore.commands;

import net.synergyserver.synergycore.Teleport;
import net.synergyserver.synergycore.TeleportType;
import net.synergyserver.synergycore.configs.Message;
import net.synergyserver.synergycore.events.TeleportRequestSendEvent;
import net.synergyserver.synergycore.utils.PlayerUtil;
import org.bukkit.Bukkit;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.UUID;

@CommandDeclaration(
        commandName = "tpr",
        aliases = {"tpa", "tpask", "tprequest"},
        permission = "syn.tpr",
        usage = "/tpr <player>",
        description = "Requests a player to teleport to them.",
        minArgs = 1,
        maxArgs = 1,
        validSenders = SenderType.PLAYER
)
public class TPRCommand extends MainCommand {

    @Override
    public boolean execute(CommandSender sender, String[] args, CommandFlags flags) {
        Player player = (Player) sender;
        UUID receiverID = PlayerUtil.getUUID(args[0], false, player.hasPermission("vanish.see"));

        // If no player was found then give the sender an error message
        if (receiverID == null) {
            player.sendMessage(Message.format("commands.error.player_not_found", args[0]));
            return false;
        }

        Player receiver = Bukkit.getPlayer(receiverID);

        // Emit an event
        Teleport teleport = new Teleport(player.getUniqueId(), receiverID,
                receiver.getLocation(), TeleportType.TO_RECEIVER);
        TeleportRequestSendEvent event = new TeleportRequestSendEvent(teleport);
        Bukkit.getPluginManager().callEvent(event);

        // Check if the event was cancelled before continuing
        if (event.isCancelled()) {
            // Even though we're exiting this method early, technically the command was still successful.
            return true;
        }

        // Actually send the teleport request now
        PlayerUtil.getProfile(receiverID).setPendingTeleport(event.getTeleport());

        // Give both parties the appropriate feedback
        player.sendMessage(Message.format("commands.teleportation.info.request_sent", receiver.getName()));
        receiver.sendMessage(Message.format("commands.teleportation.info.pending_tpr_request", player.getName()));

        return true;
    }

}
