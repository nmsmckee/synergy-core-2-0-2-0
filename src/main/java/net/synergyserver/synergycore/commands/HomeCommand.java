package net.synergyserver.synergycore.commands;

import net.synergyserver.synergycore.configs.Message;
import net.synergyserver.synergycore.profiles.MinecraftProfile;
import net.synergyserver.synergycore.settings.CoreToggleSetting;
import net.synergyserver.synergycore.utils.PlayerUtil;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

@CommandDeclaration(
        commandName = "home",
        aliases = {"homes"},
        permission = "syn.home",
        usage = "/home <tp|create|delete|list|home>",
        description = "Main command for managing your homes.",
        minArgs = 1,
        validSenders = SenderType.PLAYER,
        executeIfInvalidSubCommand = true
)
public class HomeCommand extends MainCommand {

    @Override
    public boolean execute(CommandSender sender, String[] args, CommandFlags flags) {
        HomeTPCommand homeTPCommand = (HomeTPCommand) getSubCommands().get("tp");
        MinecraftProfile mcp = PlayerUtil.getProfile((Player) sender);


        // If the player has the setting enabled...
        if (CoreToggleSetting.HOME_SHORTCUT.getValue(mcp)){
            // Default to teleporting the player to their home, if they have permission to
            if (args.length > 0 && sender.hasPermission(homeTPCommand.getPermission())) {
                return homeTPCommand.validate(sender, args);
            }
        }

        // Otherwise give them the correct usage and return false
        sender.sendMessage(Message.format("commands.error.incorrect_syntax", getUsage()));
        return false;
    }
}
