package net.synergyserver.synergycore.commands;

import net.synergyserver.synergycore.configs.Message;
import net.synergyserver.synergycore.database.DataManager;
import net.synergyserver.synergycore.profiles.MinecraftProfile;
import net.synergyserver.synergycore.profiles.SynUser;
import net.synergyserver.synergycore.utils.PlayerUtil;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

@CommandDeclaration(
        commandName = "dubtrack",
        permission = "syn.dubtrack",
        usage = "/dubtrack",
        description = "Provides a link to the Dubtrack room if your account is already connected. " +
                "Otherwise, prints information on how to connect your account",
        maxArgs = 0,
        validSenders = SenderType.PLAYER
)
public class DubtrackCommand extends MainCommand {

    @Override
    public boolean execute(CommandSender sender, String[] args, CommandFlags flags) {
        Player player = (Player) sender;
        MinecraftProfile mcp = PlayerUtil.getProfile(player);
        SynUser synUser = DataManager.getInstance().getDataEntity(SynUser.class, mcp.getSynID());

        if (synUser.getDubtrackID() == null) {
            sender.sendMessage(Message.format("commands.dubtrack.info.account_not_connected"));
            return true;
        }

        sender.sendMessage(Message.format("commands.dubtrack.info.account_connected"));
        return true;
    }
}
