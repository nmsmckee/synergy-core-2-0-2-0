package net.synergyserver.synergycore.commands;

import org.bukkit.command.CommandSender;

@CommandDeclaration(
        commandName = "subscriptionstatus",
        aliases = "ss",
        permission = "syn.donator.subscription-status",
        usage = "/donator subscriptionstatus <get|set>",
        description = "Main command for managing donation subscription statuses.",
        parentCommandName = "donator"
)
public class DonatorSubscriptionStatusCommand extends SubCommand {

    @Override
    public boolean execute(CommandSender sender, String[] args, CommandFlags flags) {
        return false;
    }
}
