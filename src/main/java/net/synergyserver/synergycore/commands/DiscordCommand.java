package net.synergyserver.synergycore.commands;

import net.synergyserver.synergycore.configs.Message;
import net.synergyserver.synergycore.database.DataManager;
import net.synergyserver.synergycore.profiles.MinecraftProfile;
import net.synergyserver.synergycore.profiles.SynUser;
import net.synergyserver.synergycore.utils.PlayerUtil;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

@CommandDeclaration(
        commandName = "discord",
        permission = "syn.discord",
        usage = "/discord",
        description = "Provides a link to the Discord server if your account is already connected. " +
                "Otherwise, prints information on how to connect your account",
        maxArgs = 0,
        validSenders = SenderType.PLAYER
)
public class DiscordCommand extends MainCommand {

    @Override
    public boolean execute(CommandSender sender, String[] args, CommandFlags flags) {
        Player player = (Player) sender;
        MinecraftProfile mcp = PlayerUtil.getProfile(player);
        SynUser synUser = DataManager.getInstance().getDataEntity(SynUser.class, mcp.getSynID());

        if (synUser.getDiscordID() == null) {
            sender.sendMessage(Message.format("commands.discord.info.account_not_connected"));
            return true;
        }

        sender.sendMessage(Message.format("commands.discord.info.account_connected"));
        return true;
    }
}
