package net.synergyserver.synergycore.commands;

import net.synergyserver.synergycore.SynergyPlugin;
import org.bukkit.command.CommandSender;
import org.bukkit.plugin.Plugin;

import java.util.HashMap;
import java.util.List;

/**
 * Represents a command that is handled by SynergyCore's custom command system.
 */
public interface SynCommand {

    /**
     * Registers this command to the given <code>SynergyPlugin</code> and populates the fields with values
     * from its implementation's <code>CommandDeclaration</code>.
     *
     * @param plugin The owning plugin.
     */
    void populate(SynergyPlugin plugin);

    /**
     * Registers a <code>SubCommand</code> object to be handled by this <code>SynCommand</code> when it's called.
     *
     * @param subCommand The <code>SubCommand</code> to register.
     */
    void registerSubCommand(SubCommand subCommand);

    /**
     * Gets the plugin that is the owner of this command. Used for grouping by help features.
     *
     * @return The <code>SynergyPlugin</code> that owns this command.
     */
    Plugin getPlugin();

    /**
     * Gets the name of this command.
     *
     * @return The command's name.
     */
    String getCommandName();

    /**
     * Gets the aliases of this command.
     *
     * @return A list containing the aliases of this command.
     */
    List<String> getAliases();

    /**
     * Gets the required permission to use this command.
     *
     * @return The permission node to use this command.
     */
    String getPermission();

    /**
     * Gets the correct syntax for using this command.
     *
     * @return The correct syntax for this command,
     */
    String getUsage();

    /**
     * Gets the description of what this command does.
     *
     * @return The description of this command.
     */
    String getDescription();

    /**
     * Gets the minimum amount of arguments this command will handle.
     *
     * @return The min number of arguments
     */
    int getMinArgs();

    /**
     * Gets the maximum amount of arguments this command will handle.
     *
     * @return The max number of arguments.
     */
    int getMaxArgs();

    /**
     * Gets the allowed types of senders that can use this command.
     *
     * @return A list containing <code>SenderType</code> enums that are allowed to use this command.
     */
    List<SenderType> getValidSenders();

    /**
     * Checks whether <code>CommandFlags</code> should be parsed for this command.
     *
     * @return True if <code>CommandFlags</code> should be parsed.
     */
    boolean shouldParseCommandFlags();

    /**
     * Checks whether this command's code should run if it has <code>SubCommand</code>s but a bad subcommand is given.
     *
     * @return True if this command should run.
     */
    boolean isExecutedIfInvalidSubCommand();

    /**
     * Gets the HashMap containing all <code>SubCommand</code>s of this <code>SynCommand</code>.
     *
     * @return The <code>SubCommand</code>s of this <code>SynCommand</code>.
     */
    HashMap<String, SubCommand> getSubCommands();

    /**
     * Preforms an initial check to see if the command should be run. If the command is prevented to be executed
     * for any reason, the sender should be sent an appropriate error message.
     *
     * @param sender The sender of the command.
     * @param args An array containing the arguments used with the command.
     * @return True if the command was successful in preforming its action.
     */
    boolean validate(CommandSender sender, String[] args);

    /**
     * Executes the code of this command. Before any other actions are preformed, a final check should be done at the
     * beginning of this method that verifies that the given arguments are valid. If they're not valid, then the
     * sender should be sent an error message containing the correct syntax of the command.
     *
     * @param sender The sender of the command.
     * @param args An array containing the arguments used with the command.
     * @param flags A map of flags and their values that were given in the arguments of the command.
     * @return True if the command was successful in preforming its action.
     */
    boolean execute(CommandSender sender, String[] args, CommandFlags flags);

}