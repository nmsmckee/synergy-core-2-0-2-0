package net.synergyserver.synergycore.commands;

import net.synergyserver.synergycore.ChatColor;
import net.synergyserver.synergycore.SerializableLocation;
import net.synergyserver.synergycore.SynergyCore;
import net.synergyserver.synergycore.configs.Message;
import net.synergyserver.synergycore.profiles.MinecraftProfile;
import net.synergyserver.synergycore.profiles.WorldGroupProfile;
import net.synergyserver.synergycore.utils.PlayerUtil;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.event.player.PlayerTeleportEvent;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

@CommandDeclaration(
        commandName = "tp",
        aliases = {"goto", "warp"},
        permission = "syn.home.tp",
        usage = "/home tp <home> [worldgroup]",
        description = "Teleports you to the home with the given name. If a world group is specified, then this " +
                "command tries to teleport you to the home with that name in that world group.",
        minArgs = 1,
        maxArgs = 2,
        validSenders = SenderType.PLAYER,
        parentCommandName = "home"
)
public class HomeTPCommand extends SubCommand {

    @Override
    public boolean execute(CommandSender sender, String[] args, CommandFlags flags) {
        Player player = (Player) sender;
        MinecraftProfile mcp = PlayerUtil.getProfile(player);

        String name = args[0].toLowerCase();

        // If no world group is specified, search through all world groups
        if (args.length == 1) {
            // Find all the world groups that have a home with the specified name
            List<String> matchedWorldGroups = new ArrayList<>();
            for (WorldGroupProfile wgp : mcp.getPartialWorldGroupProfiles("n", "hs")) {
                HashMap<String, SerializableLocation> homes = wgp.getHomes();

                // Ignore this world group if it doesn't have a home with a matching name
                if (homes == null || !homes.containsKey(name)) {
                    continue;
                }

                matchedWorldGroups.add(ChatColor.AQUA + wgp.getWorldGroupName());
            }

            if (matchedWorldGroups.size() == 0) {
                // If no homes were found and they entered "bed" then teleport them to their bed
                if (name.equals("bed") && player.getBedSpawnLocation() != null) {
                    boolean success = player.teleport(player.getBedSpawnLocation(), PlayerTeleportEvent.TeleportCause.COMMAND);
                    if (success) {
                        player.sendMessage(Message.format("commands.home.tp.info.teleported_to_bed", name));
                    }
                    return true;
                }

                // Otherwise send them an error message
                player.sendMessage(Message.format("commands.home.tp.error.home_not_found", name));
                return false;
            }

            // If multiple homes were found then ask the player to specify which world group they mean
            if (matchedWorldGroups.size() > 1) {
                String matchedWorldGroupsList = Message.createFormattedList(
                        matchedWorldGroups,
                        Message.get("info_colored_lists.item_color_1"),
                        Message.get("info_colored_lists.item_color_2"),
                        Message.get("info_colored_lists.grammar_color")
                );
                player.sendMessage(Message.format("commands.home.tp.info.multiple_homes_found", name, matchedWorldGroupsList));
                player.sendMessage(Message.format("commands.home.tp.info.please_specify_world_group", name));
                return true;
            }

            // Otherwise teleport them to the found home and give them feedback
            String worldGroupName = matchedWorldGroups.get(0).replaceFirst(ChatColor.AQUA.toString(), "");
            WorldGroupProfile wgp = mcp.getPartialWorldGroupProfile(worldGroupName, "hs");
            boolean success = player.teleport(wgp.getHomes().get(name).getLocation(), PlayerTeleportEvent.TeleportCause.COMMAND);
            if (success) {
                player.sendMessage(Message.format("commands.home.tp.info.teleported", name));
            }
            return true;
        }

        // Otherwise use the specified world group
        String worldGroupName = SynergyCore.getExactWorldGroupName(args[1]);

        // If the world group specified is invalid then give the player an error
        if (!SynergyCore.hasWorldGroupName(worldGroupName)) {
            player.sendMessage(Message.format("commands.error.invalid_world_group_name", args[1]));
            return false;
        }

        // If they don't have data for the world group then give the player a message
        if (!mcp.hasWorldGroupProfile(worldGroupName)) {
            player.sendMessage(Message.format("commands.home.tp.error.home_not_found", name));
            return false;
        }

        WorldGroupProfile wgp = mcp.getPartialWorldGroupProfile(worldGroupName, "hs");
        HashMap<String, SerializableLocation> homes = wgp.getHomes();

        // If they don't have a home with that name then give the player a message
        if (homes == null || !homes.containsKey(name)) {
            player.sendMessage(Message.format("commands.home.tp.error.home_not_found", name));
            return false;
        }

        // Otherwise teleport them to the found home and give them feedback
        boolean success = player.teleport(homes.get(name).getLocation(), PlayerTeleportEvent.TeleportCause.COMMAND);
        if (success) {
            player.sendMessage(Message.format("commands.home.tp.info.teleported", name));
        }
        return true;
    }
}
