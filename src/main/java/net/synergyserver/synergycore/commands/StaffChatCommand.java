package net.synergyserver.synergycore.commands;

import net.synergyserver.synergycore.configs.Message;
import net.synergyserver.synergycore.profiles.MinecraftProfile;
import net.synergyserver.synergycore.settings.CoreToggleSetting;
import net.synergyserver.synergycore.utils.PlayerUtil;
import org.bukkit.Bukkit;
import org.bukkit.Sound;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

@CommandDeclaration(
        commandName = "staffchat",
        aliases =  ".",
        permission = "syn.staffchat",
        usage = "/staffchat <message>",
        description = "Sends a message to staffchat",
        minArgs = 1,
        validSenders = {SenderType.PLAYER, SenderType.CONSOLE}
)

public class StaffChatCommand extends MainCommand{

    @Override
    public boolean execute(CommandSender sender, String[] args, CommandFlags flags) {
        // Get the name for the messages' sender.
        String displayName = (sender instanceof Player) ? sender.getName() : Message.get("server.name");

        // Translate color codes
        String message = Message.translateCodes(String.join(" ", args), Message.get("chat.staff_chat.reset"));

        // Send the message to all staff members
        for (Player p : Bukkit.getOnlinePlayers()) {
            if (!p.hasPermission("syn.staffchat")) {
                continue;
            }

            // If alert_sounds is set to true then make a sound
            MinecraftProfile mcp = PlayerUtil.getProfile(p);

            if (CoreToggleSetting.ALERT_SOUNDS.getValue(mcp)) {
                if (sender instanceof Player) {
                    if (!p.equals((sender))) {
                        p.playSound(p.getLocation(), Sound.ENTITY_ARROW_HIT_PLAYER, 1, 0);
                    }
                } else {
                    p.playSound(p.getLocation(), Sound.ENTITY_ARROW_HIT_PLAYER, 1, 0);
                }
            }

            // Send the message to the player
            p.sendMessage(Message.format("chat.staff_chat.message", displayName, message));
        }

        // Send the message to console
        Bukkit.getConsoleSender().sendMessage(Message.format("chat.staff_chat.message", displayName, message));

        return false;
    }
}
