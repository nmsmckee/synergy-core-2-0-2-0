package net.synergyserver.synergycore.commands;

import net.synergyserver.synergycore.Teleport;
import net.synergyserver.synergycore.configs.Message;
import net.synergyserver.synergycore.events.TeleportRequestCancelEvent;
import net.synergyserver.synergycore.profiles.MinecraftProfile;
import net.synergyserver.synergycore.profiles.ProfileManager;
import net.synergyserver.synergycore.utils.PlayerUtil;
import org.bukkit.Bukkit;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

@CommandDeclaration(
        commandName = "tpcancel",
        aliases = "tprcancel",
        permission = "syn.tpcancel",
        usage = "/tpcancel",
        description = "Cancels all pending teleport requests that you have sent.",
        maxArgs = 0,
        validSenders = SenderType.PLAYER
)
public class TPCancelCommand extends MainCommand {

    @Override
    public boolean execute(CommandSender sender, String[] args, CommandFlags flags) {
        Player player = (Player) sender;

        int cancelled = 0;
        // Cycle through all profiles of online players and check if their pending tp request was sent by the player
        for (MinecraftProfile mcp : ProfileManager.getInstance().getCachedMinecraftProfiles().values()) {
            // Continue the loop if the current player doesn't have a tp request from the sender of this command
            if (!mcp.hasPendingTeleportRequest()) {
                continue;
            }

            Teleport teleport = mcp.getPendingTeleport();
            if (!teleport.getSender().equals(player.getUniqueId())) {
                continue;
            }
            // Ignore this teleport request if the sender can no longer see the receiver
            if (!PlayerUtil.canSee(player, teleport.getReceiver())) {
                continue;
            }

            // Emit a event
            TeleportRequestCancelEvent event = new TeleportRequestCancelEvent(teleport);
            Bukkit.getPluginManager().callEvent(event);

            // Check if the event was cancelled before continuing
            if (event.isCancelled()) {
                continue;
            }

            // Do things now
            mcp.getPlayer().sendMessage(Message.format("commands.teleportation.info.request_cancelled_receiver",
                    player.getName()));
            mcp.resolvePendingTeleportRequest();
            cancelled++;
        }

        if (cancelled == 0) {
            player.sendMessage(Message.format("commands.teleportation.info.request_cancelled_none"));
        } else {
            player.sendMessage(Message.format("commands.teleportation.info.request_cancelled_sender", cancelled));
        }
        return true;
    }
}
