package net.synergyserver.synergycore.commands;

import io.sponges.dubtrack4j.framework.Room;
import io.sponges.dubtrack4j.framework.User;
import net.synergyserver.synergycore.SynergyCore;
import net.synergyserver.synergycore.configs.Message;
import net.synergyserver.synergycore.database.DataManager;
import net.synergyserver.synergycore.exceptions.ServiceOfflineException;
import net.synergyserver.synergycore.profiles.DubtrackProfile;
import net.synergyserver.synergycore.profiles.MinecraftProfile;
import net.synergyserver.synergycore.profiles.SynUser;
import net.synergyserver.synergycore.utils.PlayerUtil;
import org.bukkit.command.CommandSender;

import java.io.IOException;
import java.util.UUID;

@CommandDeclaration(
        commandName = "dubtrack",
        aliases = "dub",
        usage = "/serviceban dubtrack <player>",
        description = "Bans a given user from the Dubtrack server.",
        maxArgs = 1,
        parentCommandName = "serviceban"
)
public class ServiceBanDubtrackCommand extends SubCommand {

    @Override
    public boolean execute(CommandSender sender, String[] args, CommandFlags flags) {
        UUID pID = PlayerUtil.getUUID(args[0], true, sender.hasPermission("vanish.see"));

        if (pID == null) {
            sender.sendMessage(Message.format("commands.error.player_not_found", args[0]));
            return false;
        }

        MinecraftProfile mcp = PlayerUtil.getProfile(pID);
        DataManager dm = DataManager.getInstance();
        SynUser synUser = dm.getDataEntity(SynUser.class, mcp.getSynID());
        DubtrackProfile dubtrackProfile = synUser.getDubtrackProfile();

        if (dubtrackProfile == null) {
            sender.sendMessage(Message.format("commands.service_ban.error.service_not_connected", mcp.getCurrentName(), "Dubtrack"));
            return false;
        }

        try {
            Room room = SynergyCore.getDubtrackRoom();
            User user = room.getUserById(synUser.getDubtrackID());

            room.banUser(user);
            dubtrackProfile.setIsBanned(true);
        } catch (ServiceOfflineException e) {
            sender.sendMessage(Message.format("commands.error.service_offline", "Dubtrack"));
            return false;
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }

        return true;
    }
}
