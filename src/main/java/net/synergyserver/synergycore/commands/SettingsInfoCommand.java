package net.synergyserver.synergycore.commands;

import net.synergyserver.synergycore.configs.Message;
import net.synergyserver.synergycore.settings.FixedOptionsSetting;
import net.synergyserver.synergycore.settings.Setting;
import net.synergyserver.synergycore.settings.SettingManager;
import net.synergyserver.synergycore.settings.SettingOption;
import net.synergyserver.synergycore.settings.SettingPreferences;
import net.synergyserver.synergycore.utils.PlayerUtil;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.List;

@CommandDeclaration(
        commandName = "info",
        permission = "syn.settings.info",
        usage = "/settings info <setting> [value]",
        description = "Displays information about the setting or value specified.",
        minArgs = 1,
        maxArgs = 2,
        validSenders = SenderType.PLAYER,
        parentCommandName = "settings"
)
public class SettingsInfoCommand extends SubCommand {

    @Override
    public boolean execute(CommandSender sender, String[] args, CommandFlags flags) {
        Player player = (Player) sender;
        SettingManager sm = SettingManager.getInstance();

        // If the given argument is not a valid setting then give the player an error
        Setting setting = sm.getSettings().get(args[0].toLowerCase());
        if (setting == null) {
            player.sendMessage(Message.format("commands.settings.error.setting_not_found", args[0]));
            return false;
        }

        // If the player provided a value and the setting has fixed values, check that it's valid
        if (args.length > 1 && setting instanceof FixedOptionsSetting && ((FixedOptionsSetting) setting).getOption(args[1]) == null) {
            player.sendMessage(Message.format("commands.settings.error.value_not_found", args[1]));
            return false;
        }

        // Send the player the request information
        player.sendMessage(Message.format("commands.settings.information.setting_id", setting.getIdentifier()));

        // Give them information about the setting or value, as requested
        // If the setting isn't a fixed option setting, give them info about the setting instead of the value
        if (args.length == 1 || (args.length == 2 && !(setting instanceof FixedOptionsSetting))) {
            // Setting information
            String category = setting.getCategory().getDisplayName().replace(" Category", "");
            player.sendMessage(Message.format("commands.settings.information.category", category.replace(" Settings", "")));
            player.sendMessage(Message.format("commands.settings.information.description", setting.getDescription()));

            // If the setting is a FixedOptionsSetting, send the list of possible values
            if (setting instanceof FixedOptionsSetting) {
                FixedOptionsSetting fixedOptionsSetting = (FixedOptionsSetting) setting;

                List<String> optionValues = new ArrayList<>();
                for (Object value : fixedOptionsSetting.getOptions().keySet()) {
                    optionValues.add(value.toString());
                }

                String valuesList = Message.createList(
                        optionValues,
                        Message.get("info_colored_lists.item_color_1"),
                        Message.get("info_colored_lists.item_color_2"),
                        Message.get("info_colored_lists.grammar_color")
                );
                player.sendMessage(Message.format("commands.settings.information.values", valuesList));
            }

            player.sendMessage(Message.format("commands.settings.information.default_value", setting.getDefaultValue()));

            SettingPreferences sp = PlayerUtil.getProfile(player).getCurrentWorldGroupProfile().getSettingPreferences();
            String currentValue = setting.getValue(sp.getCurrentSettings(), player).toString();
            player.sendMessage(Message.format("commands.settings.information.current_value", currentValue));
        } else if (setting instanceof FixedOptionsSetting) {
            // Value information
            FixedOptionsSetting fixedOptionsSetting = (FixedOptionsSetting) setting;
            SettingOption option = fixedOptionsSetting.getOption(args[1]);

            player.sendMessage(Message.format("commands.settings.information.option_id", option.getIdentifier()));

            String description = option.getDescription();
            if (!description.isEmpty()) {
                player.sendMessage(Message.format("commands.settings.information.description", option.getDescription()));
            }
        }

        return true;
    }
}
