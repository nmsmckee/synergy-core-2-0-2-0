package net.synergyserver.synergycore.commands;

import dev.morphia.query.MorphiaCursor;
import dev.morphia.query.experimental.filters.Filters;
import net.synergyserver.synergycore.SynergyCore;
import net.synergyserver.synergycore.Warp;
import net.synergyserver.synergycore.configs.Message;
import net.synergyserver.synergycore.configs.PluginConfig;
import net.synergyserver.synergycore.database.DataManager;
import net.synergyserver.synergycore.database.MongoDB;
import net.synergyserver.synergycore.utils.StringUtil;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;

import java.util.Map;

@CommandDeclaration(
        commandName = "create",
        aliases = {"set", "add"},
        permission = "syn.warp.create",
        usage = "/warp create <name>",
        description = "Creates a warp at your current location.",
        minArgs = 1,
        maxArgs = 1,
        validSenders = SenderType.PLAYER,
        parentCommandName = "warp"
)
public class WarpCreateCommand extends SubCommand {

    @Override
    public boolean execute(CommandSender sender, String[] args, CommandFlags flags) {
        Player player = (Player) sender;
        YamlConfiguration config = PluginConfig.getConfig(SynergyCore.getPlugin());

        // Get the max number of warps they can set in the current world group
        int warpLimit = config.getInt("warp.limit.default");

        if (player.hasPermission("syn.warp.limit.unlimited")) {
            warpLimit = Integer.MAX_VALUE;
        } else {
            Map<String, Object> limits = config.getConfigurationSection("warp.limit").getValues(false);
            for (String group : limits.keySet()) {
                if (player.hasPermission("syn.warp.limit." + group)) {
                    warpLimit = config.getInt("warp.limit." + group);
                }
            }
        }

        String name = args[0];

        // If the name given isn't alphanumeric then give the player an error
        if (!StringUtil.isWord(name)) {
            player.sendMessage(Message.format("commands.warp.create.error.invalid_name"));
            return false;
        }

        // Find out how many warps the player currently has
        MorphiaCursor<Warp> warpsIt = MongoDB.getInstance().getDatastore().find(Warp.class).filter(Filters.eq("o", player.getUniqueId())).iterator();

        // Count the number of warps they have in their current world group for the permission limit check
        int warpsInWorldGroup = 0;
        String currentWorldGroupName = SynergyCore.getWorldGroupName(player.getWorld().getName());
        while (warpsIt.hasNext()) {
            Warp warp = warpsIt.next();

            if (SynergyCore.getWorldGroupName(warp.getSerializableLocation().getWorldName()).equals(currentWorldGroupName)) {
                warpsInWorldGroup++;
            }

            // If they already have a warp with the given name then give them an error
            if (warp.getName().equalsIgnoreCase(name)) {
                player.sendMessage(Message.format("commands.warp.create.error.name_already_exists", name));
                warpsIt.close();
                return false;
            }
        }
        warpsIt.close();

        // If the next warp will make them go over their limit then give them an error
        if (warpsInWorldGroup + 1 > warpLimit) {
            player.sendMessage(Message.get("commands.warp.create.error.too_many_warps"));
            return false;
        }

        // Create the warp and store it in the database
        Warp warp = new Warp(name, player.getUniqueId(), player.getLocation());
        DataManager.getInstance().saveDataEntity(warp);

        // Give the player feedback and return true
        player.sendMessage(Message.format("commands.warp.create.info.created_warp", name));
        return true;
    }
}
