package net.synergyserver.synergycore.commands;

import net.synergyserver.synergycore.SynergyCore;
import net.synergyserver.synergycore.configs.Message;
import net.synergyserver.synergycore.database.DataManager;
import net.synergyserver.synergycore.profiles.MinecraftProfile;
import net.synergyserver.synergycore.profiles.WorldGroupProfile;
import net.synergyserver.synergycore.utils.PlayerUtil;
import org.bukkit.Location;
import org.bukkit.command.CommandSender;
import org.bukkit.command.ConsoleCommandSender;
import org.bukkit.entity.Player;
import ru.tehkode.permissions.bukkit.PermissionsEx;

import java.util.UUID;

@CommandDeclaration(
        commandName = "find",
        aliases = {"pos", "position", "getpos"},
        permission = "syn.find",
        usage = "/find [player] [-worldgroup <worldgroup>]",
        description = "Finds where you are or where another player is.",
        maxArgs = 1,
        parseCommandFlags = true
)
public class FindCommand extends MainCommand {

    @Override
    public boolean execute(CommandSender sender, String[] args, CommandFlags flags) {
        MinecraftProfile mcp;
        Location playerLocation;

        // Check if a world group was specified
        String correctedWorldGroupName = "";
        if (flags.hasFlag("-w", "-wg", "-worldgroup")) {
            String worldGroupName = flags.getFlagValue("-w", "-wg", "-worldgroup");

            // Correct the capitalization for use with other methods
            correctedWorldGroupName = SynergyCore.getExactWorldGroupName(worldGroupName);;

            // Check to make sure the world group is valid
            if (!SynergyCore.hasWorldGroupName(correctedWorldGroupName)) {
                sender.sendMessage(Message.format("commands.error.invalid_world_group_name", worldGroupName));
                return false;
            }
        }

        if (args.length == 0) {
            // Give an error if the sender is not a player and no player is specified
            if (!SenderType.getSenderType(sender).equals(SenderType.PLAYER)) {
                sender.sendMessage(Message.get("commands.error.wrong_sender_type"));
                return false;
            }

            mcp = PlayerUtil.getProfile((Player) sender);
            playerLocation = ((Player) sender).getLocation();
        } else {
            UUID pID = PlayerUtil.getUUID(args[0], true, sender.hasPermission("vanish.see"));

            // Give an error if no player was found
            if (pID == null) {
                sender.sendMessage(Message.format("commands.error.player_not_found", args[0]));
                return false;
            }

            // Get the most current profile
            if (PlayerUtil.canSee(sender, pID)) {
                mcp = PlayerUtil.getProfile(pID);

                // If a world group wasn't specified then get the current location, otherwise get the location for that world group
                if (correctedWorldGroupName.isEmpty()) {
                    playerLocation = mcp.getPlayer().getLocation();
                } else {
                    WorldGroupProfile wgp = mcp.getPartialWorldGroupProfile(correctedWorldGroupName,"ll");

                    // Tell the player if the target has no data for the world group
                    if (wgp == null) {
                        if (sender instanceof Player && ((Player) sender).getUniqueId().equals(mcp.getID())) {
                            sender.sendMessage(Message.format("commands.info.world_group_no_data_self", correctedWorldGroupName));
                            return true;
                        } else {
                            sender.sendMessage(Message.format("commands.info.world_group_no_data_others", mcp.getCurrentName(), correctedWorldGroupName));
                            return true;
                        }
                    }

                    playerLocation = wgp.getLastLocation().getLocation();
                }
            } else {
                mcp = DataManager.getInstance().getPartialDataEntity(MinecraftProfile.class, pID, "n", "lw", "wp");

                // If a world group wasn't specified then get the current location, otherwise get the location for that world group
                if (correctedWorldGroupName.isEmpty()) {
                    playerLocation = mcp.getPartialWorldGroupProfile(mcp.getLastWorldGroup(), "ll").getLastLocation().getLocation();
                } else {
                    WorldGroupProfile wgp = mcp.getPartialWorldGroupProfile(correctedWorldGroupName,"ll");

                    // Tell the player if the target has no data for the world group
                    if (wgp == null) {
                        if (sender instanceof Player && ((Player) sender).getUniqueId().equals(mcp.getID())) {
                            sender.sendMessage(Message.format("commands.info.world_group_no_data_self", correctedWorldGroupName));
                            return true;
                        } else {
                            sender.sendMessage(Message.format("commands.info.world_group_no_data_others", mcp.getCurrentName(), correctedWorldGroupName));
                            return true;
                        }
                    }

                    playerLocation = wgp.getLastLocation().getLocation();
                }
            }
        }

        // Get the values for the messages
        String worldName = playerLocation.getWorld().getName();
        String worldGroupName = SynergyCore.getWorldGroupName(worldName);

        // Add information about the player's coordinates if the sender has permission
        String locationMessage = "";
        if (sender instanceof ConsoleCommandSender || PermissionsEx.getUser(((Player) sender)).has("syn.find.location", worldName)) {
            int x = playerLocation.getBlockX();
            int y = playerLocation.getBlockY();
            int z = playerLocation.getBlockZ();
            locationMessage = Message.format("commands.find.location", x, y, z);
        }

        String keyOnlineStatus;
        if (PlayerUtil.canSee(sender, mcp.getID()) && (correctedWorldGroupName.isEmpty() || mcp.getCurrentWorldGroupName().equalsIgnoreCase(correctedWorldGroupName))){
            keyOnlineStatus = "online";
        } else {
            keyOnlineStatus = "offline";
        }

        // Create the main info message
        String message;
        if (sender instanceof Player && ((Player) sender).getUniqueId().equals(mcp.getID())) {
            message = Message.format("commands.find.info.self", worldName, worldGroupName, locationMessage);
        } else {
            message = Message.format("commands.find.info.other_" + keyOnlineStatus, mcp.getCurrentName(), worldName, worldGroupName, locationMessage);

            // Append the distance message if the sender is a player in the same world
            if (sender instanceof Player) {
                Location senderLocation = ((Player) sender).getLocation();
                if (senderLocation.getWorld().equals(playerLocation.getWorld())) {
                    message += Message.format("commands.find.distance_" + keyOnlineStatus, senderLocation.distance(playerLocation));
                }
            }
        }

        // Send the sender the message
        sender.sendMessage(message);
        return true;
    }
}
