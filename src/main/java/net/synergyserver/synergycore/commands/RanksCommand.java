package net.synergyserver.synergycore.commands;

import net.synergyserver.synergycore.configs.TextConfig;
import org.bukkit.command.CommandSender;

@CommandDeclaration(
        commandName = "ranks",
        aliases = {"roles", "groups"},
        permission = "syn.ranks",
        usage = "/ranks",
        description = "Lists the ranks of the server and basic information surrounding them.",
        maxArgs = 0
)
public class RanksCommand extends MainCommand {

    @Override
    public boolean execute(CommandSender sender, String[] args, CommandFlags flags) {
        for (String line : TextConfig.getText("ranks.txt")) {
            sender.sendMessage(line);
        }
        return true;
    }
}
