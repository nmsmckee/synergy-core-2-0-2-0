package net.synergyserver.synergycore.commands;

import net.synergyserver.synergycore.Chat;
import net.synergyserver.synergycore.SynergyCore;
import net.synergyserver.synergycore.events.ChatClearEvent;
import org.bukkit.Bukkit;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.UUID;

@CommandDeclaration(
        commandName = "clear",
        aliases = "c",
        permission = "syn.chat.clear",
        usage = "/chat clear",
        description = "Clears chat.",
        maxArgs = 0,
        validSenders = {SenderType.PLAYER, SenderType.CONSOLE},
        parentCommandName = "chat"
)
public class ChatClearCommand extends SubCommand {

    @Override
    public boolean execute(CommandSender sender, String[] args, CommandFlags flags) {
        Chat chat = Chat.getInstance();
        UUID clearer;

        // Get the UUID of the sender
        if (SenderType.getSenderType(sender).equals(SenderType.PLAYER)) {
            clearer = ((Player) sender).getUniqueId();
        } else {
            clearer = SynergyCore.SERVER_ID;
        }

        // Emit an event
        ChatClearEvent event = new ChatClearEvent(clearer);
        Bukkit.getPluginManager().callEvent(event);

        // Check if the event was cancelled before continuing
        if (event.isCancelled()) {
            return true;
        }

        // Clear chat
        chat.clear();
        return false;
    }
}
