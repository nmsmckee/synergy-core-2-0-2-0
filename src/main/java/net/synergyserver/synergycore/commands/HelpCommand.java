package net.synergyserver.synergycore.commands;

import net.synergyserver.synergycore.ChatColor;
import net.synergyserver.synergycore.SynergyCore;
import net.synergyserver.synergycore.configs.Message;
import net.synergyserver.synergycore.profiles.MinecraftProfile;
import net.synergyserver.synergycore.settings.CoreToggleSetting;
import net.synergyserver.synergycore.utils.PlayerUtil;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

@CommandDeclaration(
        commandName = "help",
        permission = "syn.help",
        usage = "/help [command|search|page]",
        description = "Provides help for commands.",
        executeIfInvalidSubCommand = true
)
public class HelpCommand extends MainCommand {

    @Override
    public boolean execute(CommandSender sender, String[] args, CommandFlags flags) {
        HelpSearchCommand helpSearchCommand = (HelpSearchCommand) getSubCommands().get("search");

        switch (SenderType.getSenderType(sender)) {
            case PLAYER:
                MinecraftProfile mcp = PlayerUtil.getProfile((Player) sender);
                // If the player doesn't have the setting enabled or they don't have permission to /help search then break
                if (!sender.hasPermission(helpSearchCommand.getPermission()) || !CoreToggleSetting.HELP_SHORTCUT.getValue(mcp)){
                    break;
                }
            default:
                // Also used from fallthrough with the PLAYER case
                return helpSearchCommand.validate(sender, args);
        }

        // Otherwise give them the correct usage and return false
        sender.sendMessage(Message.format("commands.error.incorrect_syntax", getUsage()));
        return false;
    }

    /**
     * Used to sort help sections.
     */
    public enum HelpOwnerType {
        SYNERGYCORE,
        SYNERGYSUITE,
        OTHER,
        SPIGOT,
        BUKKIT,
        MINECRAFT,
        UNOWNED;

        /**
         * Parses the <code>HelpOwnerType</code> for the owner of the given command.
         *
         * @param command The command.
         * @return The <code>HelpOwnerType</code>.
         */
        public static HelpOwnerType parseHelpOwnerType(Command command) {
            String ownerName = CommandManager.getInstance().getOwnerName(command);
            if (ownerName.equals(SynergyCore.getPlugin().getName())) {
                return SYNERGYCORE;
            } else if (command instanceof SynCommand) {
                return SYNERGYSUITE;
            }

            switch (ownerName) {
                case "Spigot": return SPIGOT;
                case "Bukkit": return BUKKIT;
                case "Minecraft": return MINECRAFT;
                case "": return UNOWNED;
                default: return OTHER;
            }
        }
    }

    /**
     * Formats the usage of a command using <code>formatUsageComponents</code>.
     *
     * @param command The command to format the usage of.
     * @return The formatted usage message.
     */
    public static String formatUsage(Command command) {
        // Create a default usage message if none was provided
        String usage = command.getUsage();
        if (usage == null || usage.isEmpty()) {
            usage = "/" + command.getName();
        }
        // If the usage is multiline for whatever reason (*cough* Multiverse *cough*) then only use the first line
        usage = usage.split("\\n")[0];
        // Support Bukkit's <command> help map substitution
        usage = usage.replace("<command>", command.getName());

        // Format required command arguments
        usage = formatUsageComponent(usage, "<", ">", Message.getColor("c3"));
        // Format optional command arguments
        usage = formatUsageComponent(usage, "[", "]", ChatColor.DARK_GRAY.toString());

        // Strip colors from nested components
        usage = stripNestedComponentColors(usage, "<", ">", "[", "]");
        usage = stripNestedComponentColors(usage, "[", "]", "<", ">");

        return usage;
    }

    /**
     * Formats the usage of a command using <code>formatUsageComponents</code>.
     *
     * @param command The command to format the usage of.
     * @return The formatted usage message.
     */
    public static String formatUsage(SynCommand command) {
        // Create a default usage message if none was provided
        String usage = command.getUsage();
        if (usage == null || usage.isEmpty()) {
            usage = "/" + command.getCommandName();
        }
        // If the usage is multiline for whatever reason (*cough* Multiverse *cough*) then only use the first line
        usage = usage.split("\\n")[0];
        // Support Bukkit's <command> help map substitution
        usage = usage.replace("<command>", command.getCommandName());

        // Format required command arguments
        usage = formatUsageComponent(usage, "<", ">", Message.getColor("c3"));
        // Format optional command arguments
        usage = formatUsageComponent(usage, "[", "]", ChatColor.DARK_GRAY.toString());

        // Strip colors from nested components
        usage = stripNestedComponentColors(usage, "<", ">", "[", "]");
        usage = stripNestedComponentColors(usage, "[", "]", "<", ">");

        return usage;
    }

    /**
     * Attempts to color usage components with the given starting and ending symbols in a given usage message.
     *
     * @param usage The usage message.
     * @param openingSymbol The opening symbol of the component to parse.
     * @param closingSymbol The closing symbol of the component to parse.
     * @param color The color of the component.
     * @return The updated usage message.
     */
    private static String formatUsageComponent(String usage, String openingSymbol, String closingSymbol, String color) {
        StringBuilder builder = new StringBuilder(usage);

        int startingIndex = builder.indexOf(openingSymbol);
        while (startingIndex != -1) {
            String resetColor = ChatColor.getLastColors(builder.substring(0, startingIndex));
            int endingIndex = builder.indexOf(closingSymbol, startingIndex);

            // Count how many nested components there are
            int nestedRequiredArgs = 0;
            int nextStartingIndex = builder.indexOf(openingSymbol, startingIndex + 1);
            while (nextStartingIndex != -1 && nextStartingIndex < endingIndex) {
                nestedRequiredArgs++;
                nextStartingIndex = builder.indexOf(openingSymbol, nextStartingIndex + 1);
            }

            // Skip that number of closing symbols to find the matching ending symbol
            for (int i = 0; i < nestedRequiredArgs; i++) {
                endingIndex = builder.indexOf(closingSymbol, endingIndex + 1);
            }

            // If no matching ending symbol could be found, break out to avoid an error
            if (endingIndex <= startingIndex) {
                break;
            }

            // Format the component with the given color
            builder.insert(startingIndex, color);
            // Reset the color
            builder.insert(endingIndex + color.length() + 1, resetColor);

            // Find the next starting index
            startingIndex = builder.indexOf(openingSymbol, endingIndex + color.length() + resetColor.length());
        }

        return builder.toString();
    }

    /**
     * Strips the color of all nested components so that nested components are the same color as their parent component.
     * This is a super ghetto technique that will be replaced once commands are overhauled with an OOP usage system.
     *
     * @param usage The usage message.
     * @param openingSymbol The opening symbol of the parent component.
     * @param closingSymbol The closing symbol of the parent component.
     * @param nestedOpeningSymbol The opening symbol of the nested component to strip the color of.
     * @param nestedClosingSymbol The closing symbol of the nested component to strip the color of.
     * @return The updated usage message.
     */
    private static String stripNestedComponentColors(String usage, String openingSymbol, String closingSymbol, String nestedOpeningSymbol, String nestedClosingSymbol) {
        StringBuilder builder = new StringBuilder(usage);

        int startingIndex = builder.indexOf(openingSymbol);
        while (startingIndex != -1) {
            int endingIndex = builder.indexOf(closingSymbol, startingIndex);

            // Count how many nested components there are
            int nestedRequiredArgs = 0;
            int nextStartingIndex = builder.indexOf(openingSymbol, startingIndex + 1);
            while (nextStartingIndex != -1 && nextStartingIndex < endingIndex) {
                nestedRequiredArgs++;
                nextStartingIndex = builder.indexOf(openingSymbol, nextStartingIndex + 1);
            }

            // Skip that number of closing symbols to find the matching ending symbol
            for (int i = 0; i < nestedRequiredArgs; i++) {
                endingIndex = builder.indexOf(closingSymbol, endingIndex + 1);
            }

            // If no matching ending symbol could be found, break out to avoid an error
            if (endingIndex <= startingIndex) {
                break;
            }

            int originalSize = builder.length();

            // Strip all colors within the opening and closing symbol
            String strippedSubstring = ChatColor.stripColor(builder.substring(startingIndex, endingIndex + 1));
            builder.replace(startingIndex, endingIndex + 1, strippedSubstring);

            int newSize = builder.length();

            // Find the next starting index and offset the ending index, taking into account the changed size
            startingIndex = builder.indexOf(openingSymbol, endingIndex + (newSize - originalSize));
        }

        return builder.toString();
    }
}
