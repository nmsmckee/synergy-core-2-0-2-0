package net.synergyserver.synergycore.commands;

import net.synergyserver.synergycore.Chat;
import net.synergyserver.synergycore.configs.Message;
import net.synergyserver.synergycore.profiles.MinecraftProfile;
import net.synergyserver.synergycore.settings.CoreToggleSetting;
import net.synergyserver.synergycore.utils.PlayerUtil;
import org.bukkit.Bukkit;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

@CommandDeclaration(
        commandName = "me",
        permission = "syn.me",
        usage = "/me <message>",
        description = "Sends a self message to the server",
        minArgs = 1,
        validSenders = SenderType.PLAYER
)
public class MeCommand extends MainCommand {

    @Override
    public boolean execute(CommandSender sender, String[] args, CommandFlags flags) {
        Player player = (Player) sender;
        String message = String.join(" ", args);

        // If chat is locked, give the player an error
        if (Chat.getInstance().isLocked() && !player.hasPermission("syn.chat.lock.bypass")) {
            player.sendMessage(Message.get("chat.error.locked"));
            return false;
        }

        // Broadcast the message
        for (Player p : Bukkit.getOnlinePlayers()) {
            MinecraftProfile mcp = PlayerUtil.getProfile(p);

            // Don't send to players with minimal_chat enabled
            if (CoreToggleSetting.MINIMAL_CHAT.getValue(mcp) && !p.equals(player)) {
                continue;
            }

            // Don't send to players that are ignoring the command performer
            if (mcp.hasIgnored(player.getUniqueId()) && !player.hasPermission("syn.ignore.exempt")) {
                continue;
            }

            p.sendMessage(Message.format("commands.me.message", player.getName(), message));
        }

        return true;
    }
}
