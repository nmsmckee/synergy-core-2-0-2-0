package net.synergyserver.synergycore.commands;

import net.synergyserver.synergycore.ChatColor;
import net.synergyserver.synergycore.Rank;
import net.synergyserver.synergycore.SynergyCore;
import net.synergyserver.synergycore.configs.Message;
import net.synergyserver.synergycore.profiles.MinecraftProfile;
import net.synergyserver.synergycore.utils.PlayerUtil;
import org.bukkit.Bukkit;
import org.bukkit.command.CommandSender;
import ru.tehkode.permissions.PermissionUser;
import ru.tehkode.permissions.bukkit.PermissionsEx;

import java.util.Comparator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@CommandDeclaration(
        commandName = "list",
        permission = "syn.list",
        usage = "/list [-wg [worldgroup]] [-rank <rank>] [-afk|-active]",
        description = "Displays a list of online users.",
        maxArgs = 0,
        parseCommandFlags = true
)
public class ListCommand extends MainCommand {

    @Override
    public boolean execute(CommandSender sender, String[] args, CommandFlags flags) {
        boolean categorizeByWorldGroup = false;
        String worldGroupFlag = "";
        Rank rankFlag = null;
        boolean afkFlag;
        boolean activeFlag;

        // Check if the world group flag is provided
        if (flags.hasFlag("-w", "-world", "-wg", "-worldgroup")) {
            String worldGroupName = flags.getFlagValue("-w", "-world", "-wg", "-worldgroup");

            if (worldGroupName.isEmpty()) {
                // If no world group was provided then set the flag that changes the category to world groups
                categorizeByWorldGroup = true;
            } else {
                // Correct the capitalization for use with other methods
                worldGroupFlag = SynergyCore.getExactWorldGroupName(worldGroupName);

                // Check to make sure the world group is valid
                if (!SynergyCore.hasWorldGroupName(worldGroupFlag)) {
                    sender.sendMessage(Message.format("commands.error.invalid_world_group_name", worldGroupName));
                    return false;
                }
            }
        }

        // Check if the rank flag is provided
        if (flags.hasFlag("-rank", "-role", "-group", "-r")) {
            String rank = flags.getFlagValue("-rank", "-role", "-group", "-r");

            // If no rank was provided give the sender an error
            if (rank.isEmpty()) {
                sender.sendMessage(Message.format("commands.list.error.no_rank_found", ""));
                return false;
            }

            rankFlag = Rank.parseRank(rank);

            // Check to make sure the rank is valid
            if (rankFlag == null) {
                sender.sendMessage(Message.format("commands.list.error.no_rank_found", rank));
                return false;
            }
        }

        // Check if either afk flag is provided
        afkFlag = flags.hasFlag("-afk");
        activeFlag = flags.hasFlag("-active", "-notafk");
        // Give an error if both are provided
        if (afkFlag && activeFlag) {
            sender.sendMessage(Message.format("commands.list.error.conflicting_afk_flags"));
            return false;
        }

        // Filter the list of online players according to the specified flags
        String finalWorldGroupFlag = worldGroupFlag;
        Rank finalRankFlag = rankFlag;
        boolean finalAFKFlag = afkFlag;
        boolean finalActiveFlag = activeFlag;
        List<MinecraftProfile> filteredPlayers = Bukkit.getOnlinePlayers().stream().filter(player -> {
            // Skip over the current player if the sender can't see them
            if (!PlayerUtil.canSee(sender, player.getUniqueId())) {
                return false;
            }

            MinecraftProfile mcp = PlayerUtil.getProfile(player);
            String currentWorldGroup = mcp.getCurrentWorldGroupName();
            // Skip over the current player if a world group was specified and they're not in it
            if (!finalWorldGroupFlag.isEmpty() && !finalWorldGroupFlag.equals(currentWorldGroup)) {
                return false;
            }

            // Skip over the current player if a rank was specified and they don't have it
            if (finalRankFlag != null) {
                PermissionUser pexUser = PermissionsEx.getUser(player);
                if (!pexUser.inGroup(finalRankFlag.toPermissionGroup(), false)) {
                    return false;
                }
            }

            // Skip over the current player if the afk flag was specified and they aren't afk
            if (finalAFKFlag && !mcp.isAFK()) {
                return false;
            }
            // Skip over the current player if the active flag was specified and they are afk
            if (finalActiveFlag && mcp.isAFK()) {
                return false;
            }
            return true;
        }).map(PlayerUtil::getProfile).collect(Collectors.toList());

        // Build the modifiers
        String locationModifier = Message.format("commands.list.default_modifier");
        if (!worldGroupFlag.isEmpty()) {
            locationModifier = Message.format("commands.list.world_group_modifier", worldGroupFlag);
        }
        String rankModifier = "";
        if (rankFlag != null) {
            rankModifier = Message.format("commands.list.rank_modifier", rankFlag.getDisplay());
        }
        String afkModifier = "";
        if (afkFlag) {
            afkModifier = Message.format("commands.list.afk_modifier", "AFK");
        } else if (activeFlag) {
            afkModifier = Message.format("commands.list.afk_modifier", "active");
        }

        int count = filteredPlayers.size();

        // If there are no matched players then give the sender a special message
        if (count == 0) {
            sender.sendMessage(Message.format("commands.list.info.none_found", locationModifier, rankModifier, afkModifier));
            return true;
        }

        Map<String, List<String>> categories;

        // If the categorizeByWorldGroup flag was specified then categorize by world group, otherwise categorize by rank
        if (categorizeByWorldGroup) {
            // Sort the list of players alphabetically by world group, then by rank, then by name, then group them by world group
            categories = filteredPlayers.stream()
                    .sorted(Comparator
                            .comparing(MinecraftProfile::getCurrentWorldGroupName, String.CASE_INSENSITIVE_ORDER)
                            .thenComparing((MinecraftProfile mcp) -> PlayerUtil.getPrimaryRank(mcp.getID()))
                            .thenComparing(MinecraftProfile::getCurrentName, String.CASE_INSENSITIVE_ORDER))
                    .collect(Collectors.groupingBy(
                        MinecraftProfile::getCurrentWorldGroupName,
                        LinkedHashMap::new,
                        Collectors.mapping(mcp -> formatListEntry(sender, mcp), Collectors.toList())
                    ));
        } else {
            // Sort the list of players by rank, then by name, then group them by rank
            categories = filteredPlayers.stream()
                    .sorted(Comparator
                            .comparing((MinecraftProfile mcp) -> PlayerUtil.getRankListRank(mcp.getID()))
                            .thenComparing(MinecraftProfile::getCurrentName, String.CASE_INSENSITIVE_ORDER))
                    .collect(Collectors.groupingBy(
                            (MinecraftProfile mcp) -> PlayerUtil.getRankListRank(mcp.getID()).getDisplayName(),
                            LinkedHashMap::new,
                            Collectors.mapping(mcp -> formatListEntry(sender, mcp), Collectors.toList())
                    ));
        }



        // Send the list header
        sender.sendMessage(Message.format("commands.list.info.header", count, locationModifier, rankModifier, afkModifier));

        // Send the categories
        String messageKey = categorizeByWorldGroup ? "commands.list.world_group_category" : "commands.list.rank_category";
        categories.forEach((category, names) -> {
            String nameMessage = Message.createFormattedList(names, "", Message.get("info_colored_lists.grammar_color"));
            sender.sendMessage(Message.format(messageKey, category, nameMessage));
        });

        return true;
    }

    /**
     * Formats a given list entry given the player's and the sender's
     * credentials. The player's name is colored the same as their rank
     * and is formatted in italics if they're AFK. IF the player is
     * vanished then a " - Vanished" tag is suffixed.
     *
     * @param sender The command sender accessing the list of players.
     * @param mcp The <code>MinecraftProfile</code> of the player being listed.
     * @return The formatted list entry.
     */
    private String formatListEntry(CommandSender sender, MinecraftProfile mcp) {
        String nameColor = PlayerUtil.getNameColor(mcp.getID());
        String afkFormatting = mcp.isAFK() ? ChatColor.ITALIC.toString() : "";

        String vanishStatus = "";
        if (sender.hasPermission("vanish.see") && PlayerUtil.isVanished(mcp.getPlayer())) {
            vanishStatus = ChatColor.GRAY + " - Vanished";
        }

        return nameColor + afkFormatting + mcp.getCurrentName() + vanishStatus;
    }
}
