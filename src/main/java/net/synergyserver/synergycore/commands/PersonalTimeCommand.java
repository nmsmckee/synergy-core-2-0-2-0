package net.synergyserver.synergycore.commands;

import net.synergyserver.synergycore.configs.Message;
import net.synergyserver.synergycore.profiles.WorldGroupProfile;
import net.synergyserver.synergycore.utils.PlayerUtil;
import net.synergyserver.synergycore.utils.TimeUtil;
import org.bukkit.Bukkit;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.UUID;

@CommandDeclaration(
        commandName = "personaltime",
        aliases = "ptime",
        permission = "syn.personaltime",
        usage = "/personaltime [time|reset] [player]",
        description = "Checks or sets a player's personal time.",
        maxArgs = 2,
        validSenders = SenderType.PLAYER
)
public class PersonalTimeCommand extends MainCommand {

    @Override
    public boolean execute(CommandSender sender, String[] args, CommandFlags flags) {
        Player player = (Player) sender;

        // If the player didn't specify a time to set or another player then send the player their current world time
        if (args.length == 0) {
            String time = TimeUtil.formatGameTime(player.getPlayerTime());
            long timeTicks = TimeUtil.toGameTimeTicks(player.getPlayerTime());

            // If their offset is 0 then their time matches the server
            if (player.isPlayerTimeRelative()) {
                if (player.getPlayerTimeOffset() == 0) {
                    player.sendMessage(Message.format("commands.personaltime.see.info.self_matches",
                            time,
                            timeTicks,
                            PlayerUtil.getProfile(player).getCurrentWorldGroupName()
                    ));
                } else {
                    long offsetTimeTicks = TimeUtil.toGameTimeTicks(player.getPlayerTimeOffset());
                    player.sendMessage(Message.format("commands.personaltime.see.info.self_relative",
                            time,
                            timeTicks,
                            PlayerUtil.getProfile(player).getCurrentWorldGroupName(),
                            offsetTimeTicks
                    ));
                }
            } else {
                player.sendMessage(Message.format("commands.personaltime.see.info.self_fixed",
                        time,
                        timeTicks,
                        PlayerUtil.getProfile(player).getCurrentWorldGroupName()
                ));
            }
            return true;
        }

        // Determine who the target player is
        Player target = player;
        if (args.length == 2) {
            // If the player doesn't have permission to change another player's personal time then give an error
            if (!player.hasPermission("syn.personaltime.others.set")) {
                player.sendMessage("commands.personaltime.set.error.no_permission_others");
                return false;
            }

            UUID targetID = PlayerUtil.getUUID(args[1], false, sender.hasPermission("vanish.see"));
            // Make sure the target player exists
            if (targetID == null) {
                player.sendMessage(Message.format("commands.error.player_not_found", args[1]));
                return false;
            }
            target = Bukkit.getPlayer(targetID);
        }

        WorldGroupProfile wgp = PlayerUtil.getProfile(target).getCurrentWorldGroupProfile();

        // If the target's time is being reset
        if (args[0].equalsIgnoreCase("reset")) {
            target.resetPlayerTime();
            wgp.setPersonalTime(null);

            // Give different feedback if the player is also the target
            if (player == target) {
                player.sendMessage(Message.format("commands.personaltime.set.info.reset_self", wgp.getWorldGroupName()));
            } else {
                player.sendMessage(Message.format("commands.personaltime.set.info.reset_other", target.getName(), wgp.getWorldGroupName()));
            }
            return true;
        }

        // Parse the time
        long time = TimeUtil.parseGameTime(args[0]);

        // If the time was unable to be parsed then give an error
        if (time == -1) {
            player.sendMessage(Message.format("commands.personaltime.error.invalid_time_format", args[0]));
            return false;
        }

        // Set the target's time
        target.setPlayerTime(time, false);
        wgp.setPersonalTime(time);

        // Give different feedback if the player is also the target
        if (player == target) {
            player.sendMessage(Message.format("commands.personaltime.set.info.set_self", TimeUtil.formatGameTime(time), time, wgp.getWorldGroupName()));
        } else {
            player.sendMessage(Message.format("commands.personaltime.set.info.set_other", target.getName(), TimeUtil.formatGameTime(time), time, wgp.getWorldGroupName()));
        }
        return true;
    }
}
