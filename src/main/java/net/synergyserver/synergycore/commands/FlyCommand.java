package net.synergyserver.synergycore.commands;

import net.synergyserver.synergycore.configs.Message;
import net.synergyserver.synergycore.utils.PlayerUtil;
import org.bukkit.Bukkit;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.UUID;

@CommandDeclaration(
        commandName = "fly",
        permission = "syn.fly",
        usage = "/fly [player]",
        description = "Toggles the flight status of yourself or another player.",
        maxArgs = 1,
        validSenders = SenderType.PLAYER
)
public class FlyCommand extends MainCommand {

    @Override
    public boolean execute(CommandSender sender, String[] args, CommandFlags flags) {
        Player player;

        // If the player is toggling their own flight status
        if (args.length == 0) {
            // If the sender is not a player, then there needs to be a player specified
            if (!SenderType.getSenderType(sender).equals(SenderType.PLAYER)) {
                sender.sendMessage(Message.get("commands.error.wrong_sender_type"));
                return false;
            }

            player = (Player) sender;
        } else {
            // Give an error if the sender doesn't have permission to change another player's flight status
            if (!sender.hasPermission("syn.fly.others")) {
                sender.sendMessage(Message.get("commands.find.error.no_permission_others"));
                return false;
            }

            UUID targetID = PlayerUtil.getUUID(args[0], false, sender.hasPermission("vanish.see"));

            // Give an error if no player was found
            if (targetID == null) {
                sender.sendMessage(Message.format("commands.error.player_not_found", args[0]));
                return false;
            }

            player = Bukkit.getPlayer(targetID);
        }

        // Toggle the player's flight status
        player.setAllowFlight(!player.getAllowFlight());

        // Give the sender feedback
        String flightStatus;
        if (player.getAllowFlight()) {
            flightStatus = "enabled";
        } else {
            flightStatus = "disabled";
        }

        if (sender.equals(player)) {
            player.sendMessage(Message.format("commands.fly.info.self", flightStatus));
        } else {
            player.sendMessage(Message.format("commands.fly.info.others", flightStatus, player.getName()));
        }
        return true;
    }
}