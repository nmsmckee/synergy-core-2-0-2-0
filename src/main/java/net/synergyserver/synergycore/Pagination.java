package net.synergyserver.synergycore;

import net.synergyserver.synergycore.configs.Message;
import net.synergyserver.synergycore.configs.PluginConfig;
import org.bukkit.command.CommandSender;

import java.util.ArrayList;
import java.util.List;

/**
 * Represents a group of pages that can be individually read.
 */
public class Pagination {

    private List<Page> pages = new ArrayList<>();
    private int currentIndex = 0;
    private String title;

    public Pagination(List<String> text) {
        this(text, null);
    }

    public Pagination(List<String> text, String title) {
        this(text, title, PluginConfig.getConfig(SynergyCore.getPlugin()).getInt("default_page_size"), false);
    }

    /**
     * Constructs a new pagination object.
     *
     * @param text The text to paginate.
     * @param pageSize The size of each page.
     */
    public Pagination(List<String> text, String title, int pageSize) {
        this(text, title, pageSize, false);
    }

    public Pagination(List<String> text, String title, int pageSize, boolean inverted) {
        this.title = title;
        int pageIndex = 0;

        // Invert the pages, if necessary
        if (inverted) {
            for (int i = text.size(); i > 0; i -= pageSize) {
                // Set the start index
                int startIndex = i - pageSize;
                // Make sure the start index is still within bounds
                startIndex = Math.max(startIndex, 0);

                // Create a new page and add it
                Page page = new Page(text.subList(startIndex, i), pageIndex++);
                pages.add(page);

                // Set the footer but not on the last page
                if (i - pageSize > 0) {
                    page.setFooter(Message.get("pagination.default_footer_text"));
                }
            }
        } else {
            for (int i = 0; i < text.size(); i += pageSize) {
                // Set the end index
                int endIndex = i + pageSize;
                // Make sure the end index is still within bounds
                endIndex = Math.min(endIndex, text.size());

                // Create a new page and add it
                Page page = new Page(text.subList(i, endIndex), pageIndex++);
                pages.add(page);

                // Set the footer but not on the last page
                if (i + pageSize < text.size()) {
                    page.setFooter(Message.get("pagination.default_footer_text"));
                }
            }
        }

        // Set the page headers since we now know the total number of pages
        for (Page page : pages) {
            if (title == null) {
                page.setHeader(Message.format("pagination.default_header_text_no_title", page.getPageIndex() + 1, pages.size()));
            } else {
                page.setHeader(Message.format("pagination.default_header_text", title, page.getPageIndex() + 1, pages.size()));
            }
        }
    }

    /**
     * Gets the pages of this pagination.
     *
     * @return The pages of this pagination.
     */
    public List<Page> getPages() {
        return pages;
    }

    /**
     * Get the current index of the pagination.
     *
     * @return The pagination's current index.
     */
    public int getCurrentIndex() {
        return currentIndex;
    }

    /**
     * Sets the current index of the pagination.
     *
     * @param currentIndex The pagination's new current index.
     */
    public void setCurrentIndex(int currentIndex) {
        this.currentIndex = currentIndex;
    }

    /**
     * Gets the title of this pagination.
     *
     * @return The title of this pagination or null if none exists.
     */
    public String getTitle() {
        return title;
    }

    /**
     * Get the current page.
     *
     * @return The current page.
     */
    public Page getCurrentPage() {
        return pages.get(currentIndex);
    }

    /**
     * Increment the pagination to the next page.
     *
     * @return The next page in the pagination, or the last page if there are no more pages.
     */
    public Page nextPage() {
        currentIndex++;

        // If the current page is outside the range then reset it to the last page
        if (currentIndex >= pages.size()) {
            currentIndex = pages.size() - 1;
        }

        // Return the next page
        return pages.get(currentIndex);
    }

    /**
     * Decrement the pagination to the previous page.
     *
     * @return The previous page in the pagination, or the first page if there are no previous pages.
     */
    public Page previousPage() {
        currentIndex--;

        // If the current page is outside the range then reset it to the first page
        if (currentIndex < 0) {
            currentIndex = 0;
        }
        // Return the previous page
        return pages.get(currentIndex);
    }

    /**
     * Set the header for every page in the pagination.
     *
     * @param header The new header.
     */
    public void setHeaders(String header) {
        pages.forEach(page -> page.setHeader(header));
    }

    /**
     * Set the footer for every page in the pagination.
     *
     * @param footer The new footer.
     */
    public void setFooters(String footer) {
        pages.forEach(page -> page.setFooter(footer));
    }

    /**
     * Prints the current page to a given player.
     *
     * @param receiver the player to receive the list
     * @param formatKey the key used to format the list in Messages#format
     */
    public void printCurrentPage(CommandSender receiver, String formatKey) {
        receiver.sendMessage(this.getCurrentPage().getHeader());

        //print all of the lines of this page, ignoring any that are empty
        for (String line : this.getCurrentPage().getText()) {
            if (!line.equals("")) {
                receiver.sendMessage(Message.format(formatKey, line));
            }
        }

        //print the footer if it is not null.
        if (this.getCurrentPage().getFooter() != null) {
            receiver.sendMessage(this.getCurrentPage().getFooter());
        }
    }
}
