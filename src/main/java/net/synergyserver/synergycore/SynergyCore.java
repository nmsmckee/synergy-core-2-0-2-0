package net.synergyserver.synergycore;

import com.comphenix.protocol.ProtocolLibrary;
import com.comphenix.protocol.ProtocolManager;
import dev.morphia.Datastore;
import dev.morphia.query.FindOptions;
import dev.morphia.query.MorphiaCursor;
import io.sponges.dubtrack4j.DubtrackAPI;
import io.sponges.dubtrack4j.DubtrackBuilder;
import io.sponges.dubtrack4j.framework.Room;
import io.sponges.dubtrack4j.internal.DubtrackAPIImpl;
import net.dv8tion.jda.api.JDA;
import net.dv8tion.jda.api.JDABuilder;
import net.dv8tion.jda.api.entities.Guild;
import net.dv8tion.jda.api.requests.GatewayIntent;
import net.dv8tion.jda.api.utils.cache.CacheFlag;
import net.synergyserver.synergycore.commands.*;
import net.synergyserver.synergycore.configs.Message;
import net.synergyserver.synergycore.configs.PluginConfig;
import net.synergyserver.synergycore.configs.TextConfig;
import net.synergyserver.synergycore.database.DataManager;
import net.synergyserver.synergycore.database.MongoDB;
import net.synergyserver.synergycore.exceptions.ServiceOfflineException;
import net.synergyserver.synergycore.listeners.DiscordListener;
import net.synergyserver.synergycore.listeners.DubtrackListener;
import net.synergyserver.synergycore.listeners.GUIListener;
import net.synergyserver.synergycore.listeners.PacketListeners;
import net.synergyserver.synergycore.listeners.PlayerConnectionListener;
import net.synergyserver.synergycore.listeners.PlayerListener;
import net.synergyserver.synergycore.listeners.PlayerMoveListener;
import net.synergyserver.synergycore.listeners.WorldListener;
import net.synergyserver.synergycore.profiles.DiscordProfile;
import net.synergyserver.synergycore.profiles.DubtrackProfile;
import net.synergyserver.synergycore.profiles.MinecraftProfile;
import net.synergyserver.synergycore.profiles.ProfileManager;
import net.synergyserver.synergycore.profiles.SynUser;
import net.synergyserver.synergycore.profiles.WebsiteProfile;
import net.synergyserver.synergycore.profiles.WorldGroupProfile;
import net.synergyserver.synergycore.settings.CoreMultiOptionSetting;
import net.synergyserver.synergycore.settings.CoreSetSetting;
import net.synergyserver.synergycore.settings.CoreToggleSetting;
import net.synergyserver.synergycore.settings.SettingDiffs;
import net.synergyserver.synergycore.settings.SettingManager;
import net.synergyserver.synergycore.settings.SettingPreferences;
import net.synergyserver.synergycore.settings.SettingPreset;
import net.synergyserver.synergycore.utils.*;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.scheduler.BukkitTask;
import org.bukkit.util.CachedServerIcon;
import org.bukkit.util.Consumer;
import org.spigotmc.SpigotConfig;

import javax.security.auth.login.LoginException;
import java.io.File;
import java.util.*;

/**
 * The main class for SynergyCore.
 */
public class SynergyCore extends JavaPlugin implements SynergyPlugin {

    private CachedServerIcon icon;

    private static Set<SynergyPlugin> synergyPlugins = new HashSet<>();
    private static Map<String, String> worldGroups = new HashMap<>();
    private static JDA discordClient;
    private static long lastDiscordLoginAttempt;
    private static DubtrackAPIImpl dubtrackClient;
    private static long lastDubtrackLoginAttempt;

    public static final UUID SERVER_ID = new UUID(0, 0);

    @Override
    public void onEnable() {
        getLogger().info("Starting up SynergyCore...");
        registerSynergyPlugin(this);

        getLogger().info("Loading text files...");
        TextConfig tc = TextConfig.getInstance();
        for (String textName : getTextNames()) {
            tc.load(this, textName);
        }

        getLogger().info("Starting the playtime updater...");
        long playtimeTimerDelay = TimeUtil.toFlooredUnit(TimeUtil.TimeUnit.MINUTE,
                TimeUtil.TimeUnit.GAME_TICK, getPluginConfig().getLong("playtime.timer_minutes"));
        Bukkit.getScheduler().runTaskTimer(this, PlaytimeTimer.playtimeUpdater, 0L, playtimeTimerDelay);

        getLogger().info("Starting the afktime updater...");
        long afktimeTimerDelay = TimeUtil.toFlooredUnit(TimeUtil.TimeUnit.MINUTE,
                TimeUtil.TimeUnit.GAME_TICK, getPluginConfig().getLong("afk.timer_minutes"));
        Bukkit.getScheduler().runTaskTimer(this, PlaytimeTimer.afkTimer, 0L, afktimeTimerDelay);

        getLogger().info("Starting the TPS alert timer...");
        long tpsAlertTimerDelay = TimeUtil.toFlooredUnit(TimeUtil.TimeUnit.MINUTE,
                TimeUtil.TimeUnit.GAME_TICK, getPluginConfig().getLong("low_tps_alert.timer_minutes"));
        Bukkit.getScheduler().runTaskTimer(this, ServerStatusTimer.tpsAlertTimer, 0L, tpsAlertTimerDelay);

        getLogger().info("Starting the storage monitor...");
        long storageMonitorTimerDelay = TimeUtil.toFlooredUnit(TimeUtil.TimeUnit.MINUTE,
                TimeUtil.TimeUnit.GAME_TICK, getPluginConfig().getLong("storage_monitor.timer_minutes"));
        Bukkit.getScheduler().runTaskTimer(this, ServerStatusTimer.storageMonitorTimer, 0L, storageMonitorTimerDelay);

        // Log into the Discord bot
        getLogger().info("Logging into Discord...");
        loginDiscordBot(success -> {
            if (success) {
                getLogger().info("Successfully logged into Discord.");
            } else {
                getLogger().info("Error logging into Discord.");
            }
        });

//        // Log into the Dubtrack bot
//        getLogger().info("Logging into Dubtrack...");
//        loginDubtrackBot(success -> {
//            if (success) {
//                getLogger().info("Successfully logged into Dubtrack.");
//            } else {
//                getLogger().info("Error logging into Dubtrack.");
//            }
//        });

        getLogger().info("Cleaning up the database...");
        cleanDatabase();

        getLogger().info("Loading the server icon...");
        loadServerIcon();

        getLogger().info("Injecting the custom unknown command message...");
        SpigotConfig.unknownCommandMessage = Message.format("commands.unknown_command");

        // Call postLoad as soon as all plugins have been enabled
        Bukkit.getScheduler().runTaskLater(this, this::postLoad, 0L);
    }

    public void postLoad() {
        // Call postLoad() on all SynergyPlugins
        for (SynergyPlugin plugin : synergyPlugins) {
            // Avoid a StackOverFlow
            if (!(plugin instanceof SynergyCore)) {
                // Wrap in a try/catch block just in case
                try {
                    plugin.postLoad();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }

        for (String world : PluginConfig.getConfig(this).getStringList("enabled_spawns")) {
            getLogger().info("Doing Multiverse's job for "  + world + "...");
            Bukkit.dispatchCommand(Bukkit.getConsoleSender(), "mvm set monsters true " + world);
            Bukkit.dispatchCommand(Bukkit.getConsoleSender(), "mvm set animals true " + world);
        }
    }

    public void onReload() {
        // Update the server icon and "unknown command" message
        loadServerIcon();
        SpigotConfig.unknownCommandMessage = Message.format("commands.unknown_command");

        // Also reload the text files
        TextConfig tc = TextConfig.getInstance();
        for (String textName : getTextNames()) {
            tc.unload(textName);
            tc.load(this, textName);
        }
    }

    @Override
    public void onDisable() {
        getLogger().info("Logging out of Discord...");
        logoutDiscordBotSync(success -> {
            if (success) {
                getLogger().info("Successfully logged out of Discord.");
            } else {
                getLogger().info("Error logging out of Discord.");
            }
        });

//        // Log into the Dubtrack bot with the specified token.
//        getLogger().info("Logging out of Dubtrack...");
//        logoutDubtrackBotSync(success -> {
//            if (success) {
//                getLogger().info("Successfully logged out of Dubtrack.");
//            } else {
//                getLogger().info("Error logging out of Dubtrack.");
//            }
//        });

        getLogger().info("Saving MinecraftProfiles of online players...");
        for (Player p : Bukkit.getOnlinePlayers()) {
            MinecraftProfile mcp = PlayerUtil.getProfile(p);

            String worldGroupName = mcp.getCurrentWorldGroupName();
            WorldGroupProfile wgp = mcp.getCurrentWorldGroupProfile();
            long currentTime = System.currentTimeMillis();

            // Update information for their current world group
            wgp.setLastLogOut(currentTime);
            wgp.setLastLocation(new SerializableLocation(p.getLocation()));

            // End the player being afk. if they are
            if (mcp.isAFK()) {
                wgp.setAfktimeEnd(currentTime);
            }

            // Update information for their MinecraftProfile
            mcp.setLastWorldGroup(worldGroupName);
            mcp.setLastLogOut(currentTime);

            // Uncache the player's profile
            ProfileManager.getInstance().uncacheMinecraftProfile(p.getUniqueId());
        }
    }

    public void registerCommands() {
        CommandManager cm = CommandManager.getInstance();
        // Debug
        cm.registerMainCommand(this, TestCommand.class);
        cm.registerSubCommand(this, FooCommand.class);
        cm.registerSubCommand(this, BazCommand.class);

        cm.registerMainCommand(this, TPCommand.class);
        cm.registerMainCommand(this, TPHereCommand.class);
        cm.registerMainCommand(this, TPAllCommand.class);
        cm.registerMainCommand(this, TPRCommand.class);
        cm.registerMainCommand(this, TPRHereCommand.class);
        cm.registerMainCommand(this, TPAcceptCommand.class);
        cm.registerMainCommand(this, TPDenyCommand.class);
        cm.registerMainCommand(this, TPCancelCommand.class);
        cm.registerMainCommand(this, TPRAllCommand.class);
        cm.registerMainCommand(this, TPPosCommand.class);
        cm.registerMainCommand(this, BackCommand.class);

        cm.registerMainCommand(this, MessageCommand.class);
        cm.registerMainCommand(this, ReplyCommand.class);

        cm.registerMainCommand(this, ChatCommand.class);
        cm.registerSubCommand(this, ChatLockCommand.class);
        cm.registerSubCommand(this, ChatClearCommand.class);

        cm.registerMainCommand(this, ApplyCommand.class);
        cm.registerMainCommand(this, AppCommand.class);
        cm.registerSubCommand(this, AppListCommand.class);
        cm.registerSubCommand(this, AppAcceptCommand.class);
        cm.registerSubCommand(this, AppDenyCommand.class);

        cm.registerMainCommand(this, SettingsCommand.class);
        cm.registerSubCommand(this, SettingsListCommand.class);
        cm.registerSubCommand(this, SettingsInfoCommand.class);
        cm.registerSubCommand(this, SettingsSetCommand.class);
        cm.registerSubCommand(this, SettingsGUICommand.class);
        cm.registerSubCommand(this, SettingsPresetCommand.class);
        cm.registerSubCommand(this, SettingsPresetLoadCommand.class);
        cm.registerSubCommand(this, SettingsPresetListCommand.class);
        cm.registerSubCommand(this, SettingsPresetCreateCommand.class);
        cm.registerSubCommand(this, SettingsPresetDeleteCommand.class);

//        cm.registerMainCommand(this, HomeCommand.class);
//        cm.registerSubCommand(this, HomeCreateCommand.class);
//        cm.registerSubCommand(this, HomeDeleteCommand.class);
//        cm.registerSubCommand(this, HomeListCommand.class);
//        cm.registerSubCommand(this, HomeTPCommand.class);
//        cm.registerSubCommand(this, HomePositionCommand.class);

        cm.registerMainCommand(this, WarpCommand.class);
        cm.registerSubCommand(this, WarpCreateCommand.class);
        cm.registerSubCommand(this, WarpDeleteCommand.class);
        cm.registerSubCommand(this, WarpInfoCommand.class);
        cm.registerSubCommand(this, WarpListCommand.class);
        cm.registerSubCommand(this, WarpTPCommand.class);
        cm.registerSubCommand(this, WarpPrivacyCommand.class);
        cm.registerSubCommand(this, WarpBlacklistCommand.class);
        cm.registerSubCommand(this, WarpBlacklistAddCommand.class);
        cm.registerSubCommand(this, WarpBlacklistRemoveCommand.class);
        cm.registerSubCommand(this, WarpWhitelistCommand.class);
        cm.registerSubCommand(this, WarpWhitelistAddCommand.class);
        cm.registerSubCommand(this, WarpWhitelistRemoveCommand.class);

        cm.registerMainCommand(this, PingCommand.class);
        cm.registerMainCommand(this, PlaytimeCommand.class);
        cm.registerSubCommand(this, PlaytimeCheckCommand.class);
        cm.registerSubCommand(this, PlaytimeTopCommand.class);
        cm.registerMainCommand(this, IgnoreCommand.class);
        cm.registerMainCommand(this, UnignoreCommand.class);
        cm.registerMainCommand(this, IgnoredCommand.class);
        cm.registerMainCommand(this, GamemodeCommand.class);
        cm.registerMainCommand(this, ExplodeCommand.class);
        cm.registerMainCommand(this, AFKCommand.class);
        cm.registerMainCommand(this, BroadcastCommand.class);
        cm.registerMainCommand(this, EjectCommand.class);
        cm.registerMainCommand(this, VanishCommand.class);
        cm.registerMainCommand(this, EchoCommand.class);
        cm.registerMainCommand(this, SayCommand.class);
        cm.registerMainCommand(this, StaffChatCommand.class);

        cm.registerMainCommand(this, KickAllCommand.class);

        cm.registerMainCommand(this, ConnectionCommand.class);
        cm.registerSubCommand(this, ConnectionAddCommand.class);
        cm.registerSubCommand(this, ConnectionRemoveCommand.class);
        cm.registerSubCommand(this, ConnectionListCommand.class);
        cm.registerMainCommand(this, OnlineCommand.class);
        cm.registerSubCommand(this, OnlineDiscordCommand.class);
        // cm.registerSubCommand(this, OnlineDubtrackCommand.class);

        cm.registerMainCommand(this, ServiceBanCommand.class);
        cm.registerSubCommand(this, ServiceBanDiscordCommand.class);
        // cm.registerSubCommand(this, ServiceBanDubtrackCommand.class);

        cm.registerMainCommand(this, DiscordCommand.class);
        // cm.registerMainCommand(this, DubtrackCommand.class);

        cm.registerMainCommand(this, SynergyBotCommand.class);
        cm.registerSubCommand(this, SynergyBotLoginCommand.class);
        cm.registerSubCommand(this, SynergyBotLogoutCommand.class);

        cm.registerMainCommand(this, DonatorCommand.class);
        cm.registerSubCommand(this, DonatorAmountCommand.class);
        cm.registerSubCommand(this, DonatorAmountAddCommand.class);
        cm.registerSubCommand(this, DonatorAmountSetCommand.class);
        cm.registerSubCommand(this, DonatorAmountGetCommand.class);
        cm.registerSubCommand(this, DonatorSubscriptionStatusCommand.class);
        cm.registerSubCommand(this, DonatorSubscriptionStatusGetCommand.class);
        cm.registerSubCommand(this, DonatorSubscriptionStatusSetCommand.class);
        cm.registerSubCommand(this, DonatorUpdateStatusCommand.class);

        cm.registerMainCommand(this, SocialSpyCommand.class);
        cm.registerSubCommand(this, SocialSpyAddCommand.class);
        cm.registerSubCommand(this, SocialSpyRemoveCommand.class);
        cm.registerSubCommand(this, SocialSpyListCommand.class);

        cm.registerMainCommand(this, ServiceUnbanCommand.class);
        cm.registerSubCommand(this, ServiceUnbanDiscordCommand.class);
        // cm.registerSubCommand(this, ServiceUnbanDubtrackCommand.class);

        cm.registerMainCommand(this, FlyCommand.class);
        cm.registerMainCommand(this, HealCommand.class);
        cm.registerMainCommand(this, FeedCommand.class);
        cm.registerMainCommand(this, ListCommand.class);
        cm.registerMainCommand(this, EnderChestCommand.class);
        cm.registerMainCommand(this, ClearCommand.class);

        cm.registerMainCommand(this, SnoopCommand.class);
        cm.registerSubCommand(this, SnoopAddCommand.class);
        cm.registerSubCommand(this, SnoopRemoveCommand.class);
        cm.registerSubCommand(this, SnoopListCommand.class);

        cm.registerMainCommand(this, MailCommand.class);
        cm.registerSubCommand(this, MailReadCommand.class);
        cm.registerSubCommand(this, MailDeleteCommand.class);
        cm.registerSubCommand(this, MailSendCommand.class);
        cm.registerSubCommand(this, MailClearCommand.class);

        cm.registerMainCommand(this, FindCommand.class);
        cm.registerMainCommand(this, SuicideCommand.class);
        cm.registerMainCommand(this, MoreCommand.class);
        cm.registerMainCommand(this, MeCommand.class);
        cm.registerMainCommand(this, SpeedCommand.class);

        cm.registerMainCommand(this, PowerToolCommand.class);
        cm.registerSubCommand(this, PowerToolAddCommand.class);
        cm.registerSubCommand(this, PowerToolRemoveCommand.class);
        cm.registerSubCommand(this, PowerToolListCommand.class);
        cm.registerSubCommand(this, PowerToolClearCommand.class);

        cm.registerMainCommand(this, BookCommand.class);
        cm.registerMainCommand(this, NearCommand.class);
        cm.registerMainCommand(this, RulesCommand.class);
        cm.registerMainCommand(this, SkullCommand.class);
        cm.registerMainCommand(this, HatCommand.class);
        cm.registerMainCommand(this, AnnounceCommand.class);
        cm.registerMainCommand(this, FormattingCommand.class);
        cm.registerMainCommand(this, RanksCommand.class);
        cm.registerMainCommand(this, FAQCommand.class);
        cm.registerMainCommand(this, MOTDCommand.class);
        cm.registerMainCommand(this, PageCommand.class);
        cm.registerMainCommand(this, TimeCommand.class);
        cm.registerMainCommand(this, PersonalTimeCommand.class);
        cm.registerMainCommand(this, PersonalWeatherCommand.class);
        // cm.registerMainCommand(this, InvseeCommand.class);
        cm.registerMainCommand(this, SeenCommand.class);

        cm.registerMainCommand(this, LogCommand.class);
        cm.registerMainCommand(this, SynReloadCommand.class);
        cm.registerMainCommand(this, SynMigrateCommand.class);

        cm.registerMainCommand(this, SudoCommand.class);
        cm.registerSubCommand(this, SudoCommandCommand.class);
        cm.registerSubCommand(this, SudoMessageCommand.class);

        cm.registerMainCommand(this, HelpCommand.class);
        cm.registerSubCommand(this, HelpCommandCommand.class);
        cm.registerSubCommand(this, HelpSearchCommand.class);

        cm.registerMainCommand(this, ProfileCommand.class);
        cm.registerSubCommand(this, ProfileMinecraftCommand.class);
        cm.registerSubCommand(this, ProfileDiscordCommand.class);
        // cm.registerSubCommand(this, ProfileDubtrackCommand.class);

        cm.registerMainCommand(this, WeatherCommand.class);

        // TODO: Replace with something less gheto
        cm.registerMainCommand(this, BreakGlassCommand.class);
    }

    public void registerSettings() {
        SettingManager sm = SettingManager.getInstance();
        sm.registerSettings(CoreToggleSetting.class);
        sm.registerSettings(CoreMultiOptionSetting.class);
        sm.registerSettings(CoreSetSetting.class);
    }

    public void registerListeners() {
        PluginManager pm = Bukkit.getPluginManager();
        pm.registerEvents(new WorldListener(), this);
        pm.registerEvents(new PlayerConnectionListener(), this);
        pm.registerEvents(new PlayerMoveListener(), this);
        pm.registerEvents(new PlayerListener(), this);
        pm.registerEvents(new GUIListener(), this);
        pm.registerEvents(Chat.getInstance(), this);

        ProtocolManager protocolManager = ProtocolLibrary.getProtocolManager();
        protocolManager.addPacketListener(PacketListeners.nbtStripper);
        // DEBUG
        protocolManager.addPacketListener(PacketListeners.entityMetadataDebugger);
    }

    public void mapClasses() {
        MongoDB db = MongoDB.getInstance();
        // Map main DataEntities
        db.mapClasses(
                SynUser.class,
                DiscordProfile.class,
                DubtrackProfile.class,
                MinecraftProfile.class,
                WebsiteProfile.class,
                WorldGroupProfile.class,
                SettingPreferences.class,
                MemberApplication.class,
                Mail.class
        );
        // Also map embedded classes
        db.mapClasses(
                SerializableLocation.class,
                SettingDiffs.class,
                SettingPreset.class,
                PowerTool.class
        );
    }

    /**
     * Removes expired or corrupted data in the database.
     */
    private void cleanDatabase() {
        getLogger().info("Repairing damaged profiles...");
        DataManager dm = DataManager.getInstance();
        Datastore ds = MongoDB.getInstance().getDatastore();

        int repairedProfiles = 0;

        MorphiaCursor<MinecraftProfile> mcpIt = ds.find(MinecraftProfile.class).iterator(
                new FindOptions().projection().include("li", "lo", "wp"));
        while (mcpIt.hasNext()) {
            MinecraftProfile mcp = mcpIt.next();

            // Repair damaged logout times of their MinecraftProfile
            if (mcp.getLastLogOut() < mcp.getLastLogIn()) {
                mcp.setLastLogOut(mcp.getLastLogIn() + 1);
                repairedProfiles++;
            }

            // Repair damaged logout times of WorldGroupProfiles
            for (WorldGroupProfile wgp : dm.getPartialDataEntities(WorldGroupProfile.class, mcp.getWorldGroupProfileIDs(), "li", "lo")) {
                if (wgp.getLastLogOut() < wgp.getLastLogIn()) {
                    wgp.setLastLogOut(wgp.getLastLogIn() + 1);
                    repairedProfiles++;
                }
            }
        }
        mcpIt.close();
        getLogger().info("Repaired " + repairedProfiles + " damaged fields.");

        getLogger().info("Removing expired ServiceTokens...");

        MorphiaCursor<SynUser> synUserIt = ds.find(SynUser.class).iterator();
        while (synUserIt.hasNext()) {
            SynUser synUser = synUserIt.next();

            if (synUser.getDiscordToken() != null) {
                synUser.setDiscordToken(null);
            }
            if (synUser.getDubtrackToken() != null) {
                synUser.setDubtrackToken(null);
            }
        }
        synUserIt.close();


//        // Run this later due to relying on worlds being loaded
//        Bukkit.getScheduler().runTaskLater(this, () -> {
//            getLogger().info("Removing broken BoundSigns...");
//            int removedBoundSigns = 0;
//            for (BoundSign boundSign : ds.find(BoundSign.class).asList()) {
//                try {
//                    Location location = BukkitUtil.blockLocationFromString(boundSign.getID());
//
//                    if (!(location.getBlock().getState() instanceof Sign)) {
//                        dm.deleteDataEntity(boundSign);
//                        removedBoundSigns++;
//                    }
//                } catch (Exception e) {
//                    getLogger().info("Corrupted sign at " + boundSign.getID());
//                    e.printStackTrace();
//                }
//            }
//            getLogger().info("Removed " + removedBoundSigns + " BoundSigns.");
//        }, 0L);

    }

    /**
     * Convenience method for getting this plugin.
     *
     * @return The object representing this plugin.
     */
    public static SynergyCore getPlugin() {
        return (SynergyCore) getProvidingPlugin(SynergyCore.class);
    }

    public String[] getAliases() {
        return new String[]{getName(), "SynCore", "Core"};
    }

    /**
     * Attempts to get a registered <code>SynergyPlugin</code> for the given name.
     *
     * @param name The name to get a <code>SynergyPlugin</code> for.
     * @return The matching <code>SynergyPlugin</code>, if found, or null.
     */
    public SynergyPlugin getSynergyPlugin(String name) {
        for (SynergyPlugin plugin : synergyPlugins) {
            if (ListUtil.containsIgnoreCase(plugin.getAliases(), name)) {
                return plugin;
            }
        }
        return null;
    }

    /**
     * Gets all registered <code>SynergyPlugin</code>s.
     *
     * @return All registered <code>SynergyPlugin</code>s.
     */
    public Set<SynergyPlugin> getSynergyPlugins() {
        return synergyPlugins;
    }

    /**
     * Convenience method to get the configuration of this plugin.
     *
     * @return The YamlConfiguration representing the config of this plugin.
     */
    public static YamlConfiguration getPluginConfig() {
        return PluginConfig.getConfig(getPlugin());
    }

    /**
     * Loads and registers the given <code>SynergyPlugin</code>.
     *
     * @param plugin The <code>SynergyPlugin</code> to register.
     * @return True if the plugin was successfully registered.
     */
    public boolean registerSynergyPlugin(SynergyPlugin plugin) {
        if (synergyPlugins.add(plugin)) {
            getLogger().info("Loading plugin configuration for " + plugin.getName() + "...");
            PluginConfig.getInstance().load(plugin);

            getLogger().info("Loading messages for " + plugin.getName() + "...");
            Message.getInstance().load(plugin);

            // Connect to the database here if it's SynergyCore
            if (plugin instanceof SynergyCore) {
                getLogger().info("Connecting to the database...");
                MongoDB.getInstance().connect();
            }

            getLogger().info("Registering world group for " + plugin.getName() + "...");
            String wgName = PluginConfig.getConfig(plugin).getString("world_group.name");
            List<String> wgWorlds = PluginConfig.getConfig(plugin).getStringList("world_group.worlds");
            if (wgName != null && !registerWorldGroup(wgName, wgWorlds)) {
                return false;
            }

            getLogger().info("Registering commands for " + plugin.getName() + "...");
            plugin.registerCommands();

            getLogger().info("Registering settings for " + plugin.getName() + "...");
            plugin.registerSettings();

            getLogger().info("Registering listeners for " + plugin.getName() + "...");
            plugin.registerListeners();

            getLogger().info("Registering warps for " + plugin.getName() + "...");
            // Assume the warps are under a top level "warps" node
            ConfigurationSection warps = PluginConfig.getConfig(plugin).getConfigurationSection("warps");
            if (warps != null) {
                // Assume the warps are given in the format "warpName: locationAsString"
                for (String warpPath : warps.getKeys(false)) {
                    registerWarp(plugin, "warps." + warpPath);
                }
            }

            getLogger().info("Mapping classes for " + plugin.getName() + "...");
            plugin.mapClasses();
        }

        getLogger().info("Successfully registered " + plugin.getName() + ".");
        return true;
    }

    /**
     * Reloads the messages and configuration from disk for the given <code>SynergyPlugin</code>.
     * This method only reloads the plugin if it has already been registered.
     *
     * @param plugin The <code>SynergyPlugin</code> to reload.
     * @return True if the plugin was successfully reloaded.
     */
    public boolean reloadSynergyPlugin(SynergyPlugin plugin) {
        if (synergyPlugins.contains(plugin)) {
            // Unload the config for the specified plugin
            PluginConfig.getInstance().unload(plugin);
            // Unload the messages for the specified plugin
            Message.getInstance().unload(plugin);

            // Load the config for the specified plugin
            PluginConfig.getInstance().load(plugin);
            // Load the messages for the specified plugin
            Message.getInstance().load(plugin);

            // Update warps
            // Assume the warps are under a top level "warps" node
            ConfigurationSection warps = PluginConfig.getConfig(plugin).getConfigurationSection("warps");
            if (warps != null) {
                // Assume the warps are given in the format "warpName: locationAsString"
                for (String warpPath : warps.getKeys(false)) {
                    registerWarp(plugin, "warps." + warpPath);
                }
            }

            // Call onReload on the plugin
            plugin.onReload();
            return true;
        }
        return false;
    }

    /**
     * Registers a list of worlds to be handled as a single world group.
     *
     * @param worldGroupName The name of the world group to register.
     * @param worldNames A list of the names of worlds to register to the world group.
     * @return True if the world group was not already registered.
     */
    public static boolean registerWorldGroup(String worldGroupName, List<String> worldNames) {
        // If the world group name was already registered, return false
        if (worldGroups.containsValue(worldGroupName)) {
            return false;
        }
        // If no world names are provided, return false
        if (worldNames.size() == 0) {
            return false;
        }
        // If one of the world names that is to be registered is already registered, return false
        for (String worldName : worldNames) {
            if (worldGroups.containsKey(worldName)) {
                return false;
            }
        }

        for (String worldName : worldNames) {
            worldGroups.put(worldName, worldGroupName);
        }
        return true;
    }

    /**
     * Gets the name of the world group of a world.
     *
     * @param worldName The name of the world to search for.
     * @return The name of the world group that the world was found in, or an empty string if the world was not registered.
     */
    public static String getWorldGroupName(String worldName) {
        if (!worldGroups.containsKey(worldName)) {
            // Special world group name for unregistered worlds
            return "*";
        }
        return worldGroups.get(worldName);
    }

    /**
     * Checks whether the given string is a valid world group name.
     *
     * @param worldGroupName The name of the presumed world group to check.
     * @return True if it is a valid world group name.
     */
    public static boolean hasWorldGroupName(String worldGroupName) {
        if (worldGroupName == null) {
            return false;
        }
        if (worldGroupName.equals("*")) {
            return true;
        }

        for (String worldName : worldGroups.keySet()) {
            if (worldGroups.get(worldName).equalsIgnoreCase(worldGroupName)) {
                return true;
            }
        }
        return false;
    }

    /**
     * Gets the exact capitalization of a world group name if it is valid.
     *
     * @param worldGroupName The world group name to get the capitalization of.
     * @return The correct capitalization, or null if no match was found.
     */
    public static String getExactWorldGroupName(String worldGroupName) {
        if (worldGroupName == null) {
            return null;
        }
        if (worldGroupName.equals("*")) {
            return "*";
        }

        for (String worldName : worldGroups.keySet()) {
            if (worldGroups.get(worldName).equalsIgnoreCase(worldGroupName)) {
                return worldGroups.get(worldName);
            }
        }
        return null;
    }

    /**
     * Gets this plugin's ClassLoader. Used to pass the right reference to Morphia for populating objects.
     *
     * @return The ClassLoader of this plugin.
     */
    public ClassLoader getMongoHack() {
        return getClassLoader();
    }

    /**
     * Checks if the Discord bot is working and online.
     *
     * @return True if the bot is online.
     */
    public static boolean isDiscordBotOnline() {
        return discordClient != null && !(discordClient.getStatus() == JDA.Status.SHUTDOWN || discordClient.getStatus() == JDA.Status.SHUTTING_DOWN || discordClient.getStatus() == JDA.Status.FAILED_TO_LOGIN);
    }

    /**
     * Gets the <code>IDiscordClient</code> that is used to interface with the Discord bot.
     *
     * @return The <code>IDiscordClient</code> used to interface with the Discord bot.
     * @throws ServiceOfflineException if the integration is offline.
     */

    public static JDA getDiscordClient() throws ServiceOfflineException {
//        // Discord4J automaticall relogs so no need to do that here if the bot is offline; just give an error
//        if (!isDiscordBotOnline()) {
//            throw new ServiceOfflineException(ServiceType.DISCORD);
//        }
        return discordClient;
    }

    /**
     * Logs in the Discord bot.
     *
     * @param callback The callback function to be run once the task is completed. True if the login was successful.
     */
    public static void loginDiscordBot(Consumer<Boolean> callback) {
        BukkitTask task = Bukkit.getScheduler().runTaskAsynchronously(getPlugin(), () -> {
            // Log the time for the rate limiter
            lastDiscordLoginAttempt = System.currentTimeMillis();

            // Build the client if it doesn't already exist
            if (discordClient == null) {
                try {
                    discordClient = JDABuilder.createDefault(getPluginConfig().getString("discord.token"))
                            .addEventListeners(DiscordListener.getInstance())
                            .enableIntents(EnumSet.allOf(GatewayIntent.class))
                            .enableCache(CacheFlag.CLIENT_STATUS)
                            .build();
                    callback.accept(true);
                } catch (LoginException e) {
                    e.printStackTrace();
                    callback.accept(false);
                }
                }
            });
    }

    /**
     * Logs out the Discord bot.
     *
     * @param callback The callback function to be run once the task is completed. True if the logout was successful.
     */
    public static void logoutDiscordBot(Consumer<Boolean> callback) {
        BukkitTask task = Bukkit.getScheduler().runTaskAsynchronously(getPlugin(), () -> {
            // Ignore if the client hasn't been made
            if (discordClient == null) {
                callback.accept(false);
            }

            // Attempt to log out
            try {
                discordClient.shutdownNow();
                callback.accept(true);
                discordClient = null;
            } catch (Exception e) {
                e.printStackTrace();
                callback.accept(false);
            }
        });
    }

    /**
     * Logs out the Discord bot synchronously. Use <code>loginDiscordBot</code> whenever possible.
     *
     * @param callback The callback function to be run once the task is completed. True if the logout was successful.
     */
    public static void logoutDiscordBotSync(Consumer<Boolean> callback) {
        // Ignore if the client hasn't been made
        if (discordClient == null) {
            callback.accept(false);
        }

        // Attempt to log out
        try {
            discordClient.shutdownNow();
            callback.accept(true);
            discordClient = null;
        } catch (Exception e) {
            e.printStackTrace();
            callback.accept(false);
        }
    }

    /**
     * Checks if the Dubtrack bot is working and online.
     *
     * @return True if the bot is online.
     */
    public static boolean isDubtrackBotOnline() {
        return dubtrackClient != null && dubtrackClient.getAccount().getUuid() != null;
    }

    /**
     * Gets the <code>DubtrackAPI</code> that is used to interface with the Dubtrack bot.
     *
     * @return The <code>DubtrackAPI</code> that is used to interface with the Dubtrack bot.
     * @throws ServiceOfflineException if the integration is offline.
     */
    public static DubtrackAPI getDubtrackClient() throws ServiceOfflineException {
        if (!isDubtrackBotOnline()) {
            // Attempt to log in, limited by the value in the config
            if (System.currentTimeMillis() - lastDubtrackLoginAttempt >
                    getPluginConfig().getInt("service_bot_auto_relog_throttle_minutes") * TimeUtil.TimeUnit.MINUTE.getMilliseconds()) {
                SynergyCore.loginDubtrackBot(success -> {});
            }
            throw new ServiceOfflineException(ServiceType.DUBTRACK);
        }
        return dubtrackClient;
    }

    /**
     * Logs in the Dubtrack bot.
     *
     * @param callback The callback function to be run once the task is completed. True if the login was successful.
     */
    public static void loginDubtrackBot(Consumer<Boolean> callback) {
        BukkitTask task = Bukkit.getScheduler().runTaskAsynchronously(getPlugin(), () -> {
            // Log the time for the rate limiter
            lastDubtrackLoginAttempt = System.currentTimeMillis();

            // Build the client if it doesn't already exist
            if (dubtrackClient == null) {
                String dubtrackUsername = getPluginConfig().getString("dubtrack.username");
                String dubtrackPassword = getPluginConfig().getString("dubtrack.password");
                dubtrackClient = (DubtrackAPIImpl) new DubtrackBuilder(dubtrackUsername, dubtrackPassword).build();
            }

            // Attempt to log in
            try {
                dubtrackClient.login();
                dubtrackClient.joinRoom(getPluginConfig().getString("dubtrack.room"));
                DubtrackListener.getInstance().registerListeners();
                callback.accept(true);
            } catch (Exception e) {
                e.printStackTrace();
                callback.accept(false);
            }
        });
    };

    /**
     * Logs out the Dubtrack bot.
     *
     * @param callback The callback function to be run once the task is completed. True if the logout was successful.
     */
    public static void logoutDubtrackBot(Consumer<Boolean> callback) {
        BukkitTask task = Bukkit.getScheduler().runTaskAsynchronously(getPlugin(), () -> {
            // Ignore if the client hasn't been made
            if (dubtrackClient == null) {
                callback.accept(false);
            }

            // Attempt to log out
            try {
                DubtrackListener.getInstance().unregisterListeners();
                dubtrackClient.logout();
                callback.accept(true);
                dubtrackClient = null;
            } catch (Exception e) {
                e.printStackTrace();
                callback.accept(false);
            }
        });
    }

    /**
     * Logs out the Dubtrack bot synchronously. Use <code>logoutDubtrackBot</code> whenever possible.
     *
     * @param callback The callback function to be run once the task is completed. True if the logout was successful.
     */
    public static void logoutDubtrackBotSync(Consumer<Boolean> callback) {
        // Ignore if the client hasn't been made
        if (dubtrackClient == null) {
            callback.accept(false);
        }

        // Attempt to log out
        try {
            DubtrackListener.getInstance().unregisterListeners();
            dubtrackClient.logout();
            callback.accept(true);
            dubtrackClient = null;
        } catch (Exception e) {
            e.printStackTrace();
            callback.accept(false);
        }
    }

    /**
     * Gets the <code>Guild</code> (Discord server) that the bot is set to use.
     *
     * @return The server's Discord <code>Guild</code>.
     * @throws ServiceOfflineException if the integration is offline.
     */
    public static Guild getDiscordServer() throws ServiceOfflineException {
        return getDiscordClient().getGuildById(PluginConfig.getConfig(SynergyCore.getPlugin()).getLong("discord.server"));
    }

    /**
     * Gets the <code>Room</code> (Dubtrack room) that the bot is set to use.
     *
     * @return The server's Dubtrack <code>Room</code>.
     * @throws ServiceOfflineException if the integration is offline.
     */
    public static Room getDubtrackRoom() throws ServiceOfflineException {
        return getDubtrackClient().getRoom(PluginConfig.getConfig(SynergyCore.getPlugin()).getString("dubtrack.room"));
    }

    /**
     * Loads the server's custom icon, specified in config.yml.
     */
    public void loadServerIcon() {
        String iconFileName = PluginConfig.getConfig(SynergyCore.getPlugin()).getString("server_icon");
        File iconFile = new File(SynergyCore.getPlugin().getDataFolder(), iconFileName);

        if (iconFile.isFile()) {
            try {
                icon = Bukkit.getServer().loadServerIcon(iconFile);
            } catch (Exception e) {
                // Fall back to the default server icon in the main folder
                icon = Bukkit.getServerIcon();
                e.printStackTrace();
            }
        } else {
            // Fall back to the default server icon in the main folder
            icon = Bukkit.getServerIcon();
        }
    }

    /**
     * Gets the server's custom icon that is displayed in the server list.
     *
     * @return The server's custom icon.
     */
    public CachedServerIcon getIcon() {
        return icon;
    }

    /**
     * Gets the list of text files that are handled by this plugin.
     *
     * @return The names of this plugin's text files.
     */
    public String[] getTextNames() {
        return new String[]{"faq.txt", "formatting.txt", "motd.txt", "ranks.txt", "rules.txt"};
    }

    /**
     * Registers a warp defined in the given plugin's config file. The last node in the given configuration path
     * is assumed to be the warp name, and the value at the path is assumed to be a location encoded as a string in
     * the format produced by <code>BukkitUtil#locationFromString</code>.
     *
     * @param plugin The <code>SynergyPlugin</code> that is registering the warp.
     * @param configWarpPath The path in the plugin's config to the warp to register.
     * @return True if the warp was successfully registered or had its details updated.
     */
    public static boolean registerWarp(SynergyPlugin plugin, String configWarpPath) {
        // Assume the warp name is the last node in the path
        String[] nodes = configWarpPath.split("\\.");
        String warpName = nodes[nodes.length - 1];

        // Parse the location
        String locationAsString = PluginConfig.getConfig(plugin).getString(configWarpPath);
        Location location = BukkitUtil.locationFromString(locationAsString);

        // If the world of the location is null then it hasn't been properly loaded and we should avoid registering this warp
        if (location.getWorld() == null) {
            plugin.getLogger().info("The world appears to be unloaded for the warp: " + warpName + ". Aborting registration for this warp.");
            return false;
        }

        // Search the database for an existing warp
        List<Warp> warps = Warp.getWarps(warpName, SERVER_ID, WarpCommand.WarpOwnerType::getTargetPriority, false, true);

        // Return if an error was encountered
        if (warps == null) {
            plugin.getLogger().info("Error registering the warp: " + warpName);
            return false;
        }

        if (warps.size() == 0) {
            // If no warp was found then create a new warp and save it in the database
            Warp warp = new Warp(warpName, SERVER_ID, location);
            DataManager.getInstance().saveDataEntity(warp);

            // Set the warp's privacy
            warp.setPrivacy(Warp.PrivacyLevel.PUBLIC);

            plugin.getLogger().info("Successfully registered the server warp: " + warpName);
            return true;
        } else {
            // Otherwise check to see if the warp's location needs updating
            Warp warp = warps.get(0);
            if (!warp.getLocation().equals(location)) {
                warp.setLocation(location);

                plugin.getLogger().info("Successfully updated the server warp: " + warpName);
                return true;
            }

            plugin.getLogger().info("No changes were made to the warp: " + warpName);
            return false;
        }
    }
}