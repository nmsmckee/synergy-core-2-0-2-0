package net.synergyserver.synergycore.guis;

import net.synergyserver.synergycore.utils.ItemGUIUtil;
import org.bukkit.Bukkit;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.InventoryHolder;

/**
 * This represents a view of a <code>GUI</code>. <code>Itemizable</code>s
 * are linked to their items in the inventory by their index.
 */
public class GUIView {

    private GUI gui;
    private Itemizable[][] itemizables;
    private Inventory inventory;

    /**
     * Creates a blank <code>GUIView</code> with the given parameters.
     *
     * @param gui The <code>GUI</code> that owns this view.
     * @param inventoryHolder The InventoryHolder of the inventory.
     * @param height The height of the inventory.
     * @param width The width of the inventory. Should be nine until a future Minecraft update allows otherwise.
     */
    public GUIView(GUI gui, InventoryHolder inventoryHolder, int height, int width) {
        this.itemizables = new Itemizable[height][width];
        this.inventory = Bukkit.createInventory(inventoryHolder, height * width);
    }

    /**
     * Creates a new <code>GUIView</code> with the given parameters. Unintended effects
     * may occur if the sizes of the array of itemizables and the inventory differ.
     *
     * @param gui The <code>GUI</code> that owns this view.
     * @param itemizables An array of itemizables to populate the inventory with.
     * @param inventory The inventory that is displayed to the player.
     */
    public GUIView(GUI gui, Itemizable[][] itemizables, Inventory inventory) {
        this.itemizables = itemizables;
        this.inventory = inventory;
    }

    /**
     * Gets the <code>GUI</code> that owns this <code>GUIView</code>.
     *
     * @return The owning <code>GUI</code>.
     */
    public GUI getGui() {
        return gui;
    }

    /**
     * Gets the <code>Itemizable</code>s used for this <code>GUIView</code>.
     *
     * @return The <code>Itemizable</code>s of this view.
     */
    public Itemizable[][] getItemizables() {
        return itemizables;
    }

    /**
     * Sets the <code>Itemizable</code>s used by this <code>GUIView</code>.
     *
     * @param itemizables The new <code>Itemizable</code>s of this view.
     */
    public void setItemizables(Itemizable[][] itemizables) {
        this.itemizables = itemizables;
    }

    /**
     * Gets the inventory of this <code>GUIView</code>.
     *
     * @return The inventory of this view.
     */
    public Inventory getInventory() {
        return inventory;
    }

    /**
     * Sets the inventory of this <code>GUIView</code>.
     *
     * @param inventory The new inventory of this view.
     */
    public void setInventory(Inventory inventory) {
        this.inventory = inventory;
    }

    /**
     * Gets an <code>Itemizable</code> using a one dimensional index.
     *
     * @param index The one dimensional index to get the <code>Itemizable</code> at.
     * @return The <code>Itemizable</code> at that index, or null if there is none there.
     */
    public Itemizable getItemizable(int index) {
        int yIndex = ItemGUIUtil.toTwoDimensionalIndexY(index, itemizables[0].length);
        int xIndex = ItemGUIUtil.toTwoDimensionalIndexX(index, itemizables[0].length);
        return itemizables[yIndex][xIndex];
    }

    /**
     * Gets an <code>Itemizable</code> using two dimensional indexes.
     *
     * @param yIndex The Y index to get the <code>Itemizable</code> at.
     * @param xIndex The X index to get the <code>Itemizable</code> at.
     * @return The <code>Itemizable</code> at those coordinates, or null if there is none there.
     */
    public Itemizable getItemizable(int yIndex, int xIndex) {
        return itemizables[yIndex][xIndex];
    }

    /**
     * Sets an <code>Itemizable</code> using a one dimensional index. This
     * method also updates the inventory of this <code>GUIView</code> as well.
     *
     * @param itemizable The <code>Itemizable</code> to set.
     * @param parameter The parameter to use when generating the item.
     * @param index The one dimensional index to set the <code>Itemizable</code> at.
     */
    public void setItemizable(Itemizable itemizable, Object parameter, int index) {
        int yIndex = ItemGUIUtil.toTwoDimensionalIndexY(index, itemizables[0].length);
        int xIndex = ItemGUIUtil.toTwoDimensionalIndexX(index, itemizables[0].length);
        itemizables[yIndex][xIndex] = itemizable;
        inventory.setItem(index, ItemGUIUtil.itemize(itemizable, parameter));
    }

    /**
     * Sets an <code>Itemizable</code> using two dimensional indexes. This
     * method also updates the inventory of this <code>GUIView</code> as well.
     *
     * @param itemizable The <code>Itemizable</code> to set.
     * @param parameter The paramater to use when generating the item.
     * @param yIndex The Y index to set the <code>Itemizable</code> at.
     * @param xIndex The X index to set the <code>Itemizable</code> at.
     */
    public void setItemizable(Itemizable itemizable, Object parameter, int yIndex, int xIndex) {
        int index = ItemGUIUtil.toOneDimensionalIndex(yIndex, xIndex, itemizables[0].length);
        itemizables[yIndex][xIndex] = itemizable;
        inventory.setItem(index, ItemGUIUtil.itemize(itemizable, parameter));
    }
}
