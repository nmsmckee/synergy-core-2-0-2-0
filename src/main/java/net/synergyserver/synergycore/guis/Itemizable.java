package net.synergyserver.synergycore.guis;

import org.bukkit.inventory.ItemStack;

/**
 * Represents something that is able to be displayed in an item-based GUI.
 */
public interface Itemizable {

    /**
     * Gets the display name to use as the name of the item in an item-based GUI for this <code>Itemizable</code>.
     *
     * @return The display name of this item.
     */
    String getDisplayName();

    /**
     * Gets the description to include in the lore of this item in an item-based GUI for this <code>Itemizable</code>.
     *
     * @return The description of this item.
     */
    String getDescription();

    /**
     * Gets the item to use in an item-based GUI for this <code>Itemizable</code>, based on the given parameter.
     *
     * @param parameter The parameter to alter the outputted item.
     * @return The <code>ItemStack</code> to use while rendering this <code>Itemizable</code>.
     */
    ItemStack getItem(Object parameter);

}
