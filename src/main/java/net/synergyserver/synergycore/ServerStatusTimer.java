package net.synergyserver.synergycore;

import net.synergyserver.synergycore.configs.PluginConfig;
import net.synergyserver.synergycore.utils.TimeUtil;
import org.bukkit.Bukkit;
import org.bukkit.craftbukkit.v1_19_R1.CraftServer;

import java.io.File;
import java.io.IOException;
import java.util.TimerTask;

public class ServerStatusTimer {

    private static long tpsLastAlerted = 0L;

    public static Runnable tpsAlertTimer = () -> {
        double[] recentTPS = ((CraftServer) Bukkit.getServer()).getServer().recentTps;
        double threshold = PluginConfig.getConfig(SynergyCore.getPlugin()).getDouble("low_tps_alert.threshold");

        // If the tps was not below the threshold for the last 5 min then return
        if (!(recentTPS[0] < threshold && recentTPS[1] < threshold)) {
            return;
        }

        long cooldown = TimeUtil.toRoundedUnit(TimeUtil.TimeUnit.MINUTE, TimeUtil.TimeUnit.MILLI,
                PluginConfig.getConfig(SynergyCore.getPlugin()).getInt("low_tps_alert.delay_between_alerts_minutes"));

        // If not enough time has passed since the last alert then return
        if (System.currentTimeMillis() - tpsLastAlerted < cooldown) {
            return;
        }

        // Should be true if using Spigot and false if using Paper
        boolean handleSampleLength = PluginConfig.getConfig(SynergyCore.getPlugin()).getBoolean("low_tps_alert.handle_sample_length");

        if (handleSampleLength) {
            int sampleLength = PluginConfig.getConfig(SynergyCore.getPlugin()).getInt("low_tps_alert.sample_length_minutes");

            // Reset the timings and schedule a task to paste it after the configured time
            Bukkit.dispatchCommand(Bukkit.getConsoleSender(), "timings reset");
            Bukkit.getScheduler().runTaskLater(SynergyCore.getPlugin(), () -> {
                Bukkit.getConsoleSender().sendMessage("Detected low timings. Dumping a " + sampleLength + " minute sample of the timings for analysis.");
                Bukkit.dispatchCommand(Bukkit.getConsoleSender(), "timings paste");
            }, TimeUtil.toFlooredUnit(TimeUtil.TimeUnit.MINUTE, TimeUtil.TimeUnit.GAME_TICK, sampleLength));
        } else {
            Bukkit.getConsoleSender().sendMessage("Detected low timings. Dumping a sample of the timings for analysis.");
            Bukkit.dispatchCommand(Bukkit.getConsoleSender(), "timings paste");
        }
    };

    public static Runnable storageMonitorTimer = () -> {
        long bytesAvailable = (new File("/")).getUsableSpace();

        // If the bytes available is less than the value in the config then shut down the server to avoid corruption
        if (bytesAvailable / 1024 / 1024 / 1024 < PluginConfig.getConfig(SynergyCore.getPlugin()).getInt("storage_monitor.threshold_gb")) {
            // Save as much data as possible and warn players
            Bukkit.dispatchCommand(Bukkit.getConsoleSender(), "save-all");
            Bukkit.broadcastMessage("The server is performing an emergency shutdown to preserve data integrity. Apologies for the inconvenience.");

            // Schedule a force kill just in case the server hangs on shutdown (e.g. CoreProtect data purge)
            long forceKillDelay = TimeUtil.toFlooredUnit(TimeUtil.TimeUnit.MILLI,
                    TimeUtil.TimeUnit.GAME_TICK, (long) PluginConfig.getConfig(SynergyCore.getPlugin()).getDouble("storage_monitor.delay_force_kill_minutes"));
            // Schedule it using Java's timer since Bukkit will be hanging
            new java.util.Timer().schedule(
                    new TimerTask() {
                        @Override
                        public void run() {
                            try {
                                String pid = java.lang.management.ManagementFactory.getRuntimeMXBean().getName().split("@")[0];
                                Runtime.getRuntime().exec("kill -9 " + pid);
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        }
                    },
                    forceKillDelay
            );

            // Shutdown the server
            Bukkit.shutdown();
        }
    };

}
