package net.synergyserver.synergycore.utils;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.regex.Pattern;

/**
 * Utility to parse and alter strings.
 */
public class StringUtil {

    // Discord's subset of markdown formatting characters
    public static final Pattern MARKDOWN_SPECIAL_CHARACTERS = Pattern.compile("(\\\\\\\\|[\\*_~`\\|>])");

    /**
     * Splits words based on common indicators, such as camel-casing and underscores,
     * and returns the list of their starting indexes. Separators like underscores
     * and numbers are included at the end of a word, rather than the beginning.
     *
     * @param str The string to split up into its words.
     * @return A list of the starting indexes of the string given.
     */
    public static List<Integer> splitWordsIntoIndexes(String str) {
        // If the string is short enough then just return it
        if (str.length() <= 3) {
            return ListUtil.makeList(0);
        }

        List<Integer> output = new ArrayList<>();
        output.add(0);

        for (int i = 1; i < str.length(); i++) {
            char prevChar = str.charAt(i - 1);
            char currChar = str.charAt(i);

            // If the current index is a letter and the previous index wasn't, then start a new word
            if (!Character.isLetter(prevChar) && Character.isLetter(currChar)) {
                output.add(i);
                continue;
            }

            // If the current index is an uppercase letter and the previous index wasn't, then start a new word
            if (Character.isLowerCase(prevChar) && Character.isUpperCase(currChar)) {
                output.add(i);
            }
        }

        return output;
    }

    /**
     * Splits words into a list of words based on common indicators, such as camel-casing and underscores.
     * Separators like underscores and numbers are included at the end of a word, rather than the beginning.
     *
     * @param str The string to split up into its words.
     * @return A list of the words of the string given.
     */
    public static List<String> splitWordsIntoWords(String str) {
        List<Integer> splitIndexes = splitWordsIntoIndexes(str);

        // If just one index then return the string
        if (splitIndexes.size() == 1) {
            return ListUtil.makeList(str);
        }

        List<String> output = new ArrayList<>();
        for (int i = 1; i < splitIndexes.size(); i++) {
            output.add(str.substring(splitIndexes.get(i - 1), splitIndexes.get(i)));
        }

        // Add the final word and return
        output.add(str.substring(splitIndexes.get(splitIndexes.size() - 1)));
        return output;
    }

    /**
     * Checks whether the given string is equal to one of the synonymous strings, ignoring case.
     *
     * @param toCompare The string to compare.
     * @param synonymousStrings A list of synonymous strings to compare the input to.
     * @return True if a match was found.
     */
    public static boolean equalsIgnoreCase(String toCompare, String... synonymousStrings) {
        for (String str : synonymousStrings) {
            if (toCompare.equalsIgnoreCase(str)) {
                return true;
            }
        }
        return false;
    }

    /**
     * Checks if the given string contains only alphanumeric characters and underscores.
     *
     * @param toCheck The string to check.
     * @return True if string contains only alphanumeric characters and underscores.
     */
    public static boolean isWord(String toCheck) {
        return toCheck.matches("\\w+$");
    }

    /**
     * Generates a randomized alphanumeric string with the given length.
     *
     * @param length The length of the random string to generate.
     * @return The random string.
     */
    public static String generateRandomString(int length) {
        StringBuilder randomString = new StringBuilder();
        Random random = new Random();

        for (int i = 0; i < length; i++) {
            int rand = random.nextInt(62);

            if (rand < 10) {
                // Turn it into a number if it's one of the first 10 numbers
                randomString.append((char) (rand + 48));
            } else if (rand < 36) {
                // Turn it into an uppercase letter if it's one of the next 26 numbers
                randomString.append((char) (rand + 55));
            } else {
                // Turn it into a lowercase letter if it's one of the last 26 numbers
                randomString.append((char) (rand + 61));
            }
        }

        return randomString.toString();
    }

    /**
     * Takes an input string and capitalizes it as if it was the first word in a sentence. (Ex: eXaMPlE -> Example)
     *
     * @param input The word to capitalize properly.
     * @return The capitalized word.
     */
    public static String capitalize(String input) {
        if (input.length() > 1) {
            return input.substring(0, 1).toUpperCase() + input.substring(1, input.length()).toLowerCase();
        } else {
            return input.toUpperCase();
        }
    }

    /**
     * Checks if a string contains another string, ignoring case.
     *
     * @param str The containing string.
     * @param innerStr The inner string.
     * @return True if the inner string is contained in the other string.
     */
    public static boolean containsIgnoreCase(String str, String innerStr) {
        return str.toLowerCase().contains(innerStr.toLowerCase());
    }

    /**
     * Gets the markdown-escaped version of the given string.
     *
     * @param str The string to escape.
     * @return The escaped string.
     */
    public static String escapeMarkdown(String str) {
        return str.replaceAll(MARKDOWN_SPECIAL_CHARACTERS.pattern(), "$1");
    }
}
