package net.synergyserver.synergycore.utils;

import net.synergyserver.synergycore.MessageArgument;

import java.util.List;
import java.util.Random;

public class MathUtil {


    /**
     * Checks if a string is a valid integer.
     *
     * @param toCheck The string to check.
     * @return True if the string is an integer.
     */
    public static boolean isInteger(String toCheck) {
        try {
            Integer.parseInt(toCheck);
            return true;
        }
        catch (Exception e) {
            return false;
        }
    }

    /**
     * Checks if a string is a positive integer.
     *
     * @param toCheck The string to check.
     * @return True if the string is a positive integer.
     */
    public static boolean isPositiveInteger(String toCheck) {
        return isInteger(toCheck) && (Integer.parseInt(toCheck) > 0);
    }

    /**
     * Checks if a string is a valid double.
     *
     * @param toCheck The string to check.
     * @return True if the string is a double.
     */
    public static boolean isDouble(String toCheck) {
        try {
            Double.parseDouble(toCheck);
            return true;
        }
        catch (Exception e) {
            return false;
        }
    }

    /**
     * Checks if a string is a valid float.
     *
     * @param toCheck The string to check.
     * @return True if the string is a float.
     */
    public static boolean isFloat(String toCheck) {
        try {
            Float.parseFloat(toCheck);
            return true;
        }
        catch (Exception e) {
            return false;
        }
    }

    /**
     * Finds the nearest preceding number in the string given from the index, if it exists.
     * If no number is found then this method returns null.
     *
     * @param toSearch The string to search.
     * @param startIndex The index to start looking backwards from.
     * @return The number, if found, or null.
     */
    public static String findPrecedingNumber(String toSearch, int startIndex) {
        // Look for the end of the first number before the index given
        int numEndIndex = -1;
        for (int i = startIndex; i >= 0; i--) {
            if (Character.isDigit(toSearch.charAt(i))) {
                numEndIndex = i;
                break;
            }
        }

        // If no end index was found for a number then return null
        if (numEndIndex == -1) {
            return null;
        }

        // Look for the start of the first number before the index given
        int numStartIndex = -1;
        boolean foundDecimal = false;
        for (int i = numEndIndex; i >= 0; i--) {
            if (i == 0) {
                numStartIndex = 0;
                break;
            }

            // Be sure to account for decimal numbers
            if (toSearch.charAt(i - 1) == '.' && !foundDecimal) {
                foundDecimal = true;
                continue;
            }

            if (!Character.isDigit(toSearch.charAt(i - 1))) {
                numStartIndex = i;
                break;
            }

            // Check to make sure the number isn't part of a colorcode
            if (i - 2 >= 0 && Character.isDigit(toSearch.charAt(i - 1)) && toSearch.charAt(i - 2) == '§') {
                numStartIndex = i;
                break;
            }
        }

        // Parse the int and return it
        return toSearch.substring(numStartIndex, numEndIndex + 1);
    }

    /**
     * Finds the nearest preceding number in the list of
     * <code>MessageArguments</code> given from the index, if it exists.
     * If no number is found then this method returns null.
     *
     * @param args The list of arguments to search.
     * @param startIndex The index to start looking backwards from.
     * @return The number, if found, or null.
     */
    public static String findPrecedingNumber(List<MessageArgument> args, int startIndex) {
        for (int i = args.size() - 1; i >= 0; i--) {
            MessageArgument arg = args.get(i);

            // Ignore the argument if it is after startIndex
            if (arg.getStartingIndex() > startIndex) {
                continue;
            }

            // Return if a number was found
            String number = findPrecedingNumber(arg.getText(), arg.getText().length() - 1);
            if (number != null) {
                return number;
            }
        }

        // Since no number was found, return null
        return null;
    }

    /**
     * Finds the nearest following number in the string given from the index, if it exists.
     * If no number is found then this method returns null.
     *
     * @param toSearch The string to search.
     * @param startIndex The index to start looking forwards from.
     * @return The number, if found, or null.
     */
    public static String findFollowingNumber(String toSearch, int startIndex) {
        // Look for the beginning of the first number after the index given
        int numStartIndex = -1;
        for (int i = startIndex; i < toSearch.length(); i++) {
            if (Character.isDigit(toSearch.charAt(i))) {
                numStartIndex = i;
                break;
            }
        }

        // If no start index was found for a number then return null
        if (numStartIndex == -1) {
            return null;
        }

        // Look for the end of the first number after the index given
        int numEndIndex = -1;
        boolean foundDecimal = false;
        for (int i = numStartIndex; i < toSearch.length(); i++) {
            if (i == toSearch.length() - 1) {
                numEndIndex = toSearch.length() - 1;
                break;
            }

            // Be sure to account for decimal numbers
            if (toSearch.charAt(i + 1) == '.' && !foundDecimal) {
                foundDecimal = true;
                continue;
            }

            if (!Character.isDigit(toSearch.charAt(i + 1))) {
                numEndIndex = i;
                break;
            }
        }

        // Parse the int and return it
        return toSearch.substring(numStartIndex, numEndIndex + 1);
    }

    /**
     * Finds the nearest following number in the list of
     * <code>MessageArguments</code> given from the index, if it exists.
     * If no number is found then this method returns null.
     *
     * @param args The list of arguments to search.
     * @param startIndex The index to start looking forwards from.
     * @return The number, if found, or null.
     */
    public static String findFollowingNumber(List<MessageArgument> args, int startIndex) {
        for (int i = 0; i < args.size(); i++) {
            MessageArgument arg = args.get(i);

            // Ignore the argument if it is before startIndex
            if (arg.getStartingIndex() < startIndex) {
                continue;
            }

            // Return if a number was found
            String number = findFollowingNumber(arg.getText(), arg.getText().length() - 1);
            if (number != null) {
                return number;
            }
        }

        // Since no number was found, return null
        return null;
    }

    /**
     * Randomly returns true with the given probability. 1.0 = 100%
     * chance of returning true and 0.0 = 0% chance of returning true.
     *
     * @param probability The probably of this method returning true. Logical inputs are in (0, 1).
     * @return Randomly true based on the given probability.
     */
    public static boolean randChance(double probability) {
        return probability > Math.random();
    }

    /**
     * Generates a random integer with the given minimum and maximum bounds.
     *
     * @param min The minimum value the output can be, inclusive.
     * @param max The maximum value the output can be, inclusive.
     * @return A random input with the given bounds.
     */
    public static int randInt(int min, int max) {
        return new Random().nextInt((max - min) + 1) + min;
    }
}
