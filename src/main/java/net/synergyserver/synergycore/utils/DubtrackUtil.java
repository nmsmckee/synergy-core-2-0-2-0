package net.synergyserver.synergycore.utils;

import io.sponges.dubtrack4j.framework.Room;
import net.synergyserver.synergycore.SynergyCore;

import java.io.IOException;

/**
 * Utility to handle things related to dubtrack.
 */
public class DubtrackUtil {

    /**
     * Checks if the given user is online.
     *
     * @param dubtrackID The id of the user to check.
     * @return True if the user is online in the given room.
     */
    public static boolean isOnline(String dubtrackID) {
        Room room = SynergyCore.getDubtrackRoom();

        try {
            return room.getUserById(dubtrackID) != null;
        } catch (IOException e) {
            return false;
        }
    }
}
