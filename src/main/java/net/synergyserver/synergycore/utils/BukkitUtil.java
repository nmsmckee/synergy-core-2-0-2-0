package net.synergyserver.synergycore.utils;

import org.bukkit.Bukkit;
import org.bukkit.Chunk;
import org.bukkit.Color;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.craftbukkit.v1_19_R1.CraftServer;
import org.bukkit.plugin.Plugin;

import java.util.HashMap;
import java.util.Map;

/**
 * Utility to assist with preforming tasks involving Bukkit objects.
 */
public class BukkitUtil {
    public static final Map<String,Color> colors = new HashMap<String,Color>(){{
        put("AQUA", Color.AQUA);
        put("BLACK", Color.BLACK);
        put("BLUE", Color.BLUE);
        put("FUCHSIA", Color.FUCHSIA);
        put("GRAY", Color.GRAY);
        put("GREEN", Color.GREEN);
        put("LIME", Color.LIME);
        put("MAROON", Color.MAROON);
        put("NAVY", Color.NAVY);
        put("OLIVE", Color.OLIVE);
        put("ORANGE", Color.ORANGE);
        put("PURPLE", Color.PURPLE);
        put("RED", Color.RED);
        put("SILVER", Color.SILVER);
        put("TEAL", Color.TEAL);
        put("WHITE", Color.WHITE);
        put("YELLOW", Color.YELLOW);
    }};

    /**
     * Checks if a string is a valid 6 digit hexadecimal color code
     * in a valid range (from 000000 to FFFFFF) using regex.
     *
     * @param colorCode The string to validate.
     * @return True if string is a 6 digit hexadecimal color code.
     */
    public static boolean isValidColorCode(String colorCode){
        return colorCode.matches("(?i)(([0-9]|[A-F]){6})");
    }

    /**
     * Checks if a string is a valid color name.
     *
     * @param colorName The string to validate.
     * @return True if the name is a valid color name.
     */
    public static boolean isValidColorName(String colorName){
        return colors.containsKey(colorName.toUpperCase());
    }

    /**
     * Parses a string to get the associated color
     *
     * @param colorCode The string to parse.
     * @return The color associated with the string, if found, otherwise null.
     */
    public static Color parseColorCode(String colorCode){
        if(isValidColorCode(colorCode)) {
            return Color.fromRGB(Integer.parseInt(colorCode, 16));
        } else {
            return null;
        }
    }

    /**
     * Parses a string to get the associated color
     *
     * @param colorName The string to parse.
     * @return The color associated with the string, if found, otherwise null.
     */
    public static Color parseColorName(String colorName){
        if(isValidColorName(colorName)){
            return colors.get(colorName.toUpperCase());
        } else {
            return null;
        }
    }

    /**
     * Serializes a location into a string.
     *
     * @param location The location to turn into a string.
     * @return The serialized location as a string.
     */
    public static String locationToString(Location location) {
        return location.getWorld().getName() + ";" +
                location.getX() + ";" +
                location.getY() + ";" +
                location.getZ() + ";" +
                location.getYaw() + ";" +
                location.getPitch();
    }

    /**
     * Deserializes a location from a string.
     *
     * @param locationAsString The location as a string.
     * @return The deserialized location.
     */
    public static Location locationFromString(String locationAsString) {
        String[] locationComponents = locationAsString.split(";");

        World world = Bukkit.getWorld(locationComponents[0]);
        double x = Double.parseDouble(locationComponents[1]);
        double y = Double.parseDouble(locationComponents[2]);
        double z = Double.parseDouble(locationComponents[3]);
        float yaw = Float.parseFloat(locationComponents[4]);
        float pitch = Float.parseFloat(locationComponents[5]);
        return new Location(world, x, y, z, yaw, pitch);
    }

    /**
     * Serializes a location of a block into a string, ignoring decimal places and orientation.
     *
     * @param location The location to turn into a string.
     * @return The serialized location as a string.
     */
    public static String blockLocationToString(Location location) {
        return location.getWorld().getName() + ";" + location.getBlockX() + ";" + location.getBlockY() + ";" + location.getBlockZ();
    }

    /**
     * Deserializes a location from a string.
     *
     * @param locationAsString The location as a string.
     * @return The deserialized location.
     */
    public static Location blockLocationFromString(String locationAsString) {
        String[] locationComponents = locationAsString.split(";");

        World world = Bukkit.getWorld(locationComponents[0]);
        int x = Integer.parseInt(locationComponents[1]);
        int y = Integer.parseInt(locationComponents[2]);
        int z = Integer.parseInt(locationComponents[3]);
        return new Location(world, x, y, z);
    }

    /**
     * Gets the world;x;z ID of the chunk that a location is in.
     *
     * @param loc The location to get the chunk ID from.
     * @return The location's chunk ID.
     */
    public static String getChunkId(Location loc) {
        return loc.getWorld().getName() + ";" + (loc.getBlockX() >> 4) + ";" + (loc.getBlockZ() >> 4);
    }

    /**
     * Gets the world;x;z ID of a chunk.
     *
     * @param chunk The chunk to get the ID of.
     * @return The chunk's ID.
     */
    public static String getChunkId(Chunk chunk) {
        return chunk.getWorld().getName() + ";" + chunk.getX() + ";" + chunk.getZ();
    }

    /**
     * Attempts to parse a <code>Plugin</code> from the given string.
     *
     * @param name The name of the plugin to get.
     * @return The plugin, if found, or null.
     */
    public static Plugin parsePlugin(String name) {
        Plugin matchedPlugin = null;
        for (Plugin plugin : Bukkit.getPluginManager().getPlugins()) {
            // If an exact match was found then return that
            if (plugin.getName().equalsIgnoreCase(name)) {
                return plugin;
            } else if (StringUtil.containsIgnoreCase(plugin.getName(), name)) {
                // Otherwise if a loose match was found then save it for later
                matchedPlugin = plugin;
            }
        }
        return matchedPlugin;
    }

    /**
     * Gets the current tick of the server. Starts at 0 on server restart.
     *
     * @return The ticks of the server.
     */
    public static int getTicks() {
        // NMS: Get MinecraftServer#ticks
        // Look for what is being incremented and modded for autosave
        return ((CraftServer) Bukkit.getServer()).getServer().ag();
    }

}
