package net.synergyserver.synergycore.utils;

import dev.morphia.Datastore;
import dev.morphia.query.FindOptions;
import dev.morphia.query.experimental.filters.Filters;
import net.md_5.bungee.api.chat.BaseComponent;
import net.md_5.bungee.chat.ComponentSerializer;
import net.minecraft.network.chat.IChatBaseComponent;
import net.minecraft.world.entity.player.EntityHuman;
import net.synergyserver.synergycore.Chat;
import net.synergyserver.synergycore.ChatColor;
import net.synergyserver.synergycore.Rank;
import net.synergyserver.synergycore.SynergyCore;
import net.synergyserver.synergycore.configs.Message;
import net.synergyserver.synergycore.database.MongoDB;
import net.synergyserver.synergycore.profiles.MinecraftProfile;
import net.synergyserver.synergycore.profiles.ProfileManager;
import net.synergyserver.synergycore.profiles.WorldGroupProfile;
import net.synergyserver.synergycore.settings.CoreMultiOptionSetting;
import net.synergyserver.synergycore.settings.CoreSetSetting;
import net.synergyserver.synergycore.settings.CoreToggleSetting;
import net.synergyserver.synergycore.settings.SettingDiffs;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.OfflinePlayer;
import org.bukkit.World;
import org.bukkit.command.CommandSender;
import org.bukkit.craftbukkit.v1_19_R1.entity.CraftEntity;
import org.bukkit.craftbukkit.v1_19_R1.entity.CraftPlayer;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.scoreboard.Scoreboard;
import org.kitteh.vanish.VanishPlugin;
import ru.tehkode.permissions.PermissionGroup;
import ru.tehkode.permissions.PermissionUser;
import ru.tehkode.permissions.bukkit.PermissionsEx;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.UUID;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

/**
 * Utility to assist with player management.
 */
public class PlayerUtil {

    /**
     * Checks if the given player is vanished.
     *
     * @param player The player to check.
     * @return True if the player is vanished.
     */
    public static boolean isVanished(Player player) {
        VanishPlugin vnp = (VanishPlugin) Bukkit.getPluginManager().getPlugin("VanishNoPacket");
        return vnp.getManager().isVanished(player);
    }

    /**
     * Sets the vanished state of a player.
     *
     * @param player The player to set the vanished state of.
     * @param vanish True to vanish the player.
     * @param isSilent True to make it unannounced and without effects.
     */
    public static void setVanished(Player player, boolean vanish, boolean isSilent) {
        VanishPlugin vnp = (VanishPlugin) Bukkit.getPluginManager().getPlugin("VanishNoPacket");

        if (vanish) {
            vnp.getManager().vanish(player, isSilent, !isSilent);
            if (!isSilent) {
                // Fake a quit message if it isn't silent
                emitQuitMessage(player);
            } else {
                // If it is silent, notify players with vanish.statusupdates since VNP won't
                for (Player p : Bukkit.getOnlinePlayers()) {
                    if (!p.hasPermission("vanish.statusupdates")) {
                        continue;
                    }

                    // Send a special message to the player that is changing status
                    if (p.equals(player)) {
                        p.sendMessage(Message.format("commands.vanish.self", "vanished. Poof"));
                    } else {
                        p.sendMessage(Message.format("commands.vanish.others", player.getName(), "vanished. Poof"));
                    }
                }
            }

            // Eject spectating players to keep up the illusion
            ejectSpectatingPlayers(player, isSilent);
        } else {
            vnp.getManager().reveal(player, isSilent, !isSilent);
            // Do this for the legacy behavior
            getProfile(player).setHasUnvanished(true);
            if (!isSilent) {
                // Fake a join message
                emitJoinMessage(player);
            } else {
                // If it is silent, notify players with vanish.see since VNP won't
                for (Player p : Bukkit.getOnlinePlayers()) {
                    if (!p.hasPermission("vanish.statusupdates")) {
                        continue;
                    }

                    // Send a special message to the player that is changing status
                    if (p.equals(player)) {
                        p.sendMessage(Message.format("commands.vanish.self", "become visible"));
                    } else {
                        p.sendMessage(Message.format("commands.vanish.others", player.getName(), "become visible"));
                    }
                }
            }
        }
    }

    /**
     * Sends the new player join message to all players that have the connection_messages setting toggled.
     *
     * @param player The player to create a join message for.
     */
    public static void emitFirstJoinMessage(Player player) {
        for (Player p : Bukkit.getOnlinePlayers()) {
            MinecraftProfile mcp = getProfile(p);
            // If connection_messages is set to true then give the player the message
            if (CoreToggleSetting.CONNECTION_MESSAGES.getValue(mcp)) {
                p.sendMessage(Message.format("connection.first_join", player.getName()));
            }
        }
    }

    /**
     * Sends the join message to all players that have the connection_messages setting toggled.
     *
     * @param player The player to create a join message for.
     */
    public static void emitJoinMessage(Player player) {
        for (Player p : Bukkit.getOnlinePlayers()) {
            MinecraftProfile mcp = getProfile(p);
            // If connection_messages is set to true then give the player the message
            if (CoreToggleSetting.CONNECTION_MESSAGES.getValue(mcp)) {
                p.sendMessage(Message.format("connection.join", player.getName()));
            }
        }
        // Send the message to console too
        Bukkit.getConsoleSender().sendMessage(Message.format("connection.join", player.getName()));
    }

    /**
     * Sends the quit message to all players that have the connection_messages setting toggled.
     *
     * @param player The player to create a quit message for.
     */
    public static void emitQuitMessage(Player player) {
        for (Player p : Bukkit.getOnlinePlayers()) {
            MinecraftProfile mcp = getProfile(p);
            // If connection_messages is set to true then give the player the message
            if (CoreToggleSetting.CONNECTION_MESSAGES.getValue(mcp)) {
                p.sendMessage(Message.format("connection.quit", player.getName()));
            }
        }
        // Send the message to console too
        Bukkit.getConsoleSender().sendMessage(Message.format("connection.quit", player.getName()));
    }

    /**
     * Checks whether <code>player</code> can see <code>toCheck</code>.
     *
     * @param sender The sender to test the permissions of.
     * @param toCheck The UUID of the player to check the vanished status of.
     * @return True if <code>toCheck</code> is online and <code>player</code> has permissions to see them.
     */
    public static boolean canSee(CommandSender sender, UUID toCheck) {
        return isOnline(toCheck) && (sender.hasPermission("vanish.see") || !isVanished(Bukkit.getPlayer(toCheck)));
    }

    public static UUID getUUID(String name, boolean includeOffline, boolean includeVanished) {
        return getUUID(name, includeOffline, includeVanished, false, false, 0);
    }

    public static UUID getUUID(String name, boolean includeOffline, boolean includeVanished, boolean includeServer) {
        return getUUID(name, includeOffline, includeVanished, includeServer, false, 0);
    }

    public static UUID getUUID(String name, boolean includeOffline, boolean includeVanished, boolean includeServer, boolean includePreviousNames) {
        return getUUID(name, includeOffline, includeVanished, includeServer, includePreviousNames, 0);
    }

    /**
     * Gets the UUID of a player, given their full name, partial name, or UUID. Priority is given as follows:
     * 1. UUID of a player that meets the provided conditions
     * 2. Case-insensitive name of an online player
     * 3. Case-insensitive name of an offline player (if includeOffline = true)
     * 4. Case-insensitive name of a previous name of an online player (if includePreviousNames = true)
     * 5. Case-insensitive name of a previous name of an offline player (if includePreviousNames = true)
     * 6. Start of the name of a vanished player (if includeVanished = true)
     * 7. Start of the name of an online player
     * 8. Part of a word in the name of a vanished player (if includeVanished = true)
     * 9. Part of a word in the name of an online player
     * 10. Crude embedded search in the name of an online player
     * 11. Server
     *
     * For partial matching, matches that are shorter than the minimum specified in the config will not be
     * considered. Additionally, common words in the blacklist in this plugin's config will be not be considered.
     *
     * @param name The name or partial name of the player to get the UUID of.
     * @param includeOffline True if offline players should be included.
     * @param includeVanished True if vanished players should be included in partial name matching.
     * @param includeServer True if the server's UUID can be returned (00000000-0000-0000-0000-000000000000)
     * @param includePreviousNames True if previous names should be included for exact matching.
     * @param minimum The minimum characters the provided name has to be to return a partial match.
     * @return The UUID of the requested player, or null if no matching players were found.
     */
    public static UUID getUUID(String name, boolean includeOffline, boolean includeVanished, boolean includeServer, boolean includePreviousNames, int minimum) {
        UUID matchedUUID;

        if (!StringUtil.isWord(name)) {
            return null;
        }

        // Attempt to parse an exact UUID; note that the string-encoded UUID must include dashes to be parsed by Java
        try {
            matchedUUID = UUID.fromString(name);

            // Check if the matched UUID matches the provided conditions
            // If the matched player is online, then return their UUID if they are able to be seen
            Player matchedPlayer = Bukkit.getPlayer(matchedUUID);
            if (matchedPlayer != null && (!isVanished(matchedPlayer) || includeVanished || includeOffline)) {
                return matchedUUID;
            }

            // If the UUID matches an offline player then make sure they have played before and offline players are allowed
            OfflinePlayer offlineMatchedPlayer = Bukkit.getOfflinePlayer(matchedUUID);
            if (includeOffline && offlineMatchedPlayer != null && offlineMatchedPlayer.hasPlayedBefore()) {
                return matchedUUID;
            }

            // If the UUID matches the server then make sure the server is allowed
            if (includeServer && SynergyCore.SERVER_ID.equals(matchedUUID)) {
                return matchedUUID;
            }

            // Otherwise the UUID, while parseable, is not valid so return null (since it can't possibly be a name)
            return null;
        } catch (IllegalArgumentException e) {}

        // Check for a case-insensitive match of an online player and ignore this match if vanished players should be ignored
        Player matchedPlayer = Bukkit.getPlayerExact(name);
        if (matchedPlayer != null && (!isVanished(matchedPlayer) || includeVanished || includeOffline)) {
            return matchedPlayer.getUniqueId();
        }

        // Check for a case-insensitive match of an offline player that has played before
        if (includeOffline) {
            OfflinePlayer offlineMatchedPlayer = Bukkit.getOfflinePlayer(name);

            if (offlineMatchedPlayer != null && offlineMatchedPlayer.hasPlayedBefore()) {
                return offlineMatchedPlayer.getUniqueId();
            }
        }

        if (includePreviousNames) {
            // Check for a case-insensitive match of the previous names of an online player and ignore this match if vanished players should be ignored
            List<MinecraftProfile> matchedMCPs = Bukkit.getOnlinePlayers().stream()
                    .filter(player -> (!isVanished(player) || includeVanished || includeOffline))
                    .map(PlayerUtil::getProfile)
                    .filter(mcp -> ListUtil.containsIgnoreCase(mcp.getKnownNames(), name))
                    .collect(Collectors.toList());
            if (matchedMCPs.size() > 0) {
                // Unfortunately not much priority determining can be done so just return the first result in the list
                return matchedMCPs.get(0).getID();
            }

            // Check for a case-insensitive match of the previous names of an offline player
            if (includeOffline) {
                Datastore ds = MongoDB.getInstance().getDatastore();

                Optional<MinecraftProfile> matchedMCP = ds.find(MinecraftProfile.class)
                        .filter(Filters.elemMatch(
                                "nl",
                                Filters.regex("nl").pattern(Pattern.quote(name)).caseInsensitive()))
                        .stream(new FindOptions().projection().include("li", "lo"))
                        .filter(mcp -> !mcp.isOnline()).findFirst();

                if (matchedMCP.isPresent()) {
                    return matchedMCP.get().getID();
                }
            }
        }

        // If the name provided is shorter than the minimum, return null
        if (name.length() < minimum) {
            return null;
        }

        // Check for a match at the beginning of of online players' names
        matchedUUID = matchNameBeginning(name, includeVanished);
        if (matchedUUID != null) {
            return matchedUUID;
        }

        // Check for a match within words of online players' names
        matchedUUID = matchNameWithin(name, includeVanished);
        if (matchedUUID != null) {
            return matchedUUID;
        }

        // Do a crude search for the partial name embedded anywhere in the name of a player and ignore case
        String partialNameLower = name.toLowerCase();
        for (Player player : Bukkit.getOnlinePlayers()) {
            // Skip this player if vanished players should be ignored
            if (isVanished(player) && !includeVanished) {
                continue;
            }

            // Ignore case
            String lowerName = player.getName().toLowerCase();
            if (lowerName.contains(partialNameLower)) {
                return player.getUniqueId();
            }
        }

        // If no players match the input of "console" or "server" and if this can return the console's UUID, then do it
        if (includeServer && (name.equalsIgnoreCase("console") || name.equalsIgnoreCase("server"))) {
            return SynergyCore.SERVER_ID;
        }

        return null;
    }

    /**
     * Matches the string given to the beginnings of online players' names, and returns the UUID of the best
     * matching player, if found. Case is not considered when calculating delta. Prioritizes vanished players.
     *
     * @param partialName The partial name to match.
     * @param includeVanished True if vanished players should be included in partial name matching.
     * @return The UUID of the closest matched player, or null if no matching players were found.
     */
    public static UUID matchNameBeginning(String partialName, boolean includeVanished) {
        // Ignore case
        String partialNameLower = partialName.toLowerCase();
        UUID match = null;
        int delta = Integer.MAX_VALUE;
        boolean matchedIsVanished = false;

        for (Player player : Bukkit.getOnlinePlayers()) {
            boolean currentIsVanished = isVanished(player);

            // Skip this player if vanished players should be ignored
            if (currentIsVanished && !includeVanished) {
                continue;
            }

            // Ignore case
            String name = player.getName().toLowerCase();
            if (!name.startsWith(partialNameLower)) {
                continue;
            }

            int currentDelta = name.length() - partialNameLower.length();

            // Prioritize vanished players
            if (matchedIsVanished && !currentIsVanished) {
                continue;
            }

            // If the delta is 0, return to speed up the process
            if (currentDelta == 0) {
                return player.getUniqueId();
            }

            // Change the matched player information if a better match is found
            if (currentDelta < delta) {
                delta = currentDelta;
                match = player.getUniqueId();

                if (currentIsVanished) {
                    matchedIsVanished = true;
                }
            }
        }

        return match;
    }

    /**
     * Matches the string given within online players' names, and returns the UUID of the best matching
     * player, if found. Case is not considered when calculating delta. Prioritizes vanished players.
     *
     * @param partialName The partial name to match.
     * @param includeVanished True if vanished players should be included in partial name matching.
     * @return The UUID of the closest matched player, or null if no matching players were found.
     */
    public static UUID matchNameWithin(String partialName, boolean includeVanished) {
        // Ignore case
        String partialNameLower = partialName.toLowerCase();
        UUID match = null;
        int delta = Integer.MAX_VALUE;
        boolean matchedIsVanished = false;

        for (Player player : Bukkit.getOnlinePlayers()) {
            boolean currentIsVanished = isVanished(player);

            // Skip this player if vanished players should be ignored
            if (currentIsVanished && !includeVanished) {
                continue;
            }

            if (!player.getName().toLowerCase().contains(partialNameLower)) {
                continue;
            }

            List<String> splitWords = StringUtil.splitWordsIntoWords(player.getName());
            int currentDelta = Integer.MAX_VALUE;

            for (String word : splitWords) {
                int wordDelta = word.toLowerCase().indexOf(partialName);
                if (wordDelta != -1 && wordDelta < currentDelta) {
                    currentDelta = wordDelta;
                }
            }

            // Prioritize vanished players
            if (matchedIsVanished && !currentIsVanished) {
                continue;
            }

            // If the delta is 0, return to speed up the process
            if (currentDelta == 0) {
                return player.getUniqueId();
            }

            if (currentDelta < delta) {
                delta = currentDelta;
                match = player.getUniqueId();

                if (currentIsVanished) {
                    matchedIsVanished = true;
                }
            }
        }

        return match;
    }

    /**
     * Checks if the given string returns a match for the given player's name.
     *
     * @param mcp The <code>MinecraftProfile</code> of the player to match.
     * @param str The suspected string that matches the name of the player.
     * @return True if the given name matches the player.
     */
    public static boolean isMatch(MinecraftProfile mcp, String str, int minimum) {
        String strLower = str.toLowerCase();

        // Get the method of name mentioning that the player has as a setting
        String value = CoreMultiOptionSetting.NAME_MENTIONING.getValue(mcp);

        // If the string provided is shorter than the minimum, return false if the name_mentioning setting isn't whitelist
        if (str.length() < minimum && !value.equals("whitelist")) {
            return false;
        }

        switch (value) {
            case "automatic":
                break;
            case "automatic_with_blacklist":
                // Return false if the string is in the blacklist
                Set<String> blacklist = CoreSetSetting.NAME_MENTIONING_BLACKLIST.getValue(mcp);
                if (blacklist.contains(strLower)) {
                    return false;
                }
                break;
            case "whitelist":
                // Return true if the string is in the whitelist
                Set<String> whitelist = CoreSetSetting.NAME_MENTIONING_WHITELIST.getValue(mcp);
                return whitelist.contains(strLower);
            default: break;
        }

        // Use the automatic method of detecting names

        // Check if the given player's name contains the suspected string
        return mcp.getCurrentName().toLowerCase().contains(strLower);
    }

    /**
     * Convenience method for getting a player's cached <code>MinecraftProfile</code>.
     *
     * @param player The player to get the profile of.
     * @return The player's <code>MinecraftProfile</code>, if found.
     */
    public static MinecraftProfile getProfile(Player player) {
        return getProfile(player.getUniqueId());
    }

    /**
     * Convenience method for getting a player's cached <code>MinecraftProfile</code>.
     *
     * @param pID The UUID of the player to get the profile of.
     * @return The player's <code>MinecraftProfile</code>, if found.
     */
    public static MinecraftProfile getProfile(UUID pID) {
        return ProfileManager.getInstance().getCachedMinecraftProfile(pID);
    }

    /**
     * Convenience method for checking if a player is online.
     *
     * @param pID The UUID of the player to check.
     * @return True if the player is currently online.
     */
    public static boolean isOnline(UUID pID) {
        if (pID.equals(SynergyCore.SERVER_ID)) {
            return true;
        }

        MinecraftProfile mcp = getProfile(pID);
        return mcp != null && mcp.isOnline();
    }

    /**
     * Gets the name of a player by their ID.
     *
     * @param pID The UUID of the player to get the name of.
     * @return The name of the player.
     */
    public static String getName(UUID pID) {
        if (pID.equals(SynergyCore.SERVER_ID)) {
            return Message.get("server.name");
        }

        OfflinePlayer player = Bukkit.getOfflinePlayer(pID);
        return player.getName();
    }

    /**
     * Gets the names of players by their IDs.
     *
     * @param pIDs The UUIDs of the players to get the names of.
     * @return The names of the players.
     */
    public static List<String> getNames(Collection<UUID> pIDs) {
        List<String> names = new ArrayList<>();
        for (UUID pID : pIDs) {
            names.add(getName(pID));
        }

        return names;
    }

    /**
     * Checks if the given player has the permission given as a global permission.
     * Used to check permissions of a player that is offline.
     *
     * @param pID The UUID of the player to check.
     * @param permission The permission to check.
     * @return True if the player has the global permission.
     */
    public static boolean hasGlobalPermission(UUID pID, String permission) {
        return PermissionsEx.getUser(pID.toString()).has(permission);
    }

    /**
     * Updates the scoreboard teams to use with the tablist.
     *
     * @param player The player to update the team of.
     */
    public static void updateTabList(Player player) {
        if (player == null) {
            return;
        }

        Scoreboard scoreboard = Bukkit.getScoreboardManager().getMainScoreboard();
        String scoreboardName = getPrimaryRank(player.getUniqueId()).getScoreBoardName();

        if (scoreboardName == null) {
            return;
        }

        scoreboard.getTeam(scoreboardName).addEntry(player.getName());
    }

    /**
     * Updates a player's display name in chat and tab.
     *
     * @param player The player to update the name of.
     */
    public static void updateDisplayName(Player player) {
        // Update the player's prefix if they're a donator
        getProfile(player).updateDonatorStatus();
        // Update their name for chat
        player.setDisplayName(Message.translateCodes(PermissionsEx.getUser(player).getPrefix()) + player.getName());

        // Add the player to the team corresponding to their primary rank in the tablist
        updateTabList(player);
        // Make the player's tab name be hexified
        player.setPlayerListName(getNameColor(player) + player.getName());
    }

    public static String getNameColor(Player player) {
        return getNameColor(player.getUniqueId());
    }

    /**
     * Gets the color of the given player's name after their rank prefix is added.
     *
     * @param pID The UUID of the player to get the color of.
     * @return The string containing the color codes of the player's name.
     */
    public static String getNameColor(UUID pID) {
        String prefix = Message.translateCodes(PermissionsEx.getUser(pID.toString()).getPrefix());
        return ChatColor.getLastColors(prefix);
    }

    /**
     * Ejects all spectating players from the given player.
     *
     * @param spectatedPlayer The player to eject specators from.
     * @param isSilent True if ejected spectators shouldn't receive feedback.
     * @return The number of spectators ejected.
     */
    public static int ejectSpectatingPlayers(Player spectatedPlayer, boolean isSilent) {
        int counter = 0;

        for (Player p : Bukkit.getOnlinePlayers()) {
            if (p.getSpectatorTarget() == null) {
                continue;
            }
            if (!p.getSpectatorTarget().equals(spectatedPlayer)) {
                continue;
            }
            if (!PlayerUtil.canSee(spectatedPlayer, p.getUniqueId())) {
                continue;
            }

            // Don't eject the player if they have override perms
            if (p.hasPermission("syn.setting.allow-spectators.override")) {
                continue;
            }

            // Eject the spectating player
            p.setSpectatorTarget(null);

            // Give feedback depending on whether or not the operation is silent
            if (!isSilent) {
                p.sendMessage(Message.format("events.spectate.info.ejected", spectatedPlayer.getName()));
            }

            // Increment the counter
            counter++;
        }

        return counter;
    }

    /**
     * Finds the nearest online player to the given location.
     *
     * @param location The location to find the closest player to.
     * @return The nearest player to the given location.
     */
    public static Player getNearestPlayer(Location location) {
        Player nearestPlayer = null;
        double nearestPlayerDistance = Double.MAX_VALUE;

        for (Player p : Bukkit.getOnlinePlayers()) {
            // Ignore the player if they're not in the same world
            if (!p.getWorld().getName().equals(location.getWorld().getName())) {
                continue;
            }

            double distance = p.getLocation().distanceSquared(location);

            if (distance < nearestPlayerDistance) {
                nearestPlayer = p;
                nearestPlayerDistance = distance;
            }
        }

        return nearestPlayer;
    }

    /**
     * Gets the primary rank for the player with the given UUID.
     *
     * @param pID The UUID to check.
     * @return The primary <code>Rank</code> of the player with the specified UUID.
     */
    public static Rank getPrimaryRank(UUID pID) {
        PermissionUser user = PermissionsEx.getUser(pID.toString());

        for (PermissionGroup rank : user.getParents()) {
            Rank synRank = Rank.fromPermissionGroup(rank);
            if (synRank != null && synRank.isPrimaryRank()) {
                return synRank;
            }
        }

        // If no group was found then assume they're a Guest
        return Rank.GUEST;
    }

    /**
     * Gets the donator <code>Rank</code> of the player with the given UUID.
     *
     * @param pID The UUID to check.
     * @return The donator <code>Rank</code> if found, or null.
     */
    public static Rank getDonatorRank(UUID pID) {
        PermissionUser user = PermissionsEx.getUser(pID.toString());

        for (PermissionGroup rank : user.getParents()) {
            Rank synRank = Rank.fromPermissionGroup(rank);
            if (synRank != null && synRank.isDonator()) {
                return synRank;
            }
        }

        return null;
    }

    /**
     * Gets the rank that is visible in the prefix of the player with the given UUID.
     *
     * @param pID The UUID to check.
     * @return The prefix <code>Rank</code> of the player with the specified UUID.
     */
    public static Rank getPrefixRank(UUID pID) {
        // If they have a donator rank use that, otherwise use their primary rank
        return Optional.ofNullable(getDonatorRank(pID)).orElseGet(() -> getPrimaryRank(pID));
    }

    /**
     * Gets the rank that is used to sort players in rank lists.
     *
     * @param pID The UUID to check.
     * @return The highest <code>Rank</code> of the player with the specified UUID.
     */
    public static Rank getRankListRank(UUID pID) {
        Rank donatorRank = getDonatorRank(pID);
        Rank primaryRank = getPrimaryRank(pID);

        return donatorRank == null ? primaryRank : ObjectUtil.max(donatorRank, primaryRank);
    }

    public static void killPlayer(Player player) {
        killPlayer(player, null);
    }

    public static void killPlayer(Player player, String deathMessageID) {
        killPlayer(player, deathMessageID, null);
    }

    /**
     * Kills the given player and broadcasts the given death message, respecting players' settings.
     *
     * @param player The player to kill.
     * @param deathMessageID The ID of the death message to broadcast. Use null to use Minecrafts's default unknown death message.
     * @param murderer The entity that killed the player.
     */
    public static void killPlayer(Player player, String deathMessageID, Entity murderer) {
        // If no death message id was provided use the default one
        if (deathMessageID == null) {
            deathMessageID = "death_message.default";
        }

        // Create the interactable player name
        EntityHuman nmsPlayer = ((CraftPlayer) player).getHandle();
        IChatBaseComponent nmsPlayerName = nmsPlayer.C_(); // getScoreboardDisplayName
        BaseComponent playerName = ComponentSerializer.parse(IChatBaseComponent.ChatSerializer.a(nmsPlayerName))[0];

        BaseComponent[] deathMessage;
        if (murderer == null) {
            // Create the death message
            deathMessage = Message.formatTextComponent(deathMessageID, playerName);
        } else {
            // Create the interactable murderer name
            net.minecraft.world.entity.Entity nmsMurderer = ((CraftEntity) murderer).getHandle();
            IChatBaseComponent nmsMurdererName = nmsMurderer.C_(); // getScoreboardDisplayName
            BaseComponent murdererName = ComponentSerializer.parse(IChatBaseComponent.ChatSerializer.a(nmsMurdererName))[0];

            // Create the death message
            deathMessage = Message.formatTextComponent(deathMessageID, playerName, murdererName);
        }

        // Kill the player and replace the death message with the custom one
        EntityDamageEvent event = new EntityDamageEvent(player, EntityDamageEvent.DamageCause.CUSTOM, player.getHealth());
        player.setLastDamageCause(event);
        player.setHealth(0);

        Chat.getInstance().broadcastDeathMessage(player, deathMessage);
    }

    /**
     * Checks if the given player can use <code>CoreToggleSetting.FULL_TELEPORTATION</code>
     * when teleporting to the given world.
     *
     * @param player The player that is teleporting.
     * @param toWorld The world that the player is teleporting to.
     * @return True if the player can use <code>CoreToggleSetting.FULL_TELEPORTATION</code>.
     */
    public static boolean canUseFullTeleportation(Player player, World toWorld) {
        // This method may be called before the player's profile is loaded
        if (PlayerUtil.getProfile(player) == null) {
            return false;
        }

        // Check the settings and permissions of the target world
        WorldGroupProfile toWGP = PlayerUtil.getProfile(player).getPartialWorldGroupProfile(SynergyCore.getWorldGroupName(toWorld.getName()), "s");

        // If they're teleporting to a new world group then exit now to avoid a NPE
        if (toWGP == null) {
            return false;
        }

        // Only load the settings preferences if it hasn't been loaded already
        if (!toWGP.equals(PlayerUtil.getProfile(player).getCurrentWorldGroupProfile())) {
            toWGP.loadSettingPreferences();
        }

        SettingDiffs newSettings = toWGP.getSettingPreferences().getCurrentSettings();
        boolean hasPermission = PermissionsEx.getUser(player).has("syn.setting.full-teleportation", toWorld.getName());

        return (boolean) newSettings.getValueOrDefault(CoreToggleSetting.FULL_TELEPORTATION, hasPermission);
    }
}
