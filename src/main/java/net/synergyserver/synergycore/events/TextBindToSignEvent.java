package net.synergyserver.synergycore.events;

import org.bukkit.block.Sign;
import org.bukkit.entity.Player;
import org.bukkit.event.Cancellable;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;

/**
 * This event is called when a player is binding text to a sign to later be accessed by right-clicking it.
 */
public class TextBindToSignEvent extends Event implements Cancellable {

    private Player player;
    private String[] text;
    private Sign sign;
    private boolean cancelled;
    private static final HandlerList handlers = new HandlerList();

    /**
     * Creates a new <code>TextBindToSignEvent</code> with the given parameters.
     *
     * @param player The player that is trying to bind the text to the sign.
     * @param text The text that is being bound to the sign.
     * @param sign The sign that the text is being bound to.
     */
    public TextBindToSignEvent(Player player, String[] text, Sign sign) {
        this.player = player;
        this.text = text;
        this.sign = sign;
        this.cancelled = false;
    }

    /**
     * Gets the player associated with this event.
     *
     * @return The player that is trying to bind the text to the sign.
     */
    public Player getPlayer() {
        return player;
    }

    /**
     * Gets the text associated with this event.
     *
     * @return The text that is being bound to the sign.
     */
    public String[] getText() {
        return text;
    }

    /**
     * Set the text that is going to be bound to the sign.
     *
     * @param text The new text.
     */
    public void setText(String[] text) {
        this.text = text;
    }

    /**
     * Gets the sign associated with this event.
     *
     * @return The sign that the text is being bound to.
     */
    public Sign getSign() {
        return sign;
    }

    public boolean isCancelled() {
        return cancelled;
    }

    public void setCancelled(boolean cancelled) {
        this.cancelled = cancelled;
    }

    public HandlerList getHandlers() {
        return handlers;
    }

    public static HandlerList getHandlerList() {
        return handlers;
    }

}
