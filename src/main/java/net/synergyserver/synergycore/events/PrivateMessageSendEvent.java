package net.synergyserver.synergycore.events;

import net.synergyserver.synergycore.PrivateMessage;
import org.bukkit.event.Cancellable;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;

/**
 * This event is called whenever a private message is sent with /msg.
 */
public class PrivateMessageSendEvent extends Event implements Cancellable {

    private PrivateMessage privateMessage;
    private boolean cancelled;
    private static final HandlerList handlers = new HandlerList();

    /**
     * Creates a new <code>PrivateMessageSendEvent</code> with the given <code>PrivateMessage</code>.
     *
     * @param privateMessage The <code>PrivateMessage</code> of this event.
     */
    public PrivateMessageSendEvent(PrivateMessage privateMessage) {
        this.privateMessage = privateMessage;
        this.cancelled = false;
    }

    /**
     * Gets the <code>PrivateMessage</code> of this event.
     *
     * @return This event's <code>PrivateMessage</code>.
     */
    public PrivateMessage getPrivateMessage() {
        return privateMessage;
    }

    public boolean isCancelled() {
        return cancelled;
    }

    public void setCancelled(boolean cancelled) {
        this.cancelled = cancelled;
    }

    public HandlerList getHandlers() {
        return handlers;
    }

    public static HandlerList getHandlerList() {
        return handlers;
    }
}
