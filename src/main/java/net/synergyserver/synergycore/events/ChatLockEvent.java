package net.synergyserver.synergycore.events;

import org.bukkit.event.HandlerList;

import java.util.UUID;

/**
 * This event is called whenever someone locks/unlocks chat.
 */
public class ChatLockEvent extends ChatEvent {

    private UUID locker;
    private boolean newLockState;
    private static final HandlerList handlers = new HandlerList();

    /**
     * Creates a new <code>ChatLockEvent</code> with the given parameters.
     *
     * @param locker The UUID of the person who locked/unlocked chat.
     * @param newLockState The new lock state that the player set.
     */
    public ChatLockEvent(UUID locker, boolean newLockState) {
        super();
        this.locker = locker;
        this.newLockState = newLockState;
    }

    /**
     * Gets the UUID of the player who initiated this event.
     *
     * @return The UUID of the player who initiated this event.
     */
    public UUID getLocker() {
        return locker;
    }

    /**
     * Gets the new lock state that the player set chat to.
     *
     * @return True if the player locked chat.
     */
    public boolean isNewLockState() {
        return newLockState;
    }

    public HandlerList getHandlers() {
        return handlers;
    }

    public static HandlerList getHandlerList() {
        return handlers;
    }

}
