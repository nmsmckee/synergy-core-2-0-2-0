package net.synergyserver.synergycore.events;

import net.synergyserver.synergycore.Mail;
import org.bukkit.event.Cancellable;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;

/**
 * This event is called whenever someone sends mail to another user.
 */
public class MailSendEvent extends Event implements Cancellable {

    private Mail mail;
    private boolean cancelled;
    private static final HandlerList handlers = new HandlerList();

    /**
     * Creates a new <code>MailSendEvent</code> with the given <code>PrivateMessage</code>.
     *
     * @param mail The mail sent for this event.
     */
    public MailSendEvent(Mail mail) {
        this.mail = mail;
        this.cancelled = false;
    }

    /**
     * Gets the <code>Mail</code> of this event.
     *
     * @return The mail of this event.
     */
    public Mail getMail() {
        return mail;
    }

    public boolean isCancelled() {
        return cancelled;
    }

    public void setCancelled(boolean cancelled) {
        this.cancelled = cancelled;
    }

    public HandlerList getHandlers() {
        return handlers;
    }

    public static HandlerList getHandlerList() {
        return handlers;
    }
}
