package net.synergyserver.synergycore.events;

import net.synergyserver.synergycore.ServiceType;
import net.synergyserver.synergycore.profiles.ConnectedProfile;
import net.synergyserver.synergycore.profiles.SynUser;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;

/**
 * This event is called whenever a service is connected to a <code>SynUser</code>.
 */
public class ServiceConnectEvent extends Event {

    private SynUser target;
    private ServiceType service;
    private ConnectedProfile serviceProfile;
    private static final HandlerList handlers = new HandlerList();

    /**
     * Creates a new <code>ServiceConnectEvent</code> with the given parameters.
     *
     * @param target The <code>SynUser</code> that the account on the service is being connected to.
     * @param service The <code>ServiceType</code> of the service involved.
     * @param serviceProfile The <code>ConnectedProfile</code> representing the player's account on the service.
     */
    public ServiceConnectEvent(SynUser target, ServiceType service, ConnectedProfile serviceProfile) {
        super(true);
        this.target = target;
        this.service = service;
        this.serviceProfile = serviceProfile;
    }

    /**
     * Gets the <code>SynUser</code> that the account on the service is being connected to.
     *
     * @return The <code>SynUser</code> of the player.
     */
    public SynUser getTarget() {
        return target;
    }

    /**
     * Gets the <code>ServiceType</code> of the service.
     *
     * @return The <code>ServiceType</code> of the service.
     */
    public ServiceType getService() {
        return service;
    }

    /**
     * Gets the <code>ConnectedProfile</code> representing the player's account on the service.
     *
     * @return The <code>ConnectedProfile</code> representing the player's account on the service.
     */
    public ConnectedProfile getServiceProfile() {
        return serviceProfile;
    }

    public HandlerList getHandlers() {
        return handlers;
    }

    public static HandlerList getHandlerList() {
        return handlers;
    }
}
