package net.synergyserver.synergycore.listeners;

import me.confuser.banmanager.common.api.BmAPI;
import net.dv8tion.jda.api.entities.Guild;
import net.dv8tion.jda.api.entities.Member;
import net.synergyserver.synergycore.MemberApplication;
import net.synergyserver.synergycore.Rank;
import net.synergyserver.synergycore.SerializableLocation;
import net.synergyserver.synergycore.StaffDashboard;
import net.synergyserver.synergycore.SynergyCore;
import net.synergyserver.synergycore.WeatherType;
import net.synergyserver.synergycore.configs.Message;
import net.synergyserver.synergycore.configs.TextConfig;
import net.synergyserver.synergycore.database.DataManager;
import net.synergyserver.synergycore.events.WorldGroupProfileCreateEvent;
import net.synergyserver.synergycore.profiles.DiscordProfile;
import net.synergyserver.synergycore.profiles.MinecraftProfile;
import net.synergyserver.synergycore.profiles.ProfileManager;
import net.synergyserver.synergycore.profiles.SynUser;
import net.synergyserver.synergycore.profiles.WorldGroupProfile;
import net.synergyserver.synergycore.settings.CoreMultiOptionSetting;
import net.synergyserver.synergycore.settings.CoreToggleSetting;
import net.synergyserver.synergycore.settings.SettingPreferences;
import net.synergyserver.synergycore.utils.ListUtil;
import net.synergyserver.synergycore.utils.PlayerUtil;
import net.synergyserver.synergycore.utils.StringUtil;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerPreLoginEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.event.server.ServerListPingEvent;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

/**
 * Listens to player joins and leaves.
 */
public class PlayerConnectionListener implements Listener {

    @EventHandler(priority = EventPriority.HIGHEST, ignoreCancelled = true)
    public void onServerPing(ServerListPingEvent event) {
        // Use the motd from messages.yml
        event.setMotd(Message.format("events.server_ping.motd"));
        // Use the cached server icon
        event.setServerIcon(SynergyCore.getPlugin().getIcon());
    }

    // TODO: REPLACE WITH NONGHETTO CODE
    @EventHandler(priority = EventPriority.LOWEST, ignoreCancelled = true)
    public void onPlayerJoinMemberWhitelist(AsyncPlayerPreLoginEvent event) {
        // IGnore if the member whitelist isn't active
        if (!StaffDashboard.getInstance().memberWhitelist) {
            return;
        }

        if (PlayerUtil.getPrimaryRank(event.getUniqueId()) == Rank.GUEST) {
            event.disallow(AsyncPlayerPreLoginEvent.Result.KICK_WHITELIST, "The server is currently in Member-only mode. We apologize for the inconvenience.");
        }
    }

    @EventHandler(priority = EventPriority.MONITOR, ignoreCancelled = true)
    public void onPlayerJoinPreprocess(AsyncPlayerPreLoginEvent event) {
        // Update information for their MinecraftProfile, if it was created already
        DataManager dm = DataManager.getInstance();
        UUID pID = event.getUniqueId();
        MinecraftProfile mcp = dm.getDataEntity(MinecraftProfile.class, pID);

        if (!dm.isDataEntityInDatabase(MinecraftProfile.class, pID)) {
            return;
        }

        String name = event.getName();
        SynUser synUser = dm.getDataEntity(SynUser.class, mcp.getSynID());
        DiscordProfile discordProfile = synUser.getDiscordProfile();

        // Update name records
        if (!mcp.getCurrentName().equals(name)) {
            mcp.setCurrentName(name);
            mcp.addKnownName(name);

            if (discordProfile != null) {
                try {
                    Guild server = SynergyCore.getDiscordServer();
                    Member discordUser = server.getMemberById(Long.parseLong(discordProfile.getID()));
                    server.modifyNickname(discordUser, name).queue();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }

        if (Bukkit.getPluginManager().isPluginEnabled("BanManager")) {
            // Update ban status
            if (BmAPI.isBanned(pID)) {
                mcp.setIsBanned(true);
            } else {
                mcp.setIsBanned(false);
            }
        }

        // Update IP records
        mcp.addRecentIP(event.getAddress().getHostAddress());
    }

    @EventHandler(priority = EventPriority.MONITOR, ignoreCancelled = true)
    public void onPlayerJoin(PlayerJoinEvent event) {
        ProfileManager pm = ProfileManager.getInstance();
        DataManager dm = DataManager.getInstance();

        Player player = event.getPlayer();
        final UUID pID = player.getUniqueId();
        String name = player.getName();
        long currentTime = System.currentTimeMillis();

        // Create a profile if it's not found in the database
        if (!dm.isDataEntityInDatabase(MinecraftProfile.class, pID)) {
            SynUser synUser = new SynUser();
            synUser.setMinecraftID(pID);
            dm.saveDataEntity(synUser);

            MinecraftProfile mcp = new MinecraftProfile(
                    synUser.getID(),
                    pID,
                    name,
                    currentTime,
                    player.getLastPlayed(),
                    ListUtil.makeList(player.getAddress().getAddress().getHostAddress())
            );
            dm.saveDataEntity(mcp);
        }

        // Update details on login
        MinecraftProfile mcp = dm.getDataEntity(MinecraftProfile.class, pID);

        // Update world group profile data
        String worldGroupName = mcp.getCurrentWorldGroupName();
        // Create world profiles if they are not found for the current world groups the player is in
        if (!mcp.hasWorldGroupProfile(worldGroupName)) {
            // Create and save a new SettingPreferences in the database
            SettingPreferences sp = new SettingPreferences();
            dm.saveDataEntity(sp);

            // Create a new WorldGroupProfile
            WorldGroupProfile wgp = new WorldGroupProfile(
                    worldGroupName,
                    pID,
                    currentTime,
                    currentTime,
                    new SerializableLocation(player.getLocation()),
                    sp.getID()
            );
            wgp.setLastAction(currentTime);

            // Emit an event for other SynergyPlugins to hook into
            WorldGroupProfileCreateEvent wgpEvent = new WorldGroupProfileCreateEvent(wgp);
            Bukkit.getPluginManager().callEvent(wgpEvent);

            WorldGroupProfile newWGP = wgpEvent.getNewWGP();
            newWGP.setLastAction(currentTime);
            mcp.addWorldGroupProfile(newWGP);
            dm.saveDataEntity(newWGP);
        } else {
            WorldGroupProfile wgp = mcp.getCurrentWorldGroupProfile();
            wgp.setLastLogIn(currentTime);
            wgp.setLastAction(currentTime);
        }

        // Update last login time
        mcp.setLastLogIn(currentTime);

        // Cache this player's MinecraftProfile
        pm.cacheMinecraftProfile(mcp);
        // Cache this player's current WorldGroupProfile
        mcp.setCurrentWorldGroupProfile(mcp.getCurrentWorldGroupProfile());

        // Set the player's display name
        PlayerUtil.updateDisplayName(player);

        // Set the player's personal time and weather
        WorldGroupProfile wgp = mcp.getCurrentWorldGroupProfile();
        Long personalTime = wgp.getPersonalTime();
        if (personalTime == null) {
            player.resetPlayerTime();
        } else {
            player.setPlayerTime(personalTime, false);
        }
        WeatherType personalWeather = wgp.getPersonalWeather();
        if (personalWeather == null) {
            player.resetPlayerWeather();
        } else {
            player.setPlayerWeather(personalWeather.toBukkitWeatherType());
        }

        // If the player has played before then send them the MOTD
        if (player.hasPlayedBefore()) {
            // Send the player the MOTD if they've played before
            for (String line : TextConfig.getText("motd.txt")) {
                player.sendMessage(line.replaceAll("\\{username}", player.getName()));
            }
        } else {
            // If they're joining for the first time then emit a special join message
            PlayerUtil.emitFirstJoinMessage(player);
            Bukkit.getConsoleSender().sendMessage(Message.format("connection.first_join_log", player.getName(), player.getUniqueId()));
        }

        // Send a join message
        event.setJoinMessage("");
        // Don't expose vanished players
        if (!PlayerUtil.isVanished(player)) {
            PlayerUtil.emitJoinMessage(player);
        }

        // If join_unvanished is set to true, unvanish them
        if (CoreToggleSetting.JOIN_UNVANISHED.getValue(mcp)) {
            // Get whether or not to fake a message
            String vanishBehavior = CoreMultiOptionSetting.VANISH_BEHAVIOR.getValue(mcp);
            boolean isSilent = !StringUtil.equalsIgnoreCase(vanishBehavior, "fake_messages", "legacy");

            // Unvanish the player
            PlayerUtil.setVanished(player, false, isSilent);
        }

        // Notify the player if they have mail
        if (mcp.getMail() != null && !mcp.getMail().isEmpty()) {
            player.sendMessage(Message.format("events.login.info.mail", mcp.getMail().size()));
        }

        // If there's unresolved member applications and the player has perms, send them a message
        if (player.hasPermission("syn.app.list")) {
            StaffDashboard dashboard = StaffDashboard.getInstance();
            List<MemberApplication> applications = dashboard.getMemberApplications();

            if (applications != null && !applications.isEmpty()) {
                player.sendMessage(Message.format("events.login.info.unresolved_applications", applications.size()));
            }
        }

        // If the player appears to be an alt of a banned player, notify staff members
        List<MinecraftProfile> possibleAlts = new ArrayList<>(mcp.getPartialPossibleAlts("n", "b"));
        if (possibleAlts.size() > 1) {
            if (Bukkit.getPluginManager().isPluginEnabled("BanManager")) {
                // Filter the list of alts for ones that are banned
                possibleAlts.removeIf(alt -> !BmAPI.isBanned(alt.getID()));
            }

            if (possibleAlts.size() > 1) {
                String bannedAltsMessage = Message.createFormattedList(
                        possibleAlts.stream().map(MinecraftProfile::getCurrentName).collect(Collectors.toList()),
                        Message.getColor("c4"),
                        Message.get("info_colored_lists.grammar_color")
                );

                // Send the message to all that have the permission
                for (Player p : Bukkit.getOnlinePlayers()) {
                    if (p.hasPermission("bm.notify.duplicateips")) {
                        p.sendMessage(Message.format("events.login.warning.banned_alt_joined", mcp.getCurrentName(), bannedAltsMessage));
                    }
                }
            }
        }
    }

    @EventHandler(priority = EventPriority.LOWEST, ignoreCancelled = true)
    public void onPlayerLeave(PlayerQuitEvent event) {
        ProfileManager pm = ProfileManager.getInstance();

        Player player = event.getPlayer();
        MinecraftProfile mcp = PlayerUtil.getProfile(player);

        String worldGroupName = mcp.getCurrentWorldGroupName();
        WorldGroupProfile wgp = mcp.getCurrentWorldGroupProfile();
        long currentTime = System.currentTimeMillis();

        // Update information for their current world group
        wgp.setLastLogOut(currentTime);
        wgp.setLastLocation(new SerializableLocation(player.getLocation()));

        // End the player being afk. if they are
        if (mcp.isAFK()) {
            wgp.setAfktimeEnd(currentTime);
        }

        // Update information for their MinecraftProfile
        mcp.setLastWorldGroup(worldGroupName);
        mcp.setLastLogOut(currentTime);

        // If announce_quit is set to true and they're unvanished, announce it
        event.setQuitMessage("");
        if (CoreToggleSetting.ANNOUNCE_QUIT.getValue(mcp) && !PlayerUtil.isVanished(player)) {
            PlayerUtil.emitQuitMessage(player);

            // Eject spectating players and notify them
            PlayerUtil.ejectSpectatingPlayers(player, false);
        } else {
            // Eject spectating players silently
            PlayerUtil.ejectSpectatingPlayers(player, true);
        }

        // Uncache the player's profile
        pm.uncacheMinecraftProfile(player.getUniqueId());
    }

}
