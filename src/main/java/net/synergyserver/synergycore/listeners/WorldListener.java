package net.synergyserver.synergycore.listeners;

import org.bukkit.entity.minecart.CommandMinecart;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.vehicle.VehicleCreateEvent;

/**
 * Listens to events that occur in the world.
 */
public class WorldListener implements Listener {

    @EventHandler(priority = EventPriority.LOWEST, ignoreCancelled = true)
    public void onCommandBlockPlace(BlockPlaceEvent event) {
        switch (event.getBlock().getType()) {
            case COMMAND_BLOCK, CHAIN_COMMAND_BLOCK, REPEATING_COMMAND_BLOCK, COMMAND_BLOCK_MINECART:
                if (!event.getPlayer().hasPermission("syn.command-blocks")) {
                    event.setCancelled(true);
                }
                break;
            default:
                break;
        }
    }

    @EventHandler(priority = EventPriority.LOWEST, ignoreCancelled = true)
    public void onCommandBlockPlace(VehicleCreateEvent event) {
        if (event.getVehicle() instanceof CommandMinecart) {
            event.setCancelled(true);
        }
    }

}
