package net.synergyserver.synergycore.listeners;

import com.onarandombox.MultiverseCore.event.MVTeleportEvent;
import net.synergyserver.synergycore.SerializableLocation;
import net.synergyserver.synergycore.SynergyCore;
import net.synergyserver.synergycore.WeatherType;
import net.synergyserver.synergycore.configs.Message;
import net.synergyserver.synergycore.database.DataManager;
import net.synergyserver.synergycore.events.AFKStatusChangeEvent;
import net.synergyserver.synergycore.events.WorldGroupProfileCreateEvent;
import net.synergyserver.synergycore.profiles.MinecraftProfile;
import net.synergyserver.synergycore.profiles.WorldGroupProfile;
import net.synergyserver.synergycore.settings.CoreToggleSetting;
import net.synergyserver.synergycore.settings.SettingPreferences;
import net.synergyserver.synergycore.utils.PlayerUtil;
import org.bukkit.Bukkit;
import org.bukkit.GameMode;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.craftbukkit.v1_19_R1.CraftWorld;
import org.bukkit.craftbukkit.v1_19_R1.entity.CraftEntity;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Parrot;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.CreatureSpawnEvent;
import org.bukkit.event.player.PlayerChangedWorldEvent;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.event.player.PlayerTeleportEvent;

/**
 * Listens to player movement events.
 */
public class PlayerMoveListener implements Listener {

    @EventHandler(priority = EventPriority.MONITOR, ignoreCancelled = true)
    public void onPlayerSwitchWorldGroup(PlayerTeleportEvent event) {
        String fromWorldGroup = SynergyCore.getWorldGroupName(event.getFrom().getWorld().getName());
        String toWorldGroup = SynergyCore.getWorldGroupName(event.getTo().getWorld().getName());

        // Ignore this event if the player is staying within the world group
        if (fromWorldGroup.equals(toWorldGroup)) {
            return;
        }

        DataManager dm = DataManager.getInstance();
        long currentTime = System.currentTimeMillis();
        Player player = event.getPlayer();
        MinecraftProfile mcp = PlayerUtil.getProfile(player);

        // Eject all riding entities from the player
        player.eject();

        // Eject any parrots from the player
        Entity leftParrot = player.getShoulderEntityLeft();
        Entity rightParrot = player.getShoulderEntityRight();
        if (leftParrot != null && (leftParrot instanceof Parrot)) {
            Parrot.Variant variant = ((Parrot) leftParrot).getVariant();
            Parrot spawnedParrot = ((CraftWorld) leftParrot.getWorld()).addEntity(((CraftEntity) leftParrot).getHandle(), CreatureSpawnEvent.SpawnReason.SHOULDER_ENTITY);
            spawnedParrot.setVariant(variant);
            spawnedParrot.setSitting(true);

            player.setShoulderEntityLeft(null);
        }
        if (rightParrot != null && (leftParrot instanceof Parrot)) {
            Parrot.Variant variant = ((Parrot) leftParrot).getVariant();
            Parrot spawnedParrot = ((CraftWorld) rightParrot.getWorld()).addEntity(((CraftEntity) rightParrot).getHandle(), CreatureSpawnEvent.SpawnReason.SHOULDER_ENTITY);
            spawnedParrot.setVariant(variant);
            spawnedParrot.setSitting(true);

            player.setShoulderEntityRight(null);
        }

        // Create a new WorldGroupProfile if they don't have one for the target world
        if (!mcp.hasWorldGroupProfile(toWorldGroup)) {
            // Create and save a new SettingPreferences in the database
            SettingPreferences sp = new SettingPreferences();
            dm.saveDataEntity(sp);

            // Create a new WorldGroupProfile
            WorldGroupProfile toWGP = new WorldGroupProfile(
                    toWorldGroup,
                    mcp.getID(),
                    currentTime,
                    currentTime,
                    new SerializableLocation(event.getFrom()),
                    sp.getID()
            );

            // Emit an event for other SynergyPlugins to hook into
            WorldGroupProfileCreateEvent wgpEvent = new WorldGroupProfileCreateEvent(toWGP);
            Bukkit.getPluginManager().callEvent(wgpEvent);

            mcp.addWorldGroupProfile(wgpEvent.getNewWGP());
            dm.saveDataEntity(wgpEvent.getNewWGP());
        }

        // Update information
        WorldGroupProfile fromWGP = mcp.getWorldGroupProfile(fromWorldGroup);
        fromWGP.setLastLogOut(currentTime);
        fromWGP.setLastLocation(new SerializableLocation(event.getFrom()));

        WorldGroupProfile toWGP = mcp.getWorldGroupProfile(toWorldGroup);
        toWGP.setLastLogIn(currentTime);
        toWGP.setLastAction(fromWGP.getLastAction());

        if (mcp.isAFK()) {
            fromWGP.setAfktimeEnd(currentTime);
            toWGP.setAfktimeStart(currentTime);
        }

        // Cache the new current WorldGroupProfile
        mcp.setCurrentWorldGroupProfile(toWGP);
    }

    @EventHandler(priority = EventPriority.MONITOR, ignoreCancelled = true)
    public void onPlayerChangedWorlds(PlayerChangedWorldEvent event) {
        // Dumb fix because bukkit is dumb
        Player player = event.getPlayer();
        MinecraftProfile mcp = PlayerUtil.getProfile(player);
        WorldGroupProfile wgp = mcp.getCurrentWorldGroupProfile();

        // Refresh the player's SettingGUI with their permissions for this world
        mcp.getCurrentWorldGroupProfile().createSettingGUI();

        // Set the player's personal time and weather
        Long personalTime = wgp.getPersonalTime();
        if (personalTime == null) {
            player.resetPlayerTime();
        } else {
            player.setPlayerTime(personalTime, false);
        }
        WeatherType personalWeather = wgp.getPersonalWeather();
        if (personalWeather == null) {
            player.resetPlayerWeather();
        } else {
            player.setPlayerWeather(personalWeather.toBukkitWeatherType());
        }

        player.setWalkSpeed(0.2F);
        player.setFlySpeed(0.1F);
    }


    @EventHandler(priority = EventPriority.HIGH, ignoreCancelled = true)
    public void onPlayerTeleport(PlayerTeleportEvent event) {
        Player player = event.getPlayer();
        String fromWorldGroup = SynergyCore.getWorldGroupName(event.getFrom().getWorld().getName());

        switch (event.getCause()) {
            case SPECTATE:
                // Check permissions for players teleporting with spectator mode if they're not dismounting
                if (!event.getTo().equals(event.getFrom()) && !player.hasPermission("syn.spectator-teleport")) {
                    player.sendMessage(Message.get("events.spectate.error.spectator_no_permission"));
                    event.setCancelled(true);
                    return;
                }

                // Find the target player
                Player targetPlayer = PlayerUtil.getNearestPlayer(event.getTo());
                if (targetPlayer == null) {
                    break;
                }

                MinecraftProfile targetMCP = PlayerUtil.getProfile(targetPlayer);

                // Check if the player has teleports disabled
                if (!CoreToggleSetting.TELEPORTS.getValue(targetMCP)) {
                    if (player.hasPermission("syn.setting.teleports.override")) {
                        // If the player is using bypass perms then allow it but give them a warning
                        player.sendMessage(Message.format("events.teleport.warning.bypassed_setting", targetMCP.getCurrentName(),
                                "teleport to them"));
                    } else {
                        // Otherwise prevent it and give them an error
                        player.sendMessage(Message.format("events.teleport.error.teleports_disabled", targetMCP.getCurrentName(),
                                "teleported to"));
                        event.setCancelled(true);
                        return;
                    }
                }
                break;
            case COMMAND:
                // Save the last location for use with /back
                MinecraftProfile mcp = PlayerUtil.getProfile(player);
                mcp.getCurrentWorldGroupProfile().setLastLocation(new SerializableLocation(player.getLocation()));
                mcp.setLastWorldGroup(fromWorldGroup);

                break;
            default:
                break;
        }

        // If this event wasn't caused by a player trying to spectate someone, ignore it from this point on
        if (player.getSpectatorTarget() == null) {
            return;
        }
        if (!(player.getSpectatorTarget() instanceof Player)) {
            return;
        }

        // Check if the target has allow_spectators toggled
        Player targetPlayer = (Player) player.getSpectatorTarget();
        MinecraftProfile targetPlayerProfile = PlayerUtil.getProfile(targetPlayer);
        // If allow_spectators is set to false then deny the player if they don't have override perms
        if (!CoreToggleSetting.ALLOW_SPECTATORS.getValue(targetPlayerProfile)) {
            // If the player has override perms, then allow it
            if (player.hasPermission("syn.setting.allow-spectators.override")) {
                return;
            }

            // Otherwise deny the player from spectating the target and send them a message
            player.sendMessage(Message.format("events.spectate.error.allow_spectators_disabled", targetPlayer.getName()));
            player.setSpectatorTarget(null);
            event.setCancelled(true);
        }
    }

    @EventHandler(priority = EventPriority.MONITOR, ignoreCancelled = true)
    public void onPlayerTeleportToPosition(PlayerTeleportEvent event) {
        Location loc = event.getTo();

        // Ignore this event if it's caused by a correction or item
        switch (event.getCause()) {
            case UNKNOWN: case ENDER_PEARL: case CHORUS_FRUIT:
                return;
            default: break;
        }

        Player player = event.getPlayer();
        String currentWorldGroup = SynergyCore.getWorldGroupName(player.getWorld().getName());
        String toWorldGroup = SynergyCore.getWorldGroupName(loc.getWorld().getName());

        // Ignore this event if the delta is too small and it's likely a position correction
        if (event.getFrom().getWorld().equals(event.getTo().getWorld()) && event.getFrom().distanceSquared(loc) < 100) {
            return;
        }

        // Ignore this event if it's assumed full_teleportation was/will be used (since the first teleport location is garbage)
        if (PlayerUtil.canUseFullTeleportation(player, loc.getWorld()) && !currentWorldGroup.equals(toWorldGroup)) {
            return;
        }

        // Ignore this event if they're likely teleporting to a player instead
        for (Player p : Bukkit.getOnlinePlayers()) {
            // Ignore the player if they're not in the same world
            if (!p.getWorld().getName().equals(loc.getWorld().getName())) {
                continue;
            }

            if (p.getLocation().distanceSquared(loc) < 0.1D) {
                return;
            }
        }

        String spyMessage = Message.format("events.spy.teleport_to_position", player.getName(), loc.getX(), loc.getY(), loc.getZ(), loc.getWorld().getName());

        // Notify all players snooping on the player teleporting
        for (Player p : Bukkit.getOnlinePlayers()) {
            MinecraftProfile mcp = PlayerUtil.getProfile(p);

            // Skip the player if they don't have the required permission or if they are the same person
            if (!p.hasPermission("syn.snoop") || p.equals(player)) {
                continue;
            }

            // Send a snoop message to the current player if their list contains their UUID
            if (mcp.getSpiedPlayers().contains(player.getUniqueId())) {
                p.sendMessage(spyMessage);
            }
        }

        // Also send the message to console
        Bukkit.getConsoleSender().sendMessage(spyMessage);
    }

    @EventHandler(priority = EventPriority.MONITOR, ignoreCancelled = true)
    public void onPlayerSwitchWorlds(PlayerTeleportEvent event) {
        // If the player did not switch worlds then ignore this event
        if (event.getFrom().getWorld().equals(event.getTo().getWorld())){
            return;
        }

        // If this event was not caused by a command then ignore it
        if (!event.getCause().equals(PlayerTeleportEvent.TeleportCause.COMMAND)){
            return;
        }

        // Teleport them a second time because of Multiverse
        Player player = event.getPlayer();
        if (PlayerUtil.canUseFullTeleportation(player, event.getTo().getWorld())) {
            player.teleport(event.getTo());
        }
    }

    @EventHandler(priority = EventPriority.HIGH, ignoreCancelled = true)
    public void onMultiverseTeleport(MVTeleportEvent event){
        Player player = event.getTeleportee();
        MinecraftProfile mcp = PlayerUtil.getProfile(player);
        // Since Multiverse doesn't teleport players with the Command TeleportCause, set the last location here
        mcp.getCurrentWorldGroupProfile().setLastLocation(new SerializableLocation(player.getLocation()));
        mcp.setLastWorldGroup(mcp.getCurrentWorldGroupName());
    }

    @EventHandler(priority = EventPriority.MONITOR, ignoreCancelled = true)
    public void onPlayerMove(PlayerMoveEvent event) {
        // Ghetto fix for ignoring the ghost event sometimes created by teleporting
        // In this case, the event#to is the same coordinates as the event#from but with 0 as the pitch and yaw
        Location modifiedTo = new Location(event.getTo().getWorld(), event.getTo().getX(), event.getTo().getY(), event.getTo().getZ(), 0F, 0F);
        if (modifiedTo.equals(event.getFrom())) {
            return;
        }

        Player player = event.getPlayer();
        MinecraftProfile mcp = PlayerUtil.getProfile(player);

        // Don't count movement for spectators for unafking
        if (!player.getGameMode().equals(GameMode.SPECTATOR)) {
            WorldGroupProfile wgp = mcp.getCurrentWorldGroupProfile();
            float pitch = player.getEyeLocation().getPitch();
            float yaw = player.getEyeLocation().getYaw();

            // Only if their direction changed then unafk them
            if (!wgp.isSameDirection(pitch, yaw)) {
                if (mcp.isAFK()) {
                    // Unafk the player and emit an event
                    wgp.setAfktimeEnd(System.currentTimeMillis());
                    AFKStatusChangeEvent afkEvent = new AFKStatusChangeEvent(false, mcp, false);
                    Bukkit.getPluginManager().callEvent(afkEvent);
                }

                // Set their last action to the current time
                wgp.setLastAction(System.currentTimeMillis());
            }
        }

        // Launchpad things
        if (!player.hasPermission("syn.launchpad") || player.getGameMode().equals(GameMode.SPECTATOR)) {
            return;
        }

        // Check to see if the block combination is correct to be a launchpad
        Location location = player.getLocation();
        if (!location.getBlock().getType().equals(Material.HEAVY_WEIGHTED_PRESSURE_PLATE)) {
            return;
        }
        Material bottomBlockType = location.subtract(0, 1, 0).getBlock().getType();
        if (!bottomBlockType.equals(Material.REDSTONE_LAMP)) {
            return;
        }

        // Launch the player and give them a sound effect
        player.setVelocity(location.getDirection().multiply(3.5).setY(1));
        player.getWorld().playSound(location, Sound.ENTITY_ENDER_DRAGON_FLAP, 1, 0);
    }

}
