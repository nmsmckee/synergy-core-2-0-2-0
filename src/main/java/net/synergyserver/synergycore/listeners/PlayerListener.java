package net.synergyserver.synergycore.listeners;

import net.minecraft.core.BlockPosition;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.network.chat.IChatBaseComponent;
import net.minecraft.server.level.EntityPlayer;
import net.minecraft.world.level.block.entity.TileEntity;
import net.minecraft.world.level.block.entity.TileEntitySign;
import net.synergyserver.synergycore.BoundSign;
import net.synergyserver.synergycore.Chat;
import net.synergyserver.synergycore.Mail;
import net.synergyserver.synergycore.PowerTool;
import net.synergyserver.synergycore.PrivateMessage;
import net.synergyserver.synergycore.SerializableLocation;
import net.synergyserver.synergycore.SynergyCore;
import net.synergyserver.synergycore.Teleport;
import net.synergyserver.synergycore.TeleportType;
import net.synergyserver.synergycore.commands.CommandManager;
import net.synergyserver.synergycore.configs.Message;
import net.synergyserver.synergycore.database.DataManager;
import net.synergyserver.synergycore.events.AFKStatusChangeEvent;
import net.synergyserver.synergycore.events.DonatorStatusUpdateEvent;
import net.synergyserver.synergycore.events.MailSendEvent;
import net.synergyserver.synergycore.events.MemberApplicationSubmitEvent;
import net.synergyserver.synergycore.events.PrivateMessageSendEvent;
import net.synergyserver.synergycore.events.ServiceConnectEvent;
import net.synergyserver.synergycore.events.TeleportDirectEvent;
import net.synergyserver.synergycore.events.TeleportRequestAcceptEvent;
import net.synergyserver.synergycore.events.TeleportRequestSendEvent;
import net.synergyserver.synergycore.events.TextBindToSignEvent;
import net.synergyserver.synergycore.profiles.MinecraftProfile;
import net.synergyserver.synergycore.profiles.WorldGroupProfile;
import net.synergyserver.synergycore.settings.CoreMultiOptionSetting;
import net.synergyserver.synergycore.settings.CoreToggleSetting;
import net.synergyserver.synergycore.utils.BukkitUtil;
import net.synergyserver.synergycore.utils.ItemUtil;
import net.synergyserver.synergycore.utils.PlayerUtil;
import net.synergyserver.synergycore.utils.TimeUtil;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.OfflinePlayer;
import org.bukkit.Sound;
import org.bukkit.block.Block;
import org.bukkit.block.BlockState;
import org.bukkit.block.Chest;
import org.bukkit.block.Sign;
import org.bukkit.block.Skull;
import org.bukkit.command.Command;
import org.bukkit.craftbukkit.v1_19_R1.CraftWorld;
import org.bukkit.craftbukkit.v1_19_R1.entity.CraftPlayer;
import org.bukkit.craftbukkit.v1_19_R1.inventory.CraftItemStack;
import org.bukkit.entity.Player;
import org.bukkit.event.Event;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.block.SignChangeEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.EntityPickupItemEvent;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.event.player.PlayerCommandPreprocessEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerTeleportEvent;
import org.bukkit.inventory.EquipmentSlot;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.BookMeta;
import ru.tehkode.permissions.PermissionUser;
import ru.tehkode.permissions.events.PermissionEntityEvent;

import java.lang.reflect.Field;
import java.util.HashSet;
import java.util.UUID;

/**
 * Listens to all player-related events that are not covered by other listeners.
 */
public class PlayerListener implements Listener {

    @EventHandler(priority = EventPriority.MONITOR, ignoreCancelled = true)
    public void onPlayerDeath(PlayerDeathEvent event) {
        // Save the last location for use with /back
        Player player = event.getEntity().getPlayer();
        MinecraftProfile mcp = PlayerUtil.getProfile(player);
        SerializableLocation loc = new SerializableLocation(player.getLocation());
        mcp.getCurrentWorldGroupProfile().setLastLocation(loc);
        mcp.setLastWorldGroup(mcp.getCurrentWorldGroupName());

        // Send them a message with their coordinates
        player.sendMessage(Message.format("events.death.info.whoops", loc.getX(), loc.getY(), loc.getZ()));

        // Save the death message to distribute later, but prevent it from being sent to everyone
        // Use NMS for the JSON hovering things
        IChatBaseComponent message = ((CraftPlayer) event.getEntity()).getHandle().er() // getCombatTracker
                .b(); //getDeathMessage
        event.setDeathMessage("");

        // Don't broadcast death events caused by custom plugins
        if (player.getLastDamageCause() != null && player.getLastDamageCause().getCause().equals(EntityDamageEvent.DamageCause.CUSTOM)) {
            return;
        }

        // Don't broadcast the death message if the player has them disabled
        if (!CoreToggleSetting.EMIT_DEATH_MESSAGES.getValue(mcp)) {
            return;
        }

        // Broadcast the death message
        Chat.getInstance().broadcastDeathMessage(player, IChatBaseComponent.ChatSerializer.a(message));
    }

    @EventHandler(priority = EventPriority.MONITOR, ignoreCancelled = true)
    public void onRightClickBlock(PlayerInteractEvent event) {
        Player player = event.getPlayer();

        // Ignore the event if it wasn't a right click or if they were sneaking
        if (!event.getAction().equals(Action.RIGHT_CLICK_BLOCK) || player.isSneaking()) {
            return;
        }
        // Ignore the event created by the off hand to avoid double event calls
        if (event.getHand().equals(EquipmentSlot.OFF_HAND)) {
            return;
        }

        BlockState blockState = event.getClickedBlock().getState();

        if (blockState instanceof Skull) {
            // Getting skull owner
            OfflinePlayer skullOwner = ((Skull) blockState).getOwningPlayer();

            // If the skull data is missing or if it's a custom texture, give the player a special message
            if (skullOwner == null || skullOwner.getName() == null) {
                player.sendMessage(Message.get("events.skull.info.no_owner"));
                return;
            }

            // Otherwise give them the name of the owning player
            player.sendMessage(Message.format("events.skull.info.has_owner", skullOwner.getName()));
            // Don't let the item in their hand be used
            event.setUseItemInHand(Event.Result.DENY);
        } else if (blockState instanceof Sign) {
            Sign sign = (Sign) blockState;
            DataManager dm = DataManager.getInstance();
            String signID = BukkitUtil.blockLocationToString(blockState.getLocation());

            // Determine what operation the player is trying to do on the sign
            switch (player.getInventory().getItemInMainHand().getType()) {
                // Editing signs
                case FEATHER:
                case OAK_SIGN: case SPRUCE_SIGN: case BIRCH_SIGN: case JUNGLE_SIGN: case ACACIA_SIGN: case DARK_OAK_SIGN: case CRIMSON_SIGN: case WARPED_SIGN:
                case OAK_WALL_SIGN: case SPRUCE_WALL_SIGN: case BIRCH_WALL_SIGN: case JUNGLE_WALL_SIGN: case ACACIA_WALL_SIGN: case DARK_OAK_WALL_SIGN: case CRIMSON_WALL_SIGN: case WARPED_WALL_SIGN:
                    // Give the player an error if they don't have permission to edit signs
                    if (!player.hasPermission("syn.signs.edit")) {
                        player.sendMessage(Message.get("events.sign.edit.error.no_permission"));
                        break;
                    }

                    // Set the sign to editable so Minecraft doesn't complain
                    sign.setEditable(true);

                    // Replace all color codes with & before sending the interface to the player
                    for (int i = 0; i < sign.getLines().length; i++) {
                        sign.setLine(i, sign.getLine(i).replace("§", "&"));
                    }
                    sign.update();

                    // Open the sign interface for the player
                    Bukkit.getScheduler().runTask(SynergyCore.getPlugin(), () -> {
                        Object sign1 = ((CraftWorld) blockState.getWorld()).getHandle().getBlockEntity(
                                new BlockPosition(blockState.getX(), blockState.getY(), blockState.getZ()), true
                        ); // no idea what validate does

                        // If the tile entity isn't a sign for some reason then bail out and give the player an error
                        if (!(sign1 instanceof TileEntitySign)) {
                            player.sendMessage(Message.get("error.unknown_error"));
                            return;
                        }

                        TileEntitySign signTile = (TileEntitySign) sign1;
                        signTile.a(true); // Set isEditable to true
                        ((CraftPlayer) player).getHandle().a(signTile); // openSign
                    });
                    break;
                // Binding text to sign
                case WRITABLE_BOOK:
                    // Give the player an error if they don't have permission to bind text to signs
                    if (!player.hasPermission("syn.signs.bind-text")) {
                        player.sendMessage(Message.get("events.sign.bind_text.error.no_permission"));
                        break;
                    }

                    BookMeta bookMeta = (BookMeta) player.getInventory().getItemInMainHand().getItemMeta();

                    // Give the player an error if there isn't any text to bind
                    if (!bookMeta.hasPages()) {
                        player.sendMessage(Message.get("events.sign.bind_text.error.no_pages"));
                        break;
                    }

                    String[] textToBind = bookMeta.getPage(1).split("\\n");
                    for (int i = 0; i < textToBind.length; i++) {
                        // Remove black color codes from the message because Minecraft is dumb
                        textToBind[i] = textToBind[i].replace("§0", "");
                    }

                    TextBindToSignEvent signEvent = new TextBindToSignEvent(player, textToBind, sign);
                    Bukkit.getPluginManager().callEvent(signEvent);

                    // Check if the event was cancelled before continuing
                    if (signEvent.isCancelled()) {
                        break;
                    }

                    // Format the text, if the player has permission to
                    String[] boundText = signEvent.getText();
                    for (int i = 0; i < boundText.length; i++) {
                        boundText[i] = Message.translateCodes(boundText[i], player, "syn.signs.bind-text");
                    }

                    // Update the existing BoundSign or create one if it doesn't already exist
                    BoundSign boundSign = dm.getDataEntity(BoundSign.class, signID);

                    if (boundSign != null) {
                        boundSign.setBoundText(boundText);
                    } else {
                        boundSign = new BoundSign(blockState.getLocation(), boundText);
                        dm.saveDataEntity(boundSign);
                    }

                    // Give the player feedback
                    player.sendMessage(Message.get("events.sign.bind_text.info.success"));
                    break;
                // Seeing bound text
                default:
                    BoundSign clickedBoundSign = dm.getDataEntity(BoundSign.class, signID);

                    // Ignore the event if there is no text bound to the sign
                    if (clickedBoundSign == null || clickedBoundSign.getBoundText() == null) {
                        break;
                    }

                    // Send the text and add a blank line before and after the lines of text
                    player.sendMessage(" ");
                    player.sendMessage(clickedBoundSign.getBoundText());
                    player.sendMessage(" ");
                    break;
            }
        }
    }

    @EventHandler(priority = EventPriority.MONITOR)
    public void onPlayerSubmitMemberApplication(MemberApplicationSubmitEvent event) {
        String name = PlayerUtil.getName(event.getMemberApplication().getPlayerID());
        for (Player p : Bukkit.getOnlinePlayers()) {
            if (p.hasPermission("syn.app.list")) {
                p.sendMessage(Message.format("events.application.info.new_app_alert", name));
            }
        }
    }


    @EventHandler(priority = EventPriority.HIGHEST)
    public void onPowerToolUse(PlayerInteractEvent event) {
        Player player = event.getPlayer();

        // Ignore the event if it wasn't a left click
        if (!event.getAction().equals(Action.LEFT_CLICK_AIR) && !event.getAction().equals(Action.LEFT_CLICK_BLOCK)) {
            return;
        }

        // Ignore the event created by the off hand to avoid double event calls
        if (event.getHand().equals(EquipmentSlot.OFF_HAND)) {
            return;
        }

        ItemStack item = player.getInventory().getItemInMainHand();

        // Ignore the event if the player isn't holding an item
        if (item == null || item.getType().equals(Material.AIR)) {
            return;
        }

        MinecraftProfile mcp = PlayerUtil.getProfile(player);
        String powerToolKey = ItemUtil.itemTypeToString(item);

        // Ignore the event if the item isn't a powertool
        if (!mcp.getPowerTools().containsKey(powerToolKey)) {
            return;
        }

        // Prevent the block and the item of the powertool from being used
        event.setCancelled(true);

        // Loop through the command list and execute every command in the list
        PowerTool powerTool = mcp.getPowerTools().get(powerToolKey);
        long millisPassed = System.currentTimeMillis() - powerTool.getLastUsed();
        long cooldown = TimeUtil.toRoundedUnit(TimeUtil.TimeUnit.SECOND, TimeUtil.TimeUnit.MILLI,
                SynergyCore.getPluginConfig().getInt("powertool_cooldown_seconds"));
        long remainingTime = cooldown - millisPassed;

        // Give the player an error if the cooldown hasn't ended
        if (remainingTime > 0) {
            String timeMessage = Message.getTimeMessage(remainingTime, 2, 1);
            player.sendMessage(Message.format("events.powertool.error.cooldown", timeMessage));
            return;
        }

        for (String command : powerTool.getCommands()) {
            player.performCommand(command.replaceFirst("/", ""));
        }

        // Set the last time this tool was used
        powerTool.setLastUsed(System.currentTimeMillis());
    }

    @EventHandler(priority = EventPriority.MONITOR, ignoreCancelled = true)
    public void onPlayerBreakBlock(BlockBreakEvent event) {
        Block block = event.getBlock();

        // Ignore the event if it's not a sign that was broken
        if (!(block.getState() instanceof Sign)) {
            return;
        }

        String signID = BukkitUtil.blockLocationToString(block.getLocation());

        // If there's a bound sign in the database then remove it
        DataManager dm = DataManager.getInstance();

        BoundSign boundSign = dm.getDataEntity(BoundSign.class, signID);
        if (boundSign != null) {
            dm.deleteDataEntity(boundSign);
        }
    }

    @EventHandler(priority = EventPriority.MONITOR, ignoreCancelled = true)
    public void onPlayerRankChange(PermissionEntityEvent event) {
        // Ignore the event if it's not a player's rank changing
        if (!event.getAction().equals(PermissionEntityEvent.Action.INHERITANCE_CHANGED)) {
            return;
        }
        if (!(event.getEntity() instanceof PermissionUser)) {
            return;
        }

        PermissionUser user = (PermissionUser) event.getEntity();
        Player player = user.getPlayer();

        if (player != null && player.isOnline()) {
            // Update the player's display name
            PlayerUtil.updateDisplayName(player);

            // Update their SettingGUI
            PlayerUtil.getProfile(player).getCurrentWorldGroupProfile().createSettingGUI();
        }
    }

    @EventHandler(priority = EventPriority.LOWEST, ignoreCancelled = true)
    public void onPrivateMessageCancelCheck(PrivateMessageSendEvent event) {
        UUID senderID = event.getPrivateMessage().getSender();
        UUID receiverID = event.getPrivateMessage().getReceiver();

        // Check if the sender is ignored by the receiver
        boolean senderIsIgnored = false;
        MinecraftProfile receiverMCP = PlayerUtil.getProfile(receiverID);
        if (receiverMCP != null) {
            senderIsIgnored = receiverMCP.hasIgnored(senderID);
        }

        // Check if the sender cannot be ignored
        Player sender = Bukkit.getPlayer(senderID);
        boolean ignoreExempt = sender == null || sender.hasPermission("syn.ignore.exempt");

        // If the sender is truly ignored, cancel the event and send the sender a message
        if (senderIsIgnored && !ignoreExempt) {
            sender.sendMessage(Message.format("events.event_cancelled.error.ignored", PlayerUtil.getName(receiverID), "message"));
            event.setCancelled(true);
        }
    }

    @EventHandler(priority = EventPriority.LOWEST, ignoreCancelled = true)
    public void onMailCancelCheck(MailSendEvent event) {
        UUID senderID = event.getMail().getSender();
        UUID receiverID = event.getMail().getReceiver();

        // Check if the sender is ignored by the receiver
        MinecraftProfile receiverMCP;
        if (PlayerUtil.isOnline(receiverID)) {
            receiverMCP = PlayerUtil.getProfile(receiverID);
        } else {
            receiverMCP = DataManager.getInstance().getPartialDataEntity(MinecraftProfile.class, receiverID, "ig");
        }

        // Check if the sender cannot be ignored
        Player sender = Bukkit.getPlayer(senderID);
        boolean ignoreExempt = sender == null || sender.hasPermission("syn.ignore.exempt");

        // If the sender is truly ignored, cancel the event and send the sender a message
        if (receiverMCP.hasIgnored(senderID) && !ignoreExempt) {
            sender.sendMessage(Message.format("events.event_cancelled.error.ignored", PlayerUtil.getName(receiverID), "mail"));
            event.setCancelled(true);
        }
    }

    @EventHandler(priority = EventPriority.LOWEST, ignoreCancelled = true)
    public void onTeleportRequestCancelCheck(TeleportRequestSendEvent event) {
        Teleport teleport = event.getTeleport();
        UUID senderID = teleport.getSender();
        Player sender = Bukkit.getPlayer(senderID);
        MinecraftProfile receiverMCP = PlayerUtil.getProfile(teleport.getReceiver());

        // If the sender is ignored, cancel the event and send the sender a message
        if (receiverMCP.hasIgnored(senderID) && !sender.hasPermission("syn.ignore.exempt")) {
            sender.sendMessage(Message.format("events.event_cancelled.error.ignored",
                    receiverMCP.getCurrentName(), "teleport request"));
            event.setCancelled(true);
            return;
        }

        // Check if the player has teleports disabled
        if (!CoreToggleSetting.TELEPORTS.getValue(receiverMCP)) {
            if (sender.hasPermission("syn.setting.teleports.override")) {
                // If the player is using bypass perms then allow it but give them a warning
                sender.sendMessage(Message.format("events.teleport.warning.bypassed_setting", receiverMCP.getCurrentName(),
                        "send a teleport request"));
            } else {
                // Otherwise prevent it and give them an error
                sender.sendMessage(Message.format("events.teleport.error.teleports_disabled", receiverMCP.getCurrentName(),
                        "sent a teleport request"));
                event.setCancelled(true);
            }
        }
    }

    @EventHandler(priority = EventPriority.LOWEST, ignoreCancelled = true)
    public void onTeleportDirectCancelCheck(TeleportDirectEvent event) {
        Teleport teleport = event.getTeleport();
        UUID senderID = teleport.getSender();
        Player sender = Bukkit.getPlayer(senderID);
        MinecraftProfile receiverMCP = PlayerUtil.getProfile(teleport.getReceiver());

        // If the sender is ignored, cancel the event and send the sender a message
        if (receiverMCP.hasIgnored(senderID) && !sender.hasPermission("syn.ignore.exempt")) {
            String action = teleport.getTeleporter().equals(teleport.getReceiver()) ? "teleport" : "teleport to";
            sender.sendMessage(Message.format("events.event_cancelled.error.teleport_prevented",
                    action, receiverMCP.getCurrentName()));
            event.setCancelled(true);
            return;
        }

        // Check if the player has teleports disabled
        if (!CoreToggleSetting.TELEPORTS.getValue(receiverMCP)) {
            if (sender.hasPermission("syn.setting.teleports.override")) {
                // If the player is using bypass perms then allow it but give them a warning
                String action = teleport.getTeleporter().equals(teleport.getReceiver()) ? "teleport them" : "teleport to them";
                sender.sendMessage(Message.format("events.teleport.warning.bypassed_setting", receiverMCP.getCurrentName(), action));
            } else {
                // Otherwise prevent it and give them an error
                String action = teleport.getTeleporter().equals(teleport.getReceiver()) ? "teleported" : "teleported to";
                sender.sendMessage(Message.format("events.teleport.error.teleports_disabled", receiverMCP.getCurrentName(), action));
                event.setCancelled(true);
            }
        }
    }

    @EventHandler(priority = EventPriority.MONITOR, ignoreCancelled = true)
    public void onTeleportDirect(TeleportDirectEvent event) {
        Teleport teleport = event.getTeleport();
        Player teleporter = Bukkit.getPlayer(teleport.getTeleporter());
        Player teleportee = Bukkit.getPlayer(teleport.getTeleportee());

        // Notify all players snooping on the teleporting player
        for (Player p : Bukkit.getOnlinePlayers()) {
            MinecraftProfile mcp = PlayerUtil.getProfile(p);

            // Skip current player if they don't have the required permission or if they are in the teleport
            if (!p.hasPermission("syn.snoop") || p.equals(teleporter) || p.equals(teleportee)) {
                continue;
            }

            // Send a snoop message to the current player if their list contains the teleporter's UUID
            if (mcp.getSnoopedPlayers().contains(teleporter.getUniqueId())) {
                p.sendMessage(Message.format("events.spy.teleport_to_player", teleporter.getName(), teleportee.getName()));
            }
        }

        // Tell console of the position
        Location loc = teleport.getTeleportLocation();
        String spyMessage = Message.format("events.spy.teleport_to_position", teleporter.getName(), loc.getX(), loc.getY(), loc.getZ(), loc.getWorld().getName());
        Bukkit.getConsoleSender().sendMessage(spyMessage);
    }

    @EventHandler(priority = EventPriority.LOW, ignoreCancelled = true)
    public void onTeleportRequestAutoAccept(TeleportRequestSendEvent event) {
        Teleport teleport = event.getTeleport();
        MinecraftProfile receiverMCP = PlayerUtil.getProfile(teleport.getReceiver());

        // Check if the auto_tpaccept setting matches the type of teleport request
        String tpacceptCondition = CoreMultiOptionSetting.AUTO_TPACCEPT.getValue(receiverMCP);
        switch (tpacceptCondition) {
            case "none":
                return;
            case "tpr_only":
                if (!teleport.getType().equals(TeleportType.TO_RECEIVER)) {
                    return;
                }
                break;
            case "all":
                break;
            default:
                break;
        }

        // Emit a event
        TeleportRequestAcceptEvent acceptEvent = new TeleportRequestAcceptEvent(teleport);
        Bukkit.getPluginManager().callEvent(acceptEvent);

        // Check if the event was cancelled before continuing
        if (acceptEvent.isCancelled()) {
            return;
        }

        // Teleport the teleporter
        Player teleporter = Bukkit.getPlayer(teleport.getTeleporter());
        boolean success = teleporter.teleport(teleport.getTeleportLocation(), PlayerTeleportEvent.TeleportCause.COMMAND);

        // Cancel the event since we resolved the request
        event.setCancelled(true);

        // Give feedback
        if (success) {
            Player teleportee = Bukkit.getPlayer(teleport.getTeleportee());
            teleporter.sendMessage(Message.format("commands.teleportation.info.teleporting_teleporter", teleportee.getName()));
            // If the teleportee isn't the receiver then give them feedback too
            if (!teleport.getTeleportee().equals(teleport.getReceiver())) {
                teleportee.sendMessage(Message.format("commands.teleportation.info.teleporting_teleportee", teleporter.getName()));
            }
        }
    }

    @EventHandler(priority = EventPriority.MONITOR, ignoreCancelled = true)
    public void onPrivateMessageSend(PrivateMessageSendEvent event) {
        PrivateMessage message = event.getPrivateMessage();

        // Ignore the event if the receiver is console
        if (SynergyCore.SERVER_ID.equals(message.getReceiver())) {
            return;
        }

        Player receiver = Bukkit.getPlayer(message.getReceiver());
        MinecraftProfile receiverMCP = PlayerUtil.getProfile(receiver);
        // If alert_sounds is set to true for the receiver then make a sound
        if (CoreToggleSetting.ALERT_SOUNDS.getValue(receiverMCP)) {
            receiver.playSound(receiver.getLocation(), Sound.ENTITY_ARROW_HIT_PLAYER, 1, 0);
        }

        // Ignore the event from this point on if the sender is console
        if (SynergyCore.SERVER_ID.equals(message.getSender())) {
            return;
        }

        Player sender = Bukkit.getPlayer(message.getSender());
        MinecraftProfile senderMCP = PlayerUtil.getProfile(sender);
        // If afk_target_notification is set to true for the sender and the receiver is afk then give a message
        if (CoreToggleSetting.AFK_TARGET_NOTIFICATION.getValue(senderMCP) && receiverMCP.isAFK()){
            sender.sendMessage(Message.format("afk.warning.target_afk", receiver.getName()));
        }

        String spyMessage = Message.format("events.spy.pm",
                PlayerUtil.getName(message.getSender()),
                PlayerUtil.getName(message.getReceiver()),
                message.getMessage()
        );

        // Notify all players spying on either of the messaging players
        for (Player p : Bukkit.getOnlinePlayers()) {
            MinecraftProfile mcp = PlayerUtil.getProfile(p);
            HashSet<UUID> spiedPlayers = mcp.getSpiedPlayers();

            // Skip the player if they don't have the required permission
            if (!p.hasPermission("syn.socialspy")) {
                continue;
            }

            // If either player isn't being spied on then ignore the message
            if (!(spiedPlayers.contains(message.getSender()) || spiedPlayers.contains(message.getReceiver()) || mcp.hasGlobalSpyingEnabled())) {
                continue;
            }

            // If the spying player is participating in the message then ignore it
            if (message.getSender().equals(p.getUniqueId()) || message.getReceiver().equals(p.getUniqueId())) {
                continue;
            }

            // Notify the spying player of the message
            p.sendMessage(spyMessage);
        }

        // Also send the message tp console
        Bukkit.getConsoleSender().sendMessage(spyMessage);
    }

    @EventHandler(priority = EventPriority.MONITOR, ignoreCancelled = true)
    public void onMailSend(MailSendEvent event) {
        Mail mail = event.getMail();
        MinecraftProfile receiverMCP = PlayerUtil.getProfile(mail.getReceiver());

        // If the receiver is online then give them a notification
        if (receiverMCP != null) {
            Player receiver = receiverMCP.getPlayer();

            // If alert_sounds is set to true for the receiver then make a sound
            if (CoreToggleSetting.ALERT_SOUNDS.getValue(receiverMCP)) {
                receiver.playSound(receiver.getLocation(), Sound.ENTITY_ARROW_HIT_PLAYER, 1, 0);
            }
        }

        // Ignore the event from this point on if the sender is console
        if (SynergyCore.SERVER_ID.equals(mail.getSender())) {
            return;
        }

        if (receiverMCP != null) {
            // If afk_target_notification is set to true for the sender and the receiver is afk then give a message
            Player sender = Bukkit.getPlayer(mail.getSender());
            MinecraftProfile senderMCP = PlayerUtil.getProfile(sender);
            if (CoreToggleSetting.AFK_TARGET_NOTIFICATION.getValue(senderMCP) && receiverMCP.isAFK()){
                sender.sendMessage(Message.format("afk.warning.target_afk", receiverMCP.getCurrentName()));
            }
        }

        String spyMessage = Message.format("events.spy.mail",
                PlayerUtil.getName(mail.getSender()),
                PlayerUtil.getName(mail.getReceiver()),
                mail.getMessage()
        );

        // Notify all players spying on either of the messaging players
        for (Player p : Bukkit.getOnlinePlayers()) {
            MinecraftProfile mcp = PlayerUtil.getProfile(p);
            HashSet<UUID> spiedPlayers = mcp.getSpiedPlayers();

            // Skip the player if they don't have the required permission
            if (!p.hasPermission("syn.socialspy")) {
                continue;
            }

            // If either player isn't being spied on then ignore the message
            if (!(spiedPlayers.contains(mail.getSender()) || spiedPlayers.contains(mail.getReceiver()) || mcp.hasGlobalSpyingEnabled())) {
                continue;
            }

            // If the spying player is participating in the message then ignore it
            if (mail.getSender().equals(p.getUniqueId()) || mail.getReceiver().equals(p.getUniqueId())) {
                continue;
            }

            // Notify the spying player of the message
            p.sendMessage(spyMessage);
        }

        // Also send the message tp console
        Bukkit.getConsoleSender().sendMessage(spyMessage);
    }

    @EventHandler(priority = EventPriority.MONITOR, ignoreCancelled = true)
    public void onTeleportRequestSend(TeleportRequestSendEvent event) {
        Teleport teleport = event.getTeleport();
        Player sender = Bukkit.getPlayer(teleport.getSender());
        MinecraftProfile senderMCP = PlayerUtil.getProfile(sender);
        Player receiver = Bukkit.getPlayer(teleport.getReceiver());
        MinecraftProfile receiverMCP = PlayerUtil.getProfile(receiver);

        // If afk_target_notification is set to true for the sender and the receiver is afk then give a message
        if (CoreToggleSetting.AFK_TARGET_NOTIFICATION.getValue(senderMCP) && receiverMCP.isAFK()){
            sender.sendMessage(Message.format("afk.warning.target_afk", receiver.getName()));
        }

        // If alert_sounds is set to true for the receiver then make a sound
        if (CoreToggleSetting.ALERT_SOUNDS.getValue(receiverMCP)) {
            receiver.playSound(receiver.getLocation(), Sound.ENTITY_ARROW_HIT_PLAYER, 1, 0);
        }
    }

    @EventHandler(priority = EventPriority.LOWEST, ignoreCancelled = true)
    public void onPlayerCommandPreprocessLowercase(PlayerCommandPreprocessEvent event) {
        String[] parsedCommand = event.getMessage().split(" ");
        parsedCommand[0] = parsedCommand[0].toLowerCase();
        event.setMessage(String.join(" ", parsedCommand));
    }

    @EventHandler(priority = EventPriority.MONITOR, ignoreCancelled = true)
    public void onPlayerCommandPreprocess(PlayerCommandPreprocessEvent event) {
        Player player = event.getPlayer();
        MinecraftProfile mcp = PlayerUtil.getProfile(player);
        WorldGroupProfile wgp = mcp.getCurrentWorldGroupProfile();

        // Parse the command
        CommandManager cm = CommandManager.getInstance();
        String[] parsedCommand = event.getMessage().split(" ");
        String commandLabel = parsedCommand[0].replaceFirst("/", "");
        Command command = cm.getCommand(commandLabel);

        // Ignore the command if it's not valid
        if (command == null) {
            return;
        }

        // If the command isn't the afk command, update the player's afk status if applicable
        if (!cm.areCommandsEqual(commandLabel, "afk")) {
            // If the player is afk then unafk them and emit an event
            if (mcp.isAFK()) {
                wgp.setAfktimeEnd(System.currentTimeMillis());
                AFKStatusChangeEvent afkEvent = new AFKStatusChangeEvent(false, mcp, false);
                Bukkit.getPluginManager().callEvent(afkEvent);
            }
        }

        // If the command isn't the mail command, remind the player that they have mail if they do
        if (!cm.areCommandsEqual(commandLabel, "mail")) {
            if (mcp.getMail() != null && !mcp.getMail().isEmpty()) {
                player.sendMessage(Message.format("mail.info.unread_mail", mcp.getMail().size()));
            }
        }

        // Set their last action to the current time
        wgp.setLastAction(System.currentTimeMillis());

        // Ignore the command for /snoop if it's part of the blacklist
        if (cm.getSnoopBlacklistCommands().contains(command)) {
            return;
        }

        // Notify all players snooping on the player running the command
        for (Player p : Bukkit.getOnlinePlayers()) {
            MinecraftProfile profile = PlayerUtil.getProfile(p);

            // Skip the player if they don't have the required permission or if they are in the teleport
            if (!p.hasPermission("syn.snoop") || p.equals(player)) {
                continue;
            }

            // Send a snoop message to the current player if their list contains their UUID
            if (profile.getSnoopedPlayers().contains(player.getUniqueId())) {
                p.sendMessage(Message.format("events.spy.command", player.getName(), event.getMessage()));
            }
        }
    }

    @EventHandler(priority = EventPriority.MONITOR, ignoreCancelled = true)
    public void onAFKStatusChange(AFKStatusChangeEvent event) {
        MinecraftProfile emitterMCP = event.getPlayer();
        Player player = emitterMCP.getPlayer();

        // If they're becoming afk, store their current pitch and yaw
        if (event.getNewAFKStatus()) {
            WorldGroupProfile wgp = emitterMCP.getCurrentWorldGroupProfile();
            Location eyeLocation = player.getEyeLocation();
            wgp.setLastPitch(eyeLocation.getPitch());
            wgp.setLastYaw(eyeLocation.getYaw());
        }

        // If emit_afk_notifications is set to false ignore the event so that no messages are sent
        if (!CoreToggleSetting.EMIT_AFK_NOTIFICATIONS.getValue(emitterMCP)) {
            return;
        }

        // If the player is vanished don't expose them
        if (PlayerUtil.isVanished(player)) {
            return;
        }

        String filler;
        if (event.getNewAFKStatus()) {
            filler = "now";
        } else {
            filler = "no longer";
        }

        for (Player p : Bukkit.getOnlinePlayers()) {
            // Don't send an AFK notification if they can't see the player who changed AFK status
            if (!PlayerUtil.canSee(p, player.getUniqueId())) {
                continue;
            }

            // If receive_afk_notifications is set to true then send the player a message
            MinecraftProfile mcp = PlayerUtil.getProfile(p);
            if (CoreToggleSetting.RECEIVE_AFK_NOTIFICATIONS.getValue(mcp)) {
                // Send a special message to the emitter
                if (mcp.getID().equals(emitterMCP.getID())) {
                    p.sendMessage(Message.format("events.afk_status.info.self", filler));
                } else {
                    p.sendMessage(Message.format("events.afk_status.info.others", emitterMCP.getCurrentName(), filler));
                }
            }
        }
    }

    @EventHandler(priority = EventPriority.MONITOR, ignoreCancelled = true)
    public void onServiceConnect(ServiceConnectEvent event) {
        Player player = event.getTarget().getMinecraftProfile().getPlayer();

        // Ignore the event if the player isn't online
        if (player == null) {
            return;
        }

        String name = event.getServiceProfile().getCurrentName();
        String service = event.getService().getDisplayName();

        // If they are online then send them a message saying their connection was successful
        player.sendMessage(Message.format("service_connection.info.success", name, service));
    }

    @EventHandler(priority = EventPriority.MONITOR, ignoreCancelled = true)
    public void onDonatorStatusUpdate(DonatorStatusUpdateEvent event) {
        // Update the rank and prefix on all MinecraftProfiles of the SynUser
        for (MinecraftProfile mcp : event.getDonator().getPartialMinecraftProfiles("sid")) {
            mcp.updateDonatorStatus();
        }
    }

    @EventHandler(priority = EventPriority.MONITOR, ignoreCancelled = true)
    public void onTeleportRequestAccept(TeleportRequestAcceptEvent event) {
        Teleport teleport = event.getTeleport();
        Player teleporter = Bukkit.getPlayer(teleport.getTeleporter());
        Player teleportee = Bukkit.getPlayer(teleport.getTeleporter());

        // Notify all players snooping on the player teleporting
        for (Player p : Bukkit.getOnlinePlayers()) {
            MinecraftProfile mcp = PlayerUtil.getProfile(p);

            // Skip the player if they don't have the required permission or if they are the same person
            if (!p.hasPermission("syn.snoop") || p.equals(teleporter) || p.equals(teleportee)) {
                continue;
            }

            // Send a snoop message to the current player if their list contains their UUID
            if (mcp.getSnoopedPlayers().contains(teleporter.getUniqueId())) {
                p.sendMessage(Message.format("events.spy.teleport_to_player", teleporter.getName(), PlayerUtil.getName(teleport.getTeleportee())));
            }
        }

        // Tell console of the position
        Location loc = teleport.getTeleportLocation();
        String spyMessage = Message.format("events.spy.teleport_to_position", teleporter.getName(), loc.getX(), loc.getY(), loc.getZ(), loc.getWorld().getName());
        Bukkit.getConsoleSender().sendMessage(spyMessage);
    }

    @EventHandler(priority = EventPriority.HIGHEST, ignoreCancelled = true)
    public void onSignChange(SignChangeEvent event) {
        Player player = event.getPlayer();

        for (int i = 0; i < event.getLines().length; i++) {
            // Format the sign according to the player's permissions
            String formattedLine = Message.translateCodes(event.getLine(i), org.bukkit.ChatColor.BLACK.toString(), player, "syn.signs");

            // Replace the line in the sign
            event.setLine(i, formattedLine);
        }
    }


    @EventHandler(priority = EventPriority.MONITOR, ignoreCancelled = true)
    public void onBlockPlaceChange(BlockPlaceEvent event) {
        // Only handle if it's a chest or sign as CraftBukkit prevents those from placing NBT without being op
        Block block = event.getBlock();
        BlockState blockState = block.getState();

        if (blockState instanceof Sign || blockState instanceof Chest) {
            // Perform the behavior that occurs when the player is op
            net.minecraft.world.item.ItemStack item = CraftItemStack.asNMSCopy(event.getItemInHand());
            // If the block placed has NBT data then set that to the block in the world
            NBTTagCompound nbt = item.b("BlockEntityTag");
            if (nbt != null) {
                TileEntity tileEntity = ((CraftWorld) block.getWorld()).getHandle().getBlockEntity(
                        new BlockPosition(block.getX(), block.getY(), block.getZ()), true); //no idea what validate does
                if (tileEntity != null) {
                    NBTTagCompound newNBT = tileEntity.n(); // Initialize a blank NBTTagCompound; see net.minecraft.server.v1_16_R3.TileEntity#save()
                    NBTTagCompound nbtCopy = newNBT.g(); //clone
                    newNBT.a(nbt);
                    newNBT.a("x", block.getX());
                    newNBT.a("y", block.getY());
                    newNBT.a("z", block.getZ());
                    if (!newNBT.equals(nbtCopy)) {
                        tileEntity.a(newNBT); // Load NBT; see net.minecraft.server.v1_16_R3.TileEntity#load()
                        tileEntity.e(); // Update in world; see net.minecraft.server.v1_16_R3.TileEntity#update()
                    }
                }
            }
        }
    }


    @EventHandler(priority = EventPriority.MONITOR, ignoreCancelled = true)
    public void onGuestPickUpItem(EntityPickupItemEvent event) {
        // Ignore if it's not a player
        if (!(event.getEntity() instanceof Player)) {
            return;
        }

        Player player = (Player) event.getEntity();

        // Ignore if the player has permisison to spawn nbt
        if (player.hasPermission("syn.nbt")) {
            return;
        }

        ItemStack itemStack = event.getItem().getItemStack();

        // If the item is a shulker box then prevent it entirely
        if (itemStack.getType().name().toLowerCase().contains("shulker_box")) {
            event.setCancelled(true);
            player.sendMessage(Message.format("events.spawn_item.error.shulker_no_permission"));
            Bukkit.getConsoleSender().sendMessage(Message.format("events.spawn_item.warning.console_shulker_attempted", player.getName()));
            return;
        }

        // If the item has metadata then strip it and warn them
        if (itemStack.hasItemMeta()) {
            itemStack.setItemMeta(null);
            event.getItem().setItemStack(itemStack);
            player.sendMessage(Message.format("events.spawn_item.warning.nbt_no_permission"));
        }
    }
}
