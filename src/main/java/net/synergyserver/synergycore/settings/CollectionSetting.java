package net.synergyserver.synergycore.settings;

import net.synergyserver.synergycore.ChatColor;
import net.synergyserver.synergycore.configs.Message;
import net.synergyserver.synergycore.profiles.MinecraftProfile;
import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.permissions.Permissible;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Represents a <code>Setting</code> whose value is a collection of elements.
 *
 * @param <E> The type of elements in the collection.
 */
public abstract class CollectionSetting<E> extends Setting {

    private ItemStack item;
    private Function<String, E> stringToElement;
    private Function<E, String> elementToString;

    /**
     * Creates a new <code>CollectionSetting</code> with the given parameters.
     *
     * @param id The identifier of this setting.
     * @param category The category of this setting.
     * @param description The description of this setting.
     * @param permission The permission to use this setting.
     * @param defaultValue The default value of this setting.
     * @param defaultValueNoPermission The default value of this setting, if the user
     *                                 does not have permission to access this setting.
     * @param itemType The item to display in a GUI.
     * @param stringToElement The function that retrieves an element from a string.
     * @param elementToString The function that turns an element into a string.
     */
    protected CollectionSetting(String id, SettingCategory category, String description, String permission, String[] defaultValue,
                                String[] defaultValueNoPermission, Material itemType, Function<String, E> stringToElement,
                                Function<E, String> elementToString) {
        super(id, category, description, permission, defaultValue, defaultValueNoPermission);
        this.item = new ItemStack(itemType, 1);
        this.stringToElement = stringToElement;
        this.elementToString = elementToString;
    }

    /**
     * Gets a stream of elements from this <code>CollectionSetting</code>'s default value.
     *
     * @return The stream of elements.
     */
    public Stream<E> getDefaultValueStream() {
        return Arrays.stream((String[]) super.getDefaultValue()).map(stringToElement());
    }

    /**
     * Gets a stream of elements from this <code>CollectionSetting</code>'s default value for no permission.
     *
     * @return The stream of elements.
     */
    public Stream<E> getDefaultValueNoPermissionStream() {
        return Arrays.stream((String[]) super.getDefaultValueNoPermission()).map(stringToElement());
    }

    /**
     * Gets a stream of elements from this <code>CollectionSetting</code>'s
     * value for child classes to collect into the appropriate type.
     *
     * @param minecraftProfile The <code>MinecraftProfile</code> to check.
     * @return The stream of elements.
     */
    public Stream<E> getValueStream(MinecraftProfile minecraftProfile) {
        return ((Collection<String>) super.getValue(minecraftProfile)).stream().map(stringToElement());
    }

    /**
     * Gets a stream of elements from this <code>CollectionSetting</code>'s
     * value for child classes to collect into the appropriate type.
     *
     * @param diffs The <code>SettingDiffs</code> to get the currently set value of this <code>Setting</code> from.
     * @param permissible The Permissible to test the permission of this <code>Setting</code> against.
     * @return The stream of elements.
     */
    public Stream<E> getValueStream(SettingDiffs diffs, Permissible permissible) {
        return ((Collection<String>) super.getValue(diffs, permissible)).stream().map(stringToElement());
    }

    /**
     * Gets the function that converts elements from their string representation.
     *
     * @return The element parsing function.
     */
    public Function<String, E> stringToElement() {
        return stringToElement;
    }

    /**
     * Gets the function that gets the display name for an element.
     *
     * @return The display name function.
     */
    public Function<E, String> elementToString() {
        return elementToString;
    }

    /**
     * Parses a collection of elements from a string list
     * representation, where elements are separated by commas.
     *
     * @param value The string list of elements.
     * @return The collection of parsed elements.
     */
    public Collection<String> parseValue(String value) {
        return Arrays.stream(value.split(",")).filter(s -> !s.isEmpty()).map(stringToElement()).map(elementToString()).collect(Collectors.toList());
    }

    public ItemStack getItem(Object value) {
        ItemMeta im = item.getItemMeta();
        List<String> lore = new ArrayList<>();

        im.setDisplayName(ChatColor.WHITE + getDisplayName());
        lore.add(ChatColor.GRAY + getDescription());

        String currentValue = Message.createList(
                ((Collection<String>) value).stream().map(stringToElement()).map(elementToString()).collect(Collectors.toList()),
                Message.get("info_colored_lists.item_color_1"),
                Message.get("info_colored_lists.grammar_color")
        );

        lore.add(ChatColor.GRAY + "Currently set to [" + currentValue + ChatColor.GRAY + "]");
        lore.add(ChatColor.GRAY + "To manage this setting, do " + ChatColor.WHITE + "/settings set " + getDisplayName() + " comma,separated,values");

        im.setLore(lore);
        item.setItemMeta(im);
        return item;
    }
}
