package net.synergyserver.synergycore.settings;

import net.synergyserver.synergycore.ChatColor;
import net.synergyserver.synergycore.profiles.MinecraftProfile;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.permissions.Permissible;

import java.util.LinkedHashMap;
import java.util.List;

/**
 * Represents a <code>FixedOptionsSetting</code> that has multiple possible <code>SettingOption</code>s.
 */
public abstract class MultiOptionSetting extends FixedOptionsSetting {

    /**
     * Creates a new <code>MultiOptionSetting</code> with the given parameters. This should only be used by subclasses
     * of this class, and only by fields marked as public, static, and final.
     *
     * @param id The ID of this setting, used to reference it in official documents.
     * @param category The category of this setting, used to group settings together by their features.
     * @param description The description of what this setting does.
     * @param permission The permission node required to use this setting.
     * @param defaultValue The ID of the default <code>SettingOption</code> of this setting.
     * @param defaultValueNoPermission The default value of this setting, if the user
     *                                 does not have permission to access this setting.
     * @param optionsArray An array of <code>SettingOption</code>s that are valid values for this setting.
     */
    protected MultiOptionSetting(String id, SettingCategory category, String description, String permission,
                                 String defaultValue, String defaultValueNoPermission, SettingOption[] optionsArray) {
        super(id, category, description, permission, defaultValue, defaultValueNoPermission);

        // Put the given SettingOptions in the LinkedHashMap
        LinkedHashMap<Object, SettingOption> options = new LinkedHashMap<>(optionsArray.length);
        for (SettingOption option : optionsArray) {
            options.put(option.getIdentifier(), option);
        }
        setOptions(options);
    }

    @Override
    public String getDefaultValue() {
        return (String) super.getDefaultValue();
    }

    @Override
    public String getDefaultValueNoPermission() {
        return (String) super.getDefaultValueNoPermission();
    }

    @Override
    public Object parseOptionValue(Object toParse) {
        String value = toParse.toString().toLowerCase();

        if (getOptions().get(value) == null) {
            return null;
        }
        return value;
    }

    @Override
    public ItemStack getItem(Object value) {
        ItemStack item = super.getItem(value);
        SettingOption option = getOptions().get(value);
        ItemMeta im = item.getItemMeta();
        List<String> lore = im.getLore();

        lore.add(ChatColor.GRAY + "Currently set to " + ChatColor.AQUA + value);
        if (!option.getDescription().isEmpty()) {
            lore.add(ChatColor.GRAY + option.getDescription());
        }

        im.setLore(lore);
        item.setItemMeta(im);
        return item;
    }

    @Override
    public String getValue(MinecraftProfile minecraftProfile) {
        return (String) super.getValue(minecraftProfile);
    }

    @Override
    public String getValue(SettingDiffs diffs, Permissible permissible) {
        return (String) super.getValue(diffs, permissible);
    }

}
