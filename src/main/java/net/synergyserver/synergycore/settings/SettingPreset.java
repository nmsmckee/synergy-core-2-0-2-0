package net.synergyserver.synergycore.settings;

import dev.morphia.annotations.Embedded;
import dev.morphia.annotations.Property;
import dev.morphia.annotations.Transient;
import net.synergyserver.synergycore.ChatColor;
import net.synergyserver.synergycore.guis.Itemizable;
import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.ArrayList;
import java.util.List;

/**
 * Represents <code>SettingDiffs</code> saved as a user's preset.
 */
@Embedded
public class SettingPreset extends SettingDiffs implements Itemizable {

    @Property(value = "n")
    private String name;

    @Transient
    private String description = "Click to load this preset into your current settings.";

    /**
     * Required constructor for Morphia to work.
     */
    public SettingPreset() {}

    /**
     * Creates a new <code>SettingPreset</code> with the given parameters.
     *
     * @param name The name of this <code>SettingPreset</code>.
     */
    public SettingPreset(String name, SettingDiffs diffs) {
        super();
        this.name = name;
        setDiffs(diffs.getDiffs());
    }

    /**
     * Gets the name of this <code>SettingPreset</code>.
     *
     * @return The name of this <code>SettingPreset</code>.
     */
    public String getName() {
        return name;
    }

    /**
     * Sets the name of this <code>SettingPreset</code>.
     *
     * @param name The new name of this <code>SettingPreset</code>.
     */
    public void setName(String name) {
        this.name = name;
    }

    public String getDisplayName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public ItemStack getItem(Object parameter) {
        ItemStack item = new ItemStack(Material.MAP, 1);

        ItemMeta im = item.getItemMeta();
        List<String> lore = new ArrayList<>();

        im.setDisplayName(ChatColor.WHITE + name);
        lore.add(ChatColor.GRAY + description);

        im.setLore(lore);
        item.setItemMeta(im);
        return item;
    }
}
