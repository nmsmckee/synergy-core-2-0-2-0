package net.synergyserver.synergycore.settings;

import dev.morphia.annotations.Embedded;
import dev.morphia.annotations.Property;

import java.util.HashMap;

/**
 * Represents an object that contains the differences between a player's settings and the default values.
 */
@Embedded
public class SettingDiffs {

    @Property("d")
    private HashMap<String, Object> diffs;

    /**
     * Creates a new <code>SettingDiffs</code> object that holds values
     * of <code>Setting</code>s that the player has set manually.
     */
    public SettingDiffs() {
        this.diffs = new HashMap<>();
    }

    /**
     * Gets the differences between this player's settings and the default values.
     *
     * @return A HashMap containing the differences.
     */
    public HashMap<String, Object> getDiffs() {
        return diffs;
    }

    /**
     * Sets the differences between this player's settings and the default values.
     *
     * @param diffs A HashMap containing the differences.
     */
    public void setDiffs(HashMap<String, Object> diffs) {
        this.diffs = diffs;
    }

    /**
     * Convenience method to set a diff.
     *
     * @param id The identifier of the setting.
     * @param value The changed value of the setting from the default.
     */
    public void setDiff(String id, Object value) {
        if (diffs == null) {
            diffs = new HashMap<>();
        }

        diffs.put(id, value);
    }

    /**
     * Gets the value of the requested setting from the value the player
     * changed it to, or the default value if they have not changed it.
     *
     * @param setting The <code>Setting</code> to get the value of.
     * @param hasPermission Whether or not the player has permission to even access the <code>Setting</code>.
     * @return The value set by the player or the default value of the <code>Setting</code>.
     */
    public Object getValueOrDefault(Setting setting, boolean hasPermission) {
        if (!hasPermission) {
            return setting.getDefaultValueNoPermission();
        }
        Object value = diffs.get(setting.getIdentifier());
        if (value != null) {
            return value;
        }
        return setting.getDefaultValue();
    }

}
