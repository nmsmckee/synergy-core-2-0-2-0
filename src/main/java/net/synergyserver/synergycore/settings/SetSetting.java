package net.synergyserver.synergycore.settings;

import net.synergyserver.synergycore.profiles.MinecraftProfile;
import org.bukkit.Material;
import org.bukkit.permissions.Permissible;

import java.util.Arrays;
import java.util.LinkedHashSet;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * Represents a <code>Setting</code> whose value is a set of unique elements.
 *
 * @param <E> The type of elements in the set.
 */
public class SetSetting<E> extends CollectionSetting<E> {

    /**
     * Creates a new <code>SetSetting</code> with the given parameters.
     *
     * @param id The identifier of this setting.
     * @param category The category of this setting.
     * @param description The description of this setting.
     * @param permission The permission to use this setting.
     * @param defaultValue The default value of this setting.
     * @param defaultValueNoPermission The default value of this setting, if the user
     *                                 does not have permission to access this setting.
     * @param itemType The item to display in a GUI.
     * @param stringToElement The function that retrieves an element from a string.
     * @param elementToString The function that turns an element into a string.
     */
    public SetSetting(String id, SettingCategory category, String description, String permission, String[] defaultValue,
                      String[] defaultValueNoPermission, Material itemType, Function<String, E> stringToElement,
                      Function<E, String> elementToString) {
        super(id, category, description, permission, defaultValue, defaultValueNoPermission, itemType, stringToElement, elementToString);
    }

    @Override
    public Set<E> getDefaultValue() {
        return super.getDefaultValueStream().collect(Collectors.toCollection(LinkedHashSet::new));
    }

    @Override
    public Set<E> getDefaultValueNoPermission() {
        return super.getDefaultValueNoPermissionStream().collect(Collectors.toCollection(LinkedHashSet::new));
    }

    @Override
    public Set<E> getValue(MinecraftProfile minecraftProfile) {
        return super.getValueStream(minecraftProfile).collect(Collectors.toCollection(LinkedHashSet::new));
    }

    @Override
    public Set<E> getValue(SettingDiffs diffs, Permissible permissible) {
        return super.getValueStream(diffs, permissible).collect(Collectors.toCollection(LinkedHashSet::new));
    }

    @Override
    public Set<String> parseValue(String value) {
        return Arrays.stream(value.split(",")).map(stringToElement()).map(elementToString()).collect(Collectors.toCollection(LinkedHashSet::new));
    }
}
