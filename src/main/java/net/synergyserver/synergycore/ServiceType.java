package net.synergyserver.synergycore;

/**
 * Types of services that are handled by SynergyCore.
 */
public enum ServiceType {
    DISCORD("Discord"),
    DUBTRACK("Dubtrack");

    private String displayName;

    ServiceType(String displayName) {
        this.displayName = displayName;
    }

    /**
     * Gets the display name of this <code>ServiceType</code>, which is used in messages.
     *
     * @return The display name of this <code>ServiceType</code>.
     */
    public String getDisplayName() {
        return displayName;
    }

    /**
     * Matches the <code>ServiceType</code> to the given string.
     *
     * @param service The string to mtach.
     * @return The <code>ServiceType</code> of the string, or null if no match can be found.
     */
    public static ServiceType getServiceType(String service) {
        ServiceType serviceType = null;

        if (service.equalsIgnoreCase("discord")) {
            serviceType = DISCORD;
        } else if (service.equalsIgnoreCase("dubtrack")) {
            serviceType = DUBTRACK;
        }

        return serviceType;
    }
}
