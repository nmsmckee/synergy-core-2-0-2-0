package net.synergyserver.synergycore.profiles;

import dev.morphia.annotations.Entity;
import dev.morphia.annotations.Id;
import dev.morphia.annotations.Property;
import dev.morphia.annotations.Transient;
import dev.morphia.query.CountOptions;
import dev.morphia.query.FindOptions;
import dev.morphia.query.experimental.filters.Filters;
import net.synergyserver.synergycore.Mail;
import net.synergyserver.synergycore.Pagination;
import net.synergyserver.synergycore.PowerTool;
import net.synergyserver.synergycore.Rank;
import net.synergyserver.synergycore.SynergyCore;
import net.synergyserver.synergycore.Teleport;
import net.synergyserver.synergycore.configs.Message;
import net.synergyserver.synergycore.database.DataManager;
import net.synergyserver.synergycore.database.MongoDB;
import net.synergyserver.synergycore.utils.PlayerUtil;
import org.bson.types.ObjectId;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import ru.tehkode.permissions.PermissionUser;
import ru.tehkode.permissions.bukkit.PermissionsEx;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.UUID;

/**
 * Represents the profile of a Minecraft account.
 */
@Entity(value = "minecraftprofiles")
public class MinecraftProfile implements ConnectedProfile {

    @Property("sid")
    private ObjectId synID;

    @Id
    private UUID pID;
    @Property("n")
    private String currentName;
    @Property("li")
    private long lastLogIn;
    @Property("lo")
    private long lastLogOut;
    @Property("b")
    private boolean banned;
    @Property("nl")
    private List<String> knownNames;
    @Property("il")
    private List<String> recentIPs;
    @Property("lw")
    private String lastWorldGroup;
    @Property("wp")
    private HashSet<ObjectId> worldGroupProfileIDs;
    @Property("ig")
    private HashSet<UUID> ignoredPlayers;
    @Property("tpt")
    private long totalPlaytime;
    @Property("tat")
    private long totalAfktime;
    @Property("sp")
    private HashSet<UUID> spiedPlayers;
    @Property("gs")
    private boolean globalSocialSpy;
    @Property("snp")
    private HashSet<UUID> snoopedPlayers;
    @Property("ml")
    private List<ObjectId> mailIDs;
    @Property("ptl")
    private HashMap<String, PowerTool> powerTools;

    // Remove member application system once automatic promotion detection system is made
    @Property("la")
    private Long lastApplied;

    @Transient
    private DataManager dm = DataManager.getInstance();
    @Transient
    private WorldGroupProfile currentWorldGroupProfile;
    @Transient
    private Teleport pendingTeleport;
    @Transient
    private UUID lastMessaged;
    @Transient
    private List<String> recentChatMessages;
    @Transient
    private boolean hasUnvanished;
    @Transient
    private Pagination currentPagination;
    @Transient
    private long lastChatted;

    // rank

    /**
     * Required constructor for Morphia to work.
     */
    public MinecraftProfile() {}

    /**
     * Creates a new <code>MinecraftProfile</code> with the given parameters.
     *
     * @param synID The synID of this user, used by Synergy to differentiate between people.
     * @param pID The UUID of the player represented by this profile, also used in the database as the primary index.
     * @param currentName The current name of this player.
     * @param lastLogIn The last log in time of this player.
     * @param lastLogOut The last log out time of this player
     * @param recentIPs A list of recent IPs that this player connected to the server with.
     */
    public MinecraftProfile(ObjectId synID, UUID pID, String currentName, long lastLogIn, long lastLogOut, List<String> recentIPs) {
        this.synID = synID;
        this.pID = pID;
        this.currentName = currentName;
        this.lastLogIn = lastLogIn;
        this.lastLogOut = lastLogOut;
        this.recentIPs = recentIPs;

        this.knownNames = new ArrayList<>();
        knownNames.add(currentName);
        this.banned = false;
        this.worldGroupProfileIDs = new HashSet<>();
        this.ignoredPlayers = new HashSet<>();
        this.recentChatMessages = new ArrayList<>(3);
        this.hasUnvanished = false;
        this.mailIDs = new ArrayList<>();
    }

    @Override
    public ObjectId getSynID() {
        return synID;
    }

    @Override
    public UUID getID() {
        return pID;
    }

    /**
     * Gets the <code>Player</code> represented by this <code>MinecraftProfile</code>, if they're online.
     *
     * @return The <code>Player</code> of this <code>MinecraftProfile</code>.
     */
    public Player getPlayer() {
        return Bukkit.getPlayer(pID);
    }

    @Override
    public String getCurrentName() {
        return currentName;
    }

    @Override
    public void setCurrentName(String newName) {
        currentName = newName;
        dm.updateField(this, MinecraftProfile.class, "n", newName);
    }

    @Override
    public long getLastLogIn() {
        return lastLogIn;
    }

    @Override
    public void setLastLogIn(long logInTime) {
        lastLogIn = logInTime;
        dm.updateField(this, MinecraftProfile.class, "li", logInTime);
    }

    @Override
    public long getLastLogOut() {
        return lastLogOut;
    }

    @Override
    public void setLastLogOut(long logOutTime) {
        lastLogOut = logOutTime;
        dm.updateField(this, MinecraftProfile.class, "lo", logOutTime);
    }

    /**
     * Checks if this player is online.
     *
     * @return True if this player is online.
     */
    public boolean isOnline() {
        return (lastLogOut < lastLogIn || getPlayer() != null);
    }

    @Override
    public boolean isBanned() {
        return banned;
    }

    @Override
    public void setIsBanned(boolean banStatus) {
        banned = banStatus;
        dm.updateField(this, MinecraftProfile.class, "b", banStatus);
    }

    /**
     * Gets a list of all known names that this player has had.
     *
     * @return A list containing this player's known names.
     */
    public List<String> getKnownNames() {
        if (knownNames == null) {
            knownNames = new ArrayList<>();
        }
        return knownNames;
    }

    /**
     * Sets the list of all known names that this player has had.
     *
     * @param knownNames The new list of known names.
     */
    public void setKnownNames(List<String> knownNames) {
        this.knownNames = knownNames;
        dm.updateField(this, MinecraftProfile.class, "nl", knownNames);
    }

    /**
     * Gets a list containing recent IPs that this player has connected to the server with.
     *
     * @return A list containing the player's recent IPs.
     */
    public List<String> getRecentIPs() {
        if (recentIPs == null) {
            recentIPs = new ArrayList<>();
        }
        return recentIPs;
    }

    /**
     * Sets the list of recent IPs that this player has connected to the server with.
     *
     * @param recentIPs The new list of recent IPs.
     */
    public void setRecentIPs(List<String> recentIPs) {
        this.recentIPs = recentIPs;
        dm.updateField(this, MinecraftProfile.class, "il", recentIPs);
    }

    /**
     * Gets the name of the last world that this player was in.
     *
     * @return The name of the last world that this player was in.
     */
    public String getLastWorldGroup() {
        if (lastWorldGroup == null) {
            return getCurrentWorldGroupName();
        }
        return lastWorldGroup;
    }

    /**
     * Sets the name of the last world that this player was in.
     *
     * @param lastWorldGroup The name of the last world that this player was in.
     */
    public void setLastWorldGroup(String lastWorldGroup) {
        this.lastWorldGroup = lastWorldGroup;
        dm.updateField(this, MinecraftProfile.class, "lw", lastWorldGroup);
    }

    /**
     * Gets the collection of the <code>WorldGroupProfile</code> IDs of this <code>MinecraftProfile</code>.
     *
     * @return A HashSet containing the <code>WorldGroupProfile</code> IDs of this <code>MinecraftProfile</code>.
     */
    public HashSet<ObjectId> getWorldGroupProfileIDs() {
        return worldGroupProfileIDs;
    }

    /**
     * Sets the list of <code>WorldGroupProfile</code> IDs of this <code>MinecraftProfile</code>.
     *
     * @param worldGroupProfileIDs A list containing <code>WorldGroupProfile</code> IDs.
     */
    public void setWorldGroupProfileIDs(HashSet<ObjectId> worldGroupProfileIDs) {
        this.worldGroupProfileIDs = worldGroupProfileIDs;
        dm.updateField(this, MinecraftProfile.class, "wp", worldGroupProfileIDs);
    }

    /**
     * Adds a <code>WorldGroupProfile</code> to this <code>MinecraftProfile</code>.
     *
     * @param worldGroupProfile The <code>WorldGroupProfile</code> to register.
     * @return True if there wasn't already a <code>WorldGroupProfile</code> registered for the world group name.
     */
    public boolean addWorldGroupProfile(WorldGroupProfile worldGroupProfile) {
        if (worldGroupProfileIDs == null) {
            worldGroupProfileIDs = new HashSet<>();
        }

        if (worldGroupProfileIDs.contains(worldGroupProfile.getID())) {
            return false;
        }

        for (ObjectId wgpID : worldGroupProfileIDs) {
            String currentName = dm.getPartialDataEntity(WorldGroupProfile.class, wgpID, "n").getWorldGroupName();
            if (currentName.equalsIgnoreCase(worldGroupProfile.getWorldGroupName())) {
                return false;
            }
        }

        worldGroupProfileIDs.add(worldGroupProfile.getID());
        dm.updateField(this, MinecraftProfile.class, "wp", worldGroupProfileIDs);
        return true;
    }

    /**
     * Checks if a this <code>MinecraftProfile</code> has a <code>WorldGroupProfile</code>
     * registered that has the given world group name.
     *
     * @param worldGroupName The name of the world group to try to find.
     * @return True if a matching <code>WorldGroupProfile</code> was found.
     */
    public boolean hasWorldGroupProfile(String worldGroupName) {
        if (worldGroupProfileIDs == null) {
            return false;
        }

        return MongoDB.getInstance().getDatastore().find(WorldGroupProfile.class)
                .filter(Filters.in("_id", worldGroupProfileIDs), Filters.eq("n", worldGroupName))
                .count(new CountOptions().limit(1)) > 0;
    }

    /**
     * Gets the <code>WorldGroupProfile</code> of the requested world group if it exists and loads all data of it.
     *
     * @param worldGroupName The world group name of the <code>WorldGroupProfile</code> to get.
     * @return The requested <code>WorldGroupProfile</code>, or null if no matching documents are found in the database.
     */
    public WorldGroupProfile getWorldGroupProfile(String worldGroupName) {
        // If the requested world group profile is the one cached, then fetch that one instead
        if (currentWorldGroupProfile != null && currentWorldGroupProfile.getWorldGroupName().equals(worldGroupName)) {
            return currentWorldGroupProfile;
        }
        if (!hasWorldGroupProfile(worldGroupName)) {
            return null;
        }

        WorldGroupProfile wgp = MongoDB.getInstance().getDatastore().find(WorldGroupProfile.class)
                .filter(Filters.in("_id", worldGroupProfileIDs), Filters.eq("n", worldGroupName))
                .first();

        wgp.loadSettingPreferences();
        return wgp;
    }

    /**
     * Gets all <code>WorldGroupProfile</code>s of this player.
     *
     * @return A list containing <code>WorldGroupProfile</code>s.
     */
    public List<WorldGroupProfile> getWorldGroupProfiles() {
        List<WorldGroupProfile> wgps = dm.getDataEntities(WorldGroupProfile.class, worldGroupProfileIDs);

        // Put the current WGP in the list instead if there is one
        if (currentWorldGroupProfile != null) {
            for (int i = 0; i < wgps.size(); i++) {
                if (wgps.get(i).getID().equals(currentWorldGroupProfile.getID())) {
                    wgps.remove(i);
                    wgps.add(currentWorldGroupProfile);
                    return wgps;
                }
            }
        }
        return wgps;
    }

    /**
     * Checks if this player is AFK.
     *
     * @return True if the player is currently AFK.
     */
    public boolean isAFK() {
        if (!isOnline()) {
            return false;
        }

        WorldGroupProfile wgp = getCurrentWorldGroupProfile();
        // Should never reach this but just in case an error occurs above
        return wgp != null && (wgp.getAfktimeEnd() < wgp.getAfktimeStart());
    }

    /**
     * Convenience method for adding a name to <code>knownNames</code>, if it's new.
     *
     * @param name The name to add to the list.
     * @return True if the name was not already added.
     */
    public boolean addKnownName(String name) {
        if (knownNames == null) {
            knownNames = new ArrayList<>();
        }

        if (knownNames.contains(name)) {
            return false;
        }

        knownNames.add(name);
        dm.updateField(this, MinecraftProfile.class, "nl", knownNames);
        return true;
    }

    /**
     * Convenience method for adding an IP to <code>recentIPs</code>, if it's new.
     * Also removes the oldest IP found, if there is 5 or more stored.
     *
     * @param IP The IP to add to the list.
     * @return True if the IP was not already added.
     */
    public boolean addRecentIP(String IP) {
        if (recentIPs == null) {
            recentIPs = new ArrayList<>();
        }

        if (recentIPs.contains(IP)) {
            return false;
        }

        if (recentIPs.size() >= 5) {
            recentIPs.remove(0);
        }
        recentIPs.add(IP);
        dm.updateField(this, MinecraftProfile.class, "il", recentIPs);
        return true;
    }

    /**
     * Gets a player's total playtime for the entire server.
     *
     * @return The sum of the player's playtime for the entire server.
     */
    public long getTotalPlaytime() {
        return totalPlaytime;
    }

    /**
     * Updates the value of <code>totalPlaytime</code> with the sum of the playtime
     * of this <code>MinecraftProfile</code>'s <code>WorldGroupProfile</code>s.
     */
    public void updateTotalPlaytime() {
        // If there is no currentWorldGroupProfile then the value should already be updated
        if (currentWorldGroupProfile == null) {
            return;
        }

        long oldPlaytime = currentWorldGroupProfile.getPlaytime();
        currentWorldGroupProfile.updatePlaytime();
        long newPlaytime = currentWorldGroupProfile.getPlaytime();

        // Add the difference of the values to totalPlaytime
        totalPlaytime += newPlaytime - oldPlaytime;
        dm.updateField(this, MinecraftProfile.class, "tpt", totalPlaytime);
    }

    /**
     * Gets a player's total afktime for the entire server.
     *
     * @return The sum of the player's afktime for the entire server.
     */
    public long getTotalAfktime() {
        return totalAfktime;
    }

    /**
     * Updates the value of <code>totalAfktime</code> with the sum of the playtime
     * of this <code>MinecraftProfile</code>'s <code>WorldGroupProfile</code>s.
     */
    public void updateTotalAfktime() {
        // If there is no currentWorldGroupProfile then the value should already be updated
        if (currentWorldGroupProfile == null) {
            return;
        }

        long oldAfktime = currentWorldGroupProfile.getAfktime();
        currentWorldGroupProfile.updateAfktime();
        long newAfktime = currentWorldGroupProfile.getAfktime();

        // Add the difference of the values to totalAfktime
        totalAfktime += newAfktime - oldAfktime;
        dm.updateField(this, MinecraftProfile.class, "tat", totalAfktime);
    }

    /**
     * Gets a player's adjusted playtime for the entire server.
     *
     * @return The difference between the player's raw playtime and their afktime for the entire server.
     */
    public long getTotalCalculatedPlaytime() {
        return totalPlaytime - totalAfktime;
    }

    /**
     * Finds the name of the world group that this player is in.
     *
     * @return The name of the world group that this player is in.
     */
    public String getCurrentWorldGroupName() {
        if (getPlayer() == null) {
            return lastWorldGroup;
        }
        return SynergyCore.getWorldGroupName(getPlayer().getWorld().getName());
    }

    /**
     * Gets the <code>WorldGroupProfile</code> of the world group the player is currently in.
     *
     * @return The <code>WorldGroupProfile</code> that this player is in.
     */
    public WorldGroupProfile getCurrentWorldGroupProfile() {
        return getWorldGroupProfile(getCurrentWorldGroupName());
    }

    /**
     * Sets the given <code>WorldGroupProfile</code> as the current
     * <code>WorldGroupProfile</code> and loads its data into memory.
     *
     * @param wgp The <code>WorldGroupProfile</code> to cache and set as current.
     */
    public void setCurrentWorldGroupProfile(WorldGroupProfile wgp) {
        if (!isOnline()) {
            return;
        }
        if (worldGroupProfileIDs == null || worldGroupProfileIDs.isEmpty()) {
            return;
        }
        if (!hasWorldGroupProfile(wgp.getWorldGroupName())) {
            return;
        }

        wgp.loadSettingPreferences();
        wgp.createSettingGUI();
        currentWorldGroupProfile = wgp;
    }

    public WorldGroupProfile getPartialWorldGroupProfile(String worldGroupName, String... fields) {
        return getPartialWorldGroupProfile(WorldGroupProfile.class, worldGroupName, fields);
    }

    /**
     * Gets a partial <code>WorldGroupProfile</code> by its name containing only the requested fields.
     * This method will be overhauled in a future update.
     *
     * @param wgpType The type of <code>WorldGroupProfile</code> to fetch.
     * @param worldGroupName The name of the <code>WorldGroupProfile</code> to get.
     * @param fields The names of the fields to retrieve.
     * @return The partial <code>WorldGroupProfile</code>, if found, or null if no matching <code>WorldGroupProfile</code> was found.
     */
    @SuppressWarnings("unchecked")
    public <T extends WorldGroupProfile> T getPartialWorldGroupProfile(Class<T> wgpType, String worldGroupName, String... fields) {
        if (currentWorldGroupProfile != null && currentWorldGroupProfile.getWorldGroupName().equals(worldGroupName)) {
            return (T) currentWorldGroupProfile;
        }

        T result = MongoDB.getInstance().getDatastore().find(wgpType)
                .filter(Filters.in("_id", worldGroupProfileIDs), Filters.eq("n", worldGroupName))
                .first(new FindOptions().projection().include("_id"));

        if (result == null) {
            return null;
        }

        return dm.getPartialDataEntity(wgpType, result.getID(), fields);
    }


    public List<WorldGroupProfile> getPartialWorldGroupProfiles(String... fields) {
        return getPartialWorldGroupProfiles(WorldGroupProfile.class, fields);
    }

    /**
     * Gets a list of the partial <code>WorldGroupProfile</code>s containing only the requested fields.
     *
     * @param wgpType The type of <code>WorldGroupProfile</code> to fetch.
     * @param fields The names of the fields to retrieve.
     * @return The partial <code>WorldGroupProfile</code>s.
     */
    public <T extends WorldGroupProfile> List<T> getPartialWorldGroupProfiles(Class<T> wgpType, String... fields) {
        return dm.getPartialDataEntities(wgpType, worldGroupProfileIDs, fields);
    }

    /**
     * Gets the collection of the IDs of ignored players of the player of this <code>MinecraftProfile</code>.
     *
     * @return The IDs of this player's ignored players.
     */
    public HashSet<UUID> getIgnoredPlayers() {
        if (ignoredPlayers == null) {
            ignoredPlayers = new HashSet<>();
        }
        return ignoredPlayers;
    }

    /**
     * Sets the collection of the IDs of ignored players of the player of this <code>MinecraftProfile</code>.
     *
     * @param ignoredPlayers The new IDs of ignored players.
     */
    public void setIgnoredPlayers(HashSet<UUID> ignoredPlayers) {
        this.ignoredPlayers = ignoredPlayers;
        dm.updateField(this, MinecraftProfile.class, "ig", ignoredPlayers);
    }

    /**
     * Adds a player's ID to the ignored players list of this <code>MinecraftProfile</code>.
     *
     * @param pID The ID to add.
     * @return True if the given ID wasn't already ignored.
     */
    public boolean addIgnoredPlayer(UUID pID) {
        if (ignoredPlayers == null) {
            ignoredPlayers = new HashSet<>();
        }

        boolean success = ignoredPlayers.add(pID);
        if (success) {
            dm.updateField(this, MinecraftProfile.class, "ig", ignoredPlayers);
        }
        return success;
    }

    /**
     * Removes a player's ID from the ignored players list of this <code>MinecraftProfile</code>.
     *
     * @param pID The ID to remove.
     * @return True if the given ID was unignored.
     */
    public boolean removeIgnoredPlayer(UUID pID) {
        if (ignoredPlayers == null || ignoredPlayers.isEmpty()) {
            return false;
        }

        boolean success = ignoredPlayers.remove(pID);
        if (success) {
            dm.updateField(this, MinecraftProfile.class, "ig", ignoredPlayers);
        }
        return success;
    }

    /**
     * Checks whether the given player ID is ignored by the player of this <code>MinecraftProfile</code>.
     *
     * @param pID The player ID to check the ignored status of.
     * @return True if the given player was ignored.
     */
    public boolean hasIgnored(UUID pID) {
        return ignoredPlayers != null && !ignoredPlayers.isEmpty() && ignoredPlayers.contains(pID);
    }

    /**
     * Gets the time that this player last applied, as milliseconds since the unix epoch.
     *
     * @return The last application time of this player.
     */
    public Long getLastApplied() {
        return lastApplied;
    }

    /**
     * Sets the time of the last application of this player as milliseconds since the unix epoch.
     *
     * @param lastApplied The new time.
     */
    public void setLastApplied(Long lastApplied) {
        this.lastApplied = lastApplied;
        dm.updateField(this, MinecraftProfile.class, "la", lastApplied);
    }

    /**
     * Gets the pending <code>Teleport</code> of this player.
     *
     * @return The pending <code>Teleport</code>.
     */
    public Teleport getPendingTeleport() {
        return pendingTeleport;
    }

    /**
     * Sets the pending <code>Teleport</code> for this player.
     *
     * @param pendingTeleport The pending <code>Teleport</code>.
     */
    public void setPendingTeleport(Teleport pendingTeleport) {
        this.pendingTeleport = pendingTeleport;
    }

    /**
     * Checks if this player has a pending <code>Teleport</code>.
     *
     * @return True if this player has a pending <code>Teleport</code>.
     */
    @SuppressWarnings("BooleanMethodIsAlwaysInverted")
    public boolean hasPendingTeleportRequest() {
        return pendingTeleport != null;
    }

    /**
     * Removes the pending <code>Teleport</code>, if applicable.
     */
    public void resolvePendingTeleportRequest() {
        pendingTeleport = null;
    }

    /**
     * Gets the UUID of the person last messaged.
     *
     * @return The UUID of the person last messaged.
     */
    public UUID getLastMessaged() {
        return lastMessaged;
    }

    /**
     * Sets the UUID of the person last messaged.
     *
     * @param lastMessaged The UUID of the person last messaged.
     */
    public void setLastMessaged(UUID lastMessaged) {
        this.lastMessaged = lastMessaged;
    }

    /**
     * Gets the list containing the last 3 chat messages sent by this player.
     *
     * @return The last chat messages of this player.
     */
    public List<String> getRecentChatMessages() {
        if (recentChatMessages == null) {
            recentChatMessages = new ArrayList<>();
        }
        return recentChatMessages;
    }

    /**
     * Sets the list containing the last chat messages sent by this player.
     *
     * @param recentChatMessages The new list of chat messages.
     */
    public void setRecentChatMessages(List<String> recentChatMessages) {
        this.recentChatMessages = recentChatMessages;
    }

    /**
     * Adds a message to this player's <code>recentChatMessages</code>
     * and removes the oldest if there are more than 3.
     *
     * @param message The message to add to this player's <code>recentChatMessages</code>.
     */
    public void addRecentChatMessage(String message) {
        if (recentChatMessages == null) {
            recentChatMessages = new ArrayList<>(3);
        }

        if (recentChatMessages.size() >= 3) {
            recentChatMessages.remove(0);
        }

        recentChatMessages.add(message);
    }

    /**
     * Gets whether this player has unvanished yet. Used for the legacy vanish command behavior.
     *
     * @return True if this player has already unvanished.
     */
    public boolean hasUnvanished() {
        return hasUnvanished;
    }

    /**
     * Sets whether this player has unvanished yet. Used for the legacy vanish command behavior.
     *
     * @param hasUnvanished The new <code>hasUnvanished</code> state.
     */
    public void setHasUnvanished(boolean hasUnvanished) {
        this.hasUnvanished = hasUnvanished;
    }

    /**
     * Updates this player's prefix and rank according to the data stored in their <code>SynUser</code> profile.
     */
    public void updateDonatorStatus() {
        SynUser synUser = dm.getPartialDataEntity(SynUser.class, getSynID(), "ad", "sd");
        PermissionUser user = PermissionsEx.getUser(pID.toString());
        Rank primaryRank = PlayerUtil.getPrimaryRank(pID);

        // If they haven't donated remove the rank and prefix
        if (synUser.getAmountDonated() == 0) {
            user.removeGroup(Rank.FULL_DONATOR.getPexName());
            user.removeGroup(Rank.SUBSCRIPTION_DONATOR.getPexName());
            user.setPrefix("", null);
        }
        // If they have donated less than $25 and aren't a subscriber then remove their rank and prefix
        else if (synUser.getAmountDonated() < 2500 && !synUser.isSubscribedDonator()) {
            user.removeGroup(Rank.SUBSCRIPTION_DONATOR.getPexName());
            user.setPrefix("", null);
        }
        // If they have donated less than $25 and are a subscriber then give them the rank and prefix if they don't have it yet
        else if (synUser.getAmountDonated() < 2500 && synUser.isSubscribedDonator()) {
            // Remove the higher Donator rank if they somehow have it
            user.removeGroup(Rank.FULL_DONATOR.getPexName());

            if (!user.inGroup(Rank.SUBSCRIPTION_DONATOR.getPexName())) {
                user.addGroup(Rank.SUBSCRIPTION_DONATOR.getPexName());
            }

            // Replace the color of the rank letter with that of DonatorSubscribed
            user.setPrefix(Message.format("rank_prefix", Rank.SUBSCRIPTION_DONATOR.getPrefixColor(), primaryRank.getPrefix(), primaryRank.getNameColor()), null);
        }
        // If they have donated $25 then give them the rank and prefix if they don't have it yet
        else if (synUser.getAmountDonated() >= 2500) {
            // Remove the lower Donator rank if they already have it
            user.removeGroup(Rank.SUBSCRIPTION_DONATOR.getPexName());

            if (!user.inGroup(Rank.FULL_DONATOR.getPexName())) {
                user.addGroup(Rank.FULL_DONATOR.getPexName());
            }

            // Replace the color of the rank letter with that of DonatorFull
            user.setPrefix(Message.format("rank_prefix", Rank.FULL_DONATOR.getPrefixColor(), primaryRank.getPrefix(), primaryRank.getNameColor()), null);
        }
    }

    /**
     * Gets the list of players this player is spying on.
     *
     * @return The set of UUIDs of spied players.
     */
    public HashSet<UUID> getSpiedPlayers() {
        if (spiedPlayers == null) {
            spiedPlayers = new HashSet<>();
        }
        return spiedPlayers;
    }

    /**
     * Sets this player's list of spied users.
     *
     * @param spiedPlayers The new set of UUID of spied players.
     */
    public void setSpiedPlayers(HashSet<UUID> spiedPlayers) {
        this.spiedPlayers = spiedPlayers;
        dm.updateField(this, MinecraftProfile.class, "sp", spiedPlayers);
    }

    /**
     * Gets the global socialspy setting for this user.
     *
     * @return True if this user has global socialspy enabled.
     */
    public boolean hasGlobalSpyingEnabled() {
        return globalSocialSpy;
    }

    /**
     * Sets the current global socialspy setting for this user.
     *
     * @param spying The new global socialspy value.
     */
    public void setGlobalSocialSpy(boolean spying) {
        globalSocialSpy = spying;
        dm.updateField(this, MinecraftProfile.class, "gs", spying);
    }

    /**
     * Gets the list of players this player is snooping on.
     *
     * @return The set of UUIDs of snooped players.
     */
    public HashSet<UUID> getSnoopedPlayers() {
        if (snoopedPlayers == null) {
            snoopedPlayers = new HashSet<>();
        }
        return snoopedPlayers;
    }

    /**
     * Updates this player's <code>snoopedPlayers</code> list.
     *
     * @param snoopedPlayers TThe new set of UUID of snooped players.
     */
    public void setSnoopedPlayers(HashSet<UUID> snoopedPlayers) {
        this.snoopedPlayers = snoopedPlayers;
        dm.updateField(this, MinecraftProfile.class, "snp", snoopedPlayers);
    }

    /**
     * Gets the list of mail IDs of this <code>MinecraftProfile</code>.
     *
     * @return The list of mail IDs.
     */
    public List<ObjectId> getMailIDs() {
        if (mailIDs == null) {
            mailIDs = new ArrayList<>();
        }
        return mailIDs;
    }

    /**
     * Gets the mail of this <code>MinecraftProfile</code>.
     *
     * @return The mail of this <code>MinecraftProfile</code>.
     */
    public List<Mail> getMail() {
        if (mailIDs == null) {
            mailIDs = new ArrayList<>();
        }
        return dm.getDataEntities(Mail.class, mailIDs);
    }

    /**
     * Sends mail to this <code>MinecraftProfile</code>.
     *
     * @param mail The mail add to this <code>MinecraftProfile</code>'s mailbox.
     */
    public void addMail(Mail mail) {
        if (this.mailIDs == null) {
            this.mailIDs = new ArrayList<>();
        }
        this.mailIDs.add(mail.getID());

        // Save it in the database
        dm.saveDataEntity(mail);
        dm.updateField(this, MinecraftProfile.class, "ml", this.mailIDs);
    }

    /**
     * Deletes a specific mail.
     *
     * @param mail The mail to delete from this <code>MinecraftProfile</code>'s mailbox.
     */
    public void deleteMail(Mail mail) {
        if (mailIDs == null) {
            mailIDs = new ArrayList<>();
        }
        this.mailIDs.remove(mail.getID());

        // Delete it in the database
        dm.deleteDataEntity(mail);
        dm.updateField(this, MinecraftProfile.class, "ml", mailIDs);
    }

    /**
     * Gets the <code>PowerTool</code>s registered to this profile.
     *
     * @return The <code>PowerTool</code>s of this profile.
     */
    public HashMap<String, PowerTool> getPowerTools() {
        if (powerTools == null) {
            powerTools = new HashMap<>();
        }

        return powerTools;
    }

    /**
     * Sets the <code>PowerTool</code>s registered to this profile.
     *
     * @param powerTools The new <code>PowerTool</code>s of this profile.
     */
    public void setPowerTools(HashMap<String, PowerTool> powerTools) {
        this.powerTools = powerTools;
        dm.updateField(this, MinecraftProfile.class, "ptl", powerTools);
    }

    /**
     * Gets the possible alt accounts for this profile, based on their recent ips.
     *
     * @return The possible alts for this profile.
     */
    public List<MinecraftProfile> getPossibleAlts() {
        return MongoDB.getInstance().getDatastore().find(MinecraftProfile.class)
                .filter(Filters.in("il", recentIPs))
                .stream().toList();
    }

    /**
     * Gets the possible alt accounts for this profile, based on
     * their recent ips and containing only the requested fields.
     *
     * @return The possible partial alts for this profile.
     */
    public List<MinecraftProfile> getPartialPossibleAlts(String... fields) {
        return MongoDB.getInstance().getDatastore().find(MinecraftProfile.class)
                .filter(Filters.in("il", recentIPs))
                .stream(new FindOptions().projection().include(fields)).toList();
    }

    /**
     * Gets the current <code>Pagination</code> of this profile.
     *
     * @return The current <code>Pagination</code>.
     */
    public Pagination getCurrentPagination() {
        return currentPagination;
    }

    /**
     * Sets the current <code>Pagination</code> of this profile.
     *
     * @param currentPagination The new <code>Pagination</code>.
     */
    public void setCurrentPagination(Pagination currentPagination) {
        this.currentPagination = currentPagination;
    }

    /**
     * Gets the last time the player sent a message in chat.
     *
     * @return The timestamp of the player's last chat message.
     */
    public long getLastChatted() {
        return lastChatted;
    }

    /**
     * Sets the time for the player's last chat message.
     *
     * @param lastChatted The new timestamp.
     */
    public void setLastChatted(long lastChatted) {
        this.lastChatted = lastChatted;
    }
}
