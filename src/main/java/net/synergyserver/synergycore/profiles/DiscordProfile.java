package net.synergyserver.synergycore.profiles;

import dev.morphia.annotations.Entity;
import dev.morphia.annotations.Id;
import dev.morphia.annotations.Property;
import dev.morphia.annotations.Transient;
import net.dv8tion.jda.api.entities.Member;
import net.synergyserver.synergycore.database.DataManager;
import net.synergyserver.synergycore.utils.DiscordUtil;
import org.bson.types.ObjectId;

import java.util.ArrayList;
import java.util.List;

/**
 * Represents the profile of a Discord user.
 */
@Entity(value = "discordprofiles")
public class DiscordProfile implements ConnectedProfile {

    @Property("sid")
    private ObjectId synID;

    @Id
    private String dID;
    @Property("n")
    private String currentName;
    @Property("li")
    private long lastLogIn;
    @Property("lo")
    private long lastLogOut;
    @Property("b")
    private boolean banned;
    @Property("nl")
    private List<String> knownNames;

    @Transient
    private DataManager dm = DataManager.getInstance();

    /**
     * Required constructor for Morphia to work.
     */
    public DiscordProfile() {}

    /**
     * Creates a new <code>DiscordProfile</code> with the given variables.
     *
     * @param synID The synID of this user, used by Synergy to differentiate between people.
     * @param dID The ID of this user, used by both Discord and the database.
     * @param currentName The current name of this user.
     */
    public DiscordProfile(ObjectId synID, String dID, String currentName) {
        this.synID = synID;
        this.dID = dID;
        this.currentName = currentName;

        this.banned = false;
    }

    @Override
    public ObjectId getSynID() {
        return synID;
    }

    @Override
    public String getID() {
        return dID;
    }

    @Override
    public String getCurrentName() {
        return currentName;
    }

    @Override
    public void setCurrentName(String newName) {
        currentName = newName;
        dm.updateField(this, DiscordProfile.class, "n", newName);
    }

    @Override
    public long getLastLogIn() {
        return lastLogIn;
    }

    @Override
    public void setLastLogIn(long logInTime) {
        lastLogIn = logInTime;
        dm.updateField(this, DiscordProfile.class, "li", logInTime);
    }

    @Override
    public long getLastLogOut() {
        return lastLogOut;
    }

    @Override
    public void setLastLogOut(long logOutTime) {
        lastLogOut = logOutTime;
        dm.updateField(this, DiscordProfile.class, "lo", logOutTime);
    }

    /**
     * Checks if this Discord user is online.
     *
     * @return True if this user is online.
     */
    public boolean isOnline(Member member) {
        return (lastLogOut < lastLogIn) || DiscordUtil.isOnline(member);
    }

    @Override
    public boolean isBanned() {
        return banned;
    }

    @Override
    public void setIsBanned(boolean banStatus) {
        banned = banStatus;
        dm.updateField(this, DiscordProfile.class, "b", banStatus);
    }

    /**
     * Convenience method for adding a name to <code>knownNames</code>, if it's new.
     *
     * @param name The name to add to the list.
     * @return True if the name was not already added.
     */
    public boolean addKnownName(String name) {
        if (knownNames == null) {
            knownNames = new ArrayList<>();
        }

        if (knownNames.contains(name)) {
            return false;
        }

        knownNames.add(name);
        dm.updateField(this, DiscordProfile.class, "nl", knownNames);
        return true;
    }
}
