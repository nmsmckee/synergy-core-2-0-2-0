package net.synergyserver.synergycore.profiles;

import dev.morphia.annotations.Entity;
import dev.morphia.annotations.Id;
import dev.morphia.annotations.Property;
import dev.morphia.annotations.Transient;
import net.synergyserver.synergycore.database.DataManager;
import org.bson.types.ObjectId;

/**
 * Represents the profile of a user on the website.
 */
@Entity(value = "websiteprofiles")
public class WebsiteProfile implements ConnectedProfile {

    @Property("sid")
    private ObjectId synID;

    @Id
    private String wID;
    @Property("n")
    private String currentName;
    @Property("li")
    private long lastLogIn;
    @Property("lo")
    private long lastLogOut;
    @Property("b")
    private boolean banned;

    // post count
    // forms

    @Transient
    private DataManager dm = DataManager.getInstance();

    /**
     * Required constructor for Morphia to work.
     */
    public WebsiteProfile() {}

    /**
     * Creates a new <code>WebsiteProfile</code> with the given variables.
     *
     * @param synID The synID of this user, used by Synergy to differentiate between people.
     * @param wID The ID of this user, used by both the website and the database.
     * @param currentName The current name of this user.
     */
    public WebsiteProfile(ObjectId synID, String wID, String currentName) {
        this.synID = synID;
        this.wID = wID;
        this.currentName = currentName;

        this.banned = false;
    }

    @Override
    public ObjectId getSynID() {
        return synID;
    }

    @Override
    public Object getID() {
        return wID;
    }

    @Override
    public String getCurrentName() {
        return currentName;
    }

    @Override
    public void setCurrentName(String newName) {
        currentName = newName;
        dm.updateField(this, WebsiteProfile.class, "n", newName);
    }

    @Override
    public long getLastLogIn() {
        return lastLogIn;
    }

    @Override
    public void setLastLogIn(long logInTime) {
        lastLogIn = logInTime;
        dm.updateField(this, WebsiteProfile.class, "li", logInTime);
    }

    @Override
    public long getLastLogOut() {
        return lastLogOut;
    }

    @Override
    public void setLastLogOut(long logOutTime) {
        lastLogOut = logOutTime;
        dm.updateField(this, WebsiteProfile.class, "lo", logOutTime);
    }

    @Override
    public boolean isBanned() {
        return banned;
    }

    @Override
    public void setIsBanned(boolean banStatus) {
        banned = banStatus;
        dm.updateField(this, WebsiteProfile.class, "b", banStatus);
    }
}
