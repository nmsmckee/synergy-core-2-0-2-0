package net.synergyserver.synergycore;

import net.synergyserver.synergycore.utils.ListUtil;
import ru.tehkode.permissions.PermissionGroup;
import ru.tehkode.permissions.bukkit.PermissionsEx;

import java.util.Arrays;
import java.util.Optional;

/**
 * Represents a player's rank.
 */
public enum Rank {
    ADMIN("Admin", "Admin", ChatColor.BLUE, "A"),
    HELPER("Helper", "Helper", ChatColor.AQUA, "H"),
    FUNDER("Funder", "Funder", ChatColor.DARK_GREEN),
    FULL_DONATOR("Full Donator", "DonatorFull", ChatColor.DARK_GREEN),
    SUBSCRIPTION_DONATOR("Subscription Donator", "DonatorSubscribed", ChatColor.GREEN),
    RETIRED("Retired", ChatColor.DARK_AQUA, "Retired", ChatColor.DARK_AQUA, "R", ChatColor.DARK_PURPLE, false),
    CRIMSONSTONER("Crimsonstoner", ChatColor.DARK_AQUA, "Crimsonstoner"),
    TRUSTED("Trusted", "Trusted", ChatColor.DARK_AQUA, "T"),
    DEVOTED("Devoted", "Devoted", ChatColor.GOLD, "D"),
    MEMBER("Member", "Member", ChatColor.GRAY, "M"),
    GUEST("Guest", "Guest", ChatColor.DARK_GRAY, "G");

    private String displayName;
    private ChatColor displayColor;
    private String pexName;
    private ChatColor nameColor;
    private String prefix;
    private ChatColor prefixColor;
    private boolean isDonator;

    Rank(String displayName, ChatColor displayColor, String pexName) {
        this(displayName, displayColor, pexName, null, null, null, false);
    }

    Rank(String displayName, String pexName, ChatColor nameColor, String prefix) {
        this(displayName, nameColor, pexName, nameColor, prefix, null, false);
    }

    Rank(String displayName, String pexName, ChatColor prefixColor) {
        this(displayName, prefixColor, pexName, null, null, prefixColor, true);
    }

    Rank(String displayName, ChatColor displayColor, String pexName, ChatColor nameColor, String prefix, ChatColor prefixColor, boolean isDonator) {
        this.displayName = displayName;
        this.displayColor = displayColor;
        this.pexName = pexName;
        this.nameColor = nameColor;
        this.prefix = prefix;
        this.prefixColor = prefixColor;
        this.isDonator = isDonator;
    }

    /**
     * Gets the display name of this <code>Rank</code>, which is used in info messages.
     *
     * @return The display name of this <code>Rank</code>.
     */
    public String getDisplayName() {
        return displayName;
    }

    /**
     * Gets the color of this <code>Rank</code> that is used in info messages.
     *
     * @return The display name of this <code>Rank</code>.
     */
    public ChatColor getDisplayColor() {
        return displayColor;
    }

    /**
     * Gets the display name of this <code>Rank</code> with its display color.
     *
     * @return The colored display name of this <code>Rank</code>.
     */
    public String getDisplay() {
        return getDisplayColor() + getDisplayName();
    }

    /**
     * Gets the name that this <code>Rank</code> is registered under in PermissionsEX.
     *
     * @return The PEX name of this <code>Rank</code>.
     */
    public String getPexName() {
        return pexName;
    }

    /**
     * Gets the color that is used for player names. If this is null then this <code>Rank</code> is a secondary rank.
     *
     * @return The color of this <code>Rank</code>.
     */
    public ChatColor getNameColor() {
        return nameColor;
    }

    /**
     * Gets the string that is used as a prefix for player names. If this is null then this <code>Rank</code> is a secondary rank.
     *
     * @return The prefix of this <code>Rank</code>.
     */
    public String getPrefix() {
        return prefix;
    }

    /**
     * Gets the color that is used for the prefix of this <code>Rank</code>. If none is provided
     * by the rank itself then the default value of <code>ChatColor.GRAY</code> is used.
     *
     * @return The color of this <code>Rank</code>'s prefix.
     */
    public ChatColor getPrefixColor() {
        return Optional.ofNullable(prefixColor).orElse(ChatColor.GRAY);
    }

    /**
     * Checks if this <code>Rank</code> is a donator rank.
     *
     * @return True if this <code>Rank</code> is a donator rank.
     */
    public boolean isDonator() {
        return isDonator;
    }

    /**
     * Attempts to parse a <code>Rank</code> from the given string.
     *
     * @param str The string to parse.
     * @return The matched <code>Rank</code> if found, or null.
     */
    public static Rank parseRank(String str) {
        for (Rank rank : values()) {
            if (rank.getPexName().equalsIgnoreCase(str) || rank.getDisplayName().equalsIgnoreCase(str)) {
                return rank;
            }
        }
        return null;
    }

    /**
     * Gets the <code>Rank</code> representation from the given <code>PermissionGroup</code>.
     *
     * @param group The <code>PermissionGroup</code> to get the <code>Rank</code> representation of.
     * @return The corresponding <code>Rank</code> if found, or null.
     */
    public static Rank fromPermissionGroup(PermissionGroup group) {
        for (int i = 0; i < values().length; i++) {
            if (values()[i].pexName.equalsIgnoreCase(group.getName())) {
                return values()[i];
            }
        }
        return null;
    }

    /**
     * Gets the <code>PermissionGroup</code> representation of this <code>Rank</code>.
     *
     * @return The <code>PermissionGroup</code> of this <code>Rank</code>.
     */
    public PermissionGroup toPermissionGroup() {
        return PermissionsEx.getPermissionManager().getGroup(pexName);
    }

    /**
     * Checks if this <code>Rank</code> is in the primary hierarchy of ranks.
     *
     * @return True if this <code>Rank</code> is a primary rank.
     */
    public boolean isPrimaryRank() {
        return nameColor != null;
    }

    /**
     * Gets the name of this <code>Rank</code> that is used in scoreboards. Rank names are lowercased
     * and prefixed with a capital letter that is used to sort the tab list by rank hierarchy.
     *
     * @return The scoreboard name of this <code>Rank</code>, or null if this <code>Rank</code> is not visible in the scoreboard.
     */
    public String getScoreBoardName() {
        Rank[] primaryRanks = Arrays.stream(values()).filter(Rank::isPrimaryRank).toArray(Rank[]::new);
        int index = ListUtil.indexOf(primaryRanks, this);

        if (index == -1) {
            return null;
        } else {
            return Character.toString((char) (index + 65)) + pexName.toLowerCase();
        }
    }
}
